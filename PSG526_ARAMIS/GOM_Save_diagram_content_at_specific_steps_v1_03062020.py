# -*- coding: utf-8 -*-

import gom
import os
#import numpy as np
#out_vector=[ 220,  240,  250]
#out_vector=np.arange(1,340,1)
#out_vector=range(2,340,1)
out_vector=range(2,340,1)
#out_vector=10
Allstages=gom.app.project.stages
#print('stage1=',Allstages['Stage 5'])
#print('stage1=',Allstages)
#gom.script.sys.show_stage (stage=Allstages['Stage '+str(22)])
#
#
for step in out_vector:
	gom.script.sys.show_stage (stage=Allstages['Stage '+str(step)])
#	print('stage1=',Allstages['Stage '+str(step)])
	gom.script.diagram.export_diagram_contents (
				cell_separator=',',
				codec='iso 8859-1',
				decimal_separator='.',
				file= os.path.join(os.path.dirname(gom.app.project.get ('project_file')),
					gom.app.project.get ('name')+'_Diagram_'+str(step)+'.csv'),
				header_export=True,
				line_feed='\n',
				text_quoting='',
				type='checks')
				#type='curves')
