# -*- coding: utf-8 -*-

import gom
facet_size_p=15   
#RESULT.extensometer_length=50.  
#user specifies this in the dialog box that is created here
#Dialog box creation
RESULT=gom.script.sys.execute_user_defined_dialog (content='<dialog>' \
' <title>Enter extonsemeter length</title>' \
' <style></style>' \
' <control id="OkCancel"/>' \
' <position></position>' \
' <embedding></embedding>' \
' <sizemode>fixed</sizemode>' \
' <size height="111" width="273"/>' \
' <content columns="2" rows="1">' \
'  <widget type="label" row="0" rowspan="1" column="0" columnspan="1">' \
'   <name>label</name>' \
'   <tooltip></tooltip>' \
'   <text>Extensometer</text>' \
'   <word_wrap>false</word_wrap>' \
'  </widget>' \
'  <widget type="input::number" row="0" rowspan="1" column="1" columnspan="1">' \
'   <name>extensometer_length</name>' \
'   <tooltip></tooltip>' \
'   <value>50</value>' \
'   <minimum>0</minimum>' \
'   <maximum>1000</maximum>' \
'   <precision>2</precision>' \
'   <background_style></background_style>' \
'   <unit>LENGTH</unit>' \
'  </widget>' \
' </content>' \
'</dialog>')

gom.script.sys.show_stage (stage=gom.app.project.stages['Stage 2'])


gom.script.sys.recalculate_project (with_reports=False)


# Create an offset point from fracture location. The offset point is created in -y direction (negative length direction). The length of the offset is half of the extensometer length specified in user dialogo box.
MCAD_ELEMENT=gom.script.primitive.create_offset_point (
	direction={'direction': gom.Vec3d (0.0, -1.0, 0.0), 'inverted': True, 'point': gom.Vec3d (0.0, 0.0, 0.0), 'type': 'projected'}, 
	name='Point 1', 
	offset=RESULT.extensometer_length/2, 
	point=gom.app.project.actual_elements['frac_location_1'], 
	properties=gom.Binary ('eAG1mF2IXGcZx3+b1rhdG+O2fhQRO0wSM2k7u5vt92SbbZsPoyQ21G1aCvYwu3Nmd+zOR2Zmu7vVmFO8MBdC8ULrpaCCECj0QqyI6I1QROKF+IHihSgoWGjJhVWhjfyf97xzzpk9+5FSZ1j2nZn3fT7+z//5eM9cu1k+d+/EGqdOPHL8xOPMdauNfq9SObEcNsNW/2y33Qm7/UbYw71G2M2eMcbYw9geYNd/vhosAZ9+7Ezh8+16f7XaDQvTU4cfLJxq9+uNtcL97tyVy6Pcbeev5zVyO1C9cuXyCJ8AboaoRoMeHZapsk5AjwbPE3Lh3JXLu/g48AGIjmd2naFNjZCn7/CSbjOTRvgg8D6IejSpsswyr33hjXv0yymguIW+gCYNWjTikwEN6gSs0KJHyDIhC/QJqVG6KNtuBm6EaJ42bZZ55mXpMUCiUS2ngY9B5A43CUxwg0VaNAlp0SegS8h5VmjYqkb1wo6Ee/wS4Qu0aQ4Ef/9hifkQsNvw6NM1/xYp/UHG3ejiFjGSb+oSDcM4jYPT0GElxuHnMzsy9aPATRlTeyzRZpW/vrS1gJHokqybAUU55av8qbJo9nWpml9uHeJ9VbQatGnxpMXrDuBWiBbNiwkeY54vxlF9nJA6oZ1vsUDIaeOW8FNE0zQfN6YVgb0Zm6S1OpDYpss6v/rh1g7GTJkAPgKRkmDeyBZwNzUC5qmywLMEPGcp0GDe2PTi/TsSXAJuyRWsvJO3CUp/a0mkD5ZH6ZztkTHn75Spn3J5ZBgo69zr39eG/1965Ke3ac9+EIo5rglpuSt3avz9OztyyMO+EakWgtwl74tvv3tXnAPp1yh/NhrK9fFc3R3aFhtPuD+9Jf3edQ/lMct+RVMOn6VtCdln6iVJPwbMGrC7uTet/nrWP9j1+htbo95jIS5Dypn2L2WorxI12qzEEfn2nOSMDpS/OXsk0jebU1U1Yc1K2gqdmLAhq4wv/T+CsbkdqxYI1a+W9RNlc4uQV78mO7at2j5qaYb5LFykaxC1qPGjssR9DjgA0ZzFuEKFk1bhp219epDPj1K1NqJvtR6WdsxaiBj8wi8EsxJXlkhHEgSipeFvKBF+z58YTnW1pq6V8S4BKt9Ox5lf54UkseHwU5J4C1hD1TpuF9dmX/juQX3Oy4WstmbcpC/OSpdKxRhEj6Ys8m18r2lQq1fx0doHSTmt0AVXv/Jjff8AaF8mNvKqZbtUyuRdENNbDUFkqBkdnt8tOwrOp0i+Zk89Odj527eka2djhfcrzZa6ye7zjpW0D1sFSarAyfjXb3xTWjSWHI+9rsRdwNULUXfdmpmbHfI0DLITePn1vbdLypTDMWOR520nTvvAUiIpvld/Jks96hsHmrgPfxbYt63oHs/SoEPHylzAEiHVAQl9aHqc/11aqQYgwfLUK9IkuqnnPjyuT3kg+xHinfW0lE1N93FPo+iQdaOn1q9MSpQvhn6MSEamZBjVsCCmqdv4mv+vt2WqYlnYRk9AJ64AyQgzPH58a0K23AXiz6CDnM2c2zi0nPq6bJgHnolrx5yrI9EU2Xd58HnrVfaUuEWkvy9fWLfQ+KFqK1zVEBTeGr8/I5v8iJHUzCd4gs9Qo2JZ/jRwLs6J0++x/Rdf/eNPJDePUr6BXfvv1jz4x/vh6g1EMyzRp8kyR9Ha0VxrTfcaUAuWZfr0EEXOx2OpTjlNRQpWlUV8ZWTf9h2myKTJEQfXrR0XbKWU0g5/fpIFevQocpQxbqIQvzvcRcGqYIEvUWDV2qFOle2yJfapF2mnhsAyqza8djhCgQsDWTNMpmyQRZMZP5VuNdYppHbJPl8BJTe0fiifK9xjrJOOZEfiY4X0DHfEvJoxHN1IIqw0Gy+bBzU0wio7pXHKsExk6bt0Pyqnul+FfSwwNfiTPdm9iZwKPeuaYq/2KeO7LJrmsvWbDhWm6LCW87uk9m2XZoPNdsm3uuGz2Q7xxmPo9zjuODx8TBM8ysYi+bWY8lyo7KOeers9yZSgHTrpkO/G/2sbsH3OZgrVPjdOllN324pdpRUfeSbUsvEq2/fO48O5v6f9zd+RIJ8nfxh3h9ZGLBRXh6s6ZMh6jKoi7jBtGbumB4i8Wy6+V3g5tg7jmcYrf4fHS7zIk5CPWDI9OV4oew/EGG3PvCyrlKEbWZU8EZEGzwu300dgOK+T2pGtLvddd3XZn+njBUqDeq36WeROywWt/bihXqaHRKrUdeO8pvpD7Lfa6C2W9ZOWN0n+ZD1NIjbs6+b7dsrgo+w3q3QVq9NgLR7Ekkqqu4CLX3pClv/T5ksWlxIli4wiUbIaIz/Tjw56BDZfSmPbHpuJbzrjHs5I2yGOMsUhZilR5NKBSweKHKJgXahoKCf3k4c4yD5OcnIwpRzcBmOh7aqVVj5L/WcXh632iP/qZG6fKoLr669NaFr4JLBnaMpxGa1bv3rdwn2aGfwEvZNhVp1XDxvT97K/PJiej5LnBv7eln8vu/U3vWsPjP/zBlnqr8V5D6MCahYPsVg3Wtc5jr+Ztlw4SFvyMJNo9H/cOigYATWN'))


#Tried using these recalculations to make it work without success (this worked when doing it manually)
gom.script.sys.recalculate_project (with_reports=False)



gom.script.sys.show_stage (stage=gom.app.project.stages['Stage 2'])

# This creates an extensometer
MCAD_ELEMENT=gom.script.deformation.create_extensometer (
	computation='more_points', 
	direction={'direction': gom.Vec3d (0.0, 1.0, 0.0), 'point': gom.Vec3d (0.0, 0.0, 0.0), 'type': 'projected'}, 
	facet_size=15.0, 
	image_point={'pixels': [{'image': {'camera': 0, 'measurement': gom.app.project.measurement_series['Deformation 1'].measurements['D1']}, 'pixel': gom.Vec2d (1076.371359, 1054.916394)}, {'image': {'camera': 1, 'measurement': gom.app.project.measurement_series['Deformation 1'].measurements['D1']}, 'pixel': gom.Vec2d (926.9853089, 1058.548457)}], 'point': gom.app.project.actual_elements['Point 1']}, 
	incremental_facet_matching=False, 
	length=RESULT.extensometer_length, 
	measurement_series=gom.app.project.measurement_series['Deformation 1'], 
	name='Extensometer 1', 
	pattern_recognition='standard', 
	properties=gom.Binary ('eAG1mFtsHOUVx38GmgaXNBh6QVUv203SOMDajrlvnBhI4gaUlCg1ASGV1do7a2/xXrK7xjatm4n6QFRVQjy09LFSoaoUCYmnUlUVfamE+pA+oF7Uqg8IJJBApcpDaStBqv/55tuZWY8vQXRXlmd3z3cu//M/l5npZr1w+o6RZY4dve/I0VNMt8u1bqdYPLoQ1ING92S72Qra3VrQwb0G2MaOQQbZweAOYOo/3y/NA19/6ETum81qd6ncDnLjY/vvyR1rdqu15dxd7tzFC9u5zc5fyWvgS0D5HxcvDPBF4DoIK9To0GKBMiuU6FDjKQJWT1+8cBVfAD4F4ZGU1AmaVAh47Gav6SZzaYBPA5+AsEOdMgss8Oq33rtdv4wDn4cwYIGAOiX7vcYcDeoENOhSok3AGRap2VWF8qq8uA64BsIZmjRZ4PEXpdFCD7d7FxRMrHyWJvWe4hfulZrrgW3mXJc2NRrMMfwXnb/GgRgykO3qPDULuESNKiUWaeAstFika67+dmJLruZAKCVclSPeIaE/y7mfS9UpYC+E02aoSJHjBMwZVhXG7RvlxaGnk00aPLBGm+SmaVOmRpflpxXibkCe6PpzwLUQrvXjtR+sfjUpEcPbYZ4mS7zx3MZBD4TnpWAClKNE0MpB2YJR0suWC3cd4PMzSzcK6pGzMnMzcCOEcwbICA8xw7cJTO4UAVUCO6/cBBw3cirnIkqyToYs7BHgsxCK+DMotBK3UaHEDGVmeYISTxrta8wYaZ+5a+NgIzIOAzdkKlatycE4sDcbUukz4AM7bTJy5swt0vg1IB9lS5XmXv++3P///H2/uUkyyu71mR4IHIWrcCq89dMtBSTTOzPVNWjSjkr9mQ8+eigugORrO3835ij0oUzbLZqWG8+Rv70v+z50D+VhazLKpgI+SdPKrMvYc9J+GJg0YLdxR9L8lVz/4qp339sY9Q6zUbcTzZu/l6O+GVVoshhl5CfT0rO9Z/yfkwdCfbM+VdV6lq1zLtKKCBuwxND8/yMZ6/uxZIlQm2zYDFEBNgh4+Wn5sWn/9llL1qKvwjnaBpGa3i8LUvcNYE+qMU7ZIHEt8Xivnu+nTIfAvtV1v7bDNkzE4HO/E8wqXHkiG3ESCOf7v2GY4Hl/or/UNaTaNi3alGxKOBsn/pCVktiH/Y9K4w1uPFipR1Pp8uS5n+3V56xaSFurR4P57KRsqVUMQnh/wiM/uneaBY13NR9d+ySpppW60qXv/Urf3w2SS+VGUTVMSq1M0ZUiequHiwwVo8NT2+SHH3mKNX3qkZ7kH9+Xra2tEj6uJFuqprvLh9bSPmMdJO4CU9Gvz/5IVo4BR6Koi9EUcP1C1F2x+eNWlCwLveoEXnx351ekZczhmPLI87YVlX3JSiJuvpdekace9bWrTTQ6HwR2baq6wxPUaNGyNldinoByj4Q+NR3O/ClpVHuWYHn0JVkS3TQm7x3SpyyQ/dT/cCWpZV3Xfd6TKDpk3bqp65dGpco3Qz/5480sXkA138U0TRvf8//1gVxVLnOb2CnRijpAvHX0bww/HpEvt4L405sgJ1Pn1u4Zx34oH2aAx6PeMe36SDhG+l3ofd74Kn1K3CLU33dXVyw1fg/aCFcNBKW3wp9PyCe/YsTL5MM8zANUKFqVPwacjmri+Mfs/9mX//pr6c2ilB9gl/+7MQ/e/iRcuppwgnm61FngELp2NNe11mDtlDmrMn06SJ4z0SapU85Snpx1ZRFfFdk1uf3kGTU94uCKjeOcXamkJOHPjzJLhw55DjHIteSid4tbyVkXzPEdcizZONSpgt1giX2aRZLUElhgyTbfFgfIsdrTNcFowgd5NJqKU+VWYYVcQkr++Q4ovYHNQ8Vc5HZjnWzEEnGMRZI73AGLasJwdCuJsNJuvGARVNAKq+qUxTHDMtal75LzqJCYfkV2MctY70/+pGVjPUU6NjXFXsmp4tvMmeWCzZsWRcZosZzxu7R2TUo3TetJKbaq4bOehHjjMfQyjjsOD5/TGI+CsUhxzSUiFyq7qCbeTibeEiShkw75dvS/sgbbJ22nUO9z62QhcQtdpG54VSxrQi2dr4J97yLen/l7Mt5siRj5LP39uDu01mKhvDpcNSEDViJUlXGHacPYNd5D5KNy8ePCy7G1H88kXtkSHi/xIktDNmLx9uR4oerdE2G0OfPSrFKFrmVV/OBFFjwvnKTPQH9dx70j3V3uvOLusjs1x3MM9/q1+meeW6wWdO3XDc2yhvXSg1SN89rq97HbeqP3WN6PWt3E9ZOONM5Yf6zry22VwYfYbV7pVqxKjeVoEYs7qe4FXP6SG7LiH7dY0rgMM2yZUSaGrccozuSjgw4l2y9lsWlPpcQ3nXHPU2RtH4cYYx+TDJPn/J7ze/LsI2dTKG8ox/cnB9nLLqaY6m0pezfBWGi7bqUrX6X+s8vDRjLivyaZk1NHcHP91RFtC18GdvRtOa6i/aOu2Tu1M/gNeivLrCavHpgl78tevye5H8XPDfx9W/Z92Y2vdS7fPfTO1f8D4X0RjQEARA=='))

gom.script.sys.recalculate_project (with_reports=False)

# This step was created by selecting the extensometer, iinspect and then using dimension 
# Must show the distance change relative to first stage in mm
MCAD_ELEMENT=gom.script.inspection.inspect_dimension (
	distance_restriction='xyz', 
	elements=[gom.app.project.actual_elements['Extensometer 1.p2'], gom.app.project.actual_elements['Extensometer 1.p1'], gom.app.project.actual_elements['Extensometer 1']], 
	nominal_value_source='from_stage', 
	properties=gom.Binary ('eAHtWltsXFcVXa7akJg8mhJCKIVeJnY8bupn04RO4jhvEkjSqHHdKoiMbM+1PWQ8M5kZx3ZpyFT5aFQiKj4g/AECKqRUldqfFgHiISFVSKQCCVqBAKHwVqSgflCK1Aatfe6ec+7MnfHYbtofbEW543vOfu+199lnhnJTXcP3d8/i4P7d+/Y/hKHCSLpUTCT2Z/wpP1s6Vsjl/UIp7RdhflqwDKta0YpVaF0FoPDm+eQkgE8+eMQ7nhsvzYwUfK+/t+8B72CuNJ6e9baZfVcuL8d9sn8hPy3cMrb6yuUWrARwK1AeQxFzKOIz565cvgX3APgAUJ5ADlPoxoMYxefgYwwlPAQf4/BRgI8sxuDj8D2kdHsghivKWvDNvQDWAeUU0hjBBAoYwRSSGBF60xhBBkmMIYcMcijgxB8pw3oAKxwZ9lbe9z1KqncAWC0maKEG8rOr5er7+PljAFZFcLQ8fvn1xfEo37j7iW91kIfaqFqrKYxgFknMISmaZjEBH1d/RX6HAbQB5SGxawIJHEAaE+iX530h+1DfLMbl/bRQKiEtfxsWi03DR0K0/5CRRJ5XuB64WM4801jUNLI1ov71vRS1G8AHI6yaFYtR3OpoWfnFxXnSRks/QBvWePIM0igijVGkkUEaJfHpFHJIwcdfzpPvKICeJXuU3h+O4HYk4PWPj9KNewDsDPy8BcAGoDyDScnDJPKSkUX5VEJSbJXEfUghiaLkaRY+zl9/rY3769mZkcv4pebcl5PY4+5LA9R3B4C7Qvrux2yFN63FCK2N5t0Bzf7N5N4LCCqQ4nInYuMtfBuNJBdv5bu7AGKW+KqIPDIYEa+Q82PwcXaYNO8E8H6gzIyyq9SaJwSvSGlDYE3iyG1AuQhmbwYZvPzZ61v45iCAWAN+jAdaOh3spN3HkcQ0suKLTICaPlKIC7Yq4o4iJ4h28jnyEeQsX1DBqKLZTKAkbE1hKnDtd3aRDI20TEQuoSAiTCD+GvcHYFiGGJOmXh+ilhIwmRGjTWIEKeTkUx45oVPCiTcbC4rycpLWvLGCTiItFnatYITPYzqwwo93NEVc4d8Sp7SU9OqlxgQCM2qkugRKUn8on0VmPjNvjBnpKxPDj9yEShgHWLfKRPWM2KMgboiS65VPu26mk6YxKiHxtSHqx7zRnw0SxZrTzIlRiT3N/1GptaeQhMU0Unp6W2NTBo5WsWsJMw1pPmu2P2dJUr2nDcSwrKEwpwUANpm0ErGZhObnPzeq/7+w+4cbuKYNYMRHSMCcoLpUJ4W/faMphch6TSQ51plCkMtPv7V4VYwC7s9y/F7ikqqvjeTNBCSOaQT+7g3yV9XVlHulFtGbVPhYJWl7L5H6XgCDYthluN9lv5Dn795y7Xpjq7Og0EQGd3I/p6CKSI1C9V+D28ukXD9UmRSzUsCmkQ8C1scM1k7eDGfUl2NGHEFAYxm1BfSlJylHfRAPUka95qaMZiE7YGZzFim82EVyRwG0h6qq2xceruTzHoxIVWF95XM1NdsjP/EzSsHEpSTk4eBFebL6L4jD/7buqE51VqqC4DrRyvbQR16JcomVoW6ffmMw6KGjciHMTbutc4PkRahoBcp7HIm0qq8RMGHlJ/jwWZ3EnGbvk3z9C9/j3z8BcF3IN9QqK6sIZdQ3GYQ3qwWDISXh8NgyyuGZs0fZ9OjurkcqK3/9Bnk112WoXm60jAvtEt4WSFsnCGJR4EDw9stfIRd2KfsCrRNBFTB4wdDluY5pxU9RHFxoeO7amrtJRdsGd73GbT5I+6Q0JBZ8X/8RJVWr1+1vPgVg47ykiziFNPLIC8wlpc1lr2KCUF1TxOnfuEyn5e8lPPo8lWAnxMZq11p+ijKy9hRvz7lU6oqufnetYixrOlE+P99DUgqG2lfY9sz2pjxHa7VRzP/3WxSVvvTm4cOGzSCA7WmqT+Zf7aYsev7WCnIstK/2PH/wImXgyeZkgB1Dwbm1F+Hfrsrnxk/hXYwtlPnv8bNz4ho9SzeyKwsC3ZvCq0cok7YY9iz9MB7GIaSQkCw/AWA4yAk9d1fL0Vhq+7Z637mXfvt90o0KKS1gN/7bOA68O4DVK1DegUmUMIUMdoLPJsz5zIafnaEnWcZPA4jhdNCncpfhFIMnqMzAN0cErutDDD1ChzE4Jw2SJ09MKa7Q/T3g9KeIGHaiFSvgBb953AtPUs3D5+GBZ00WRR9dcqpi9LEWcSWbwC7MSF+dx3Z4OFuhtQM9jgyUqCekJ9MthTl4zirKpwhIur7MQahzAlsk6sjDrrA6JuD2cNtFqx1iR9Mk0lbsjTOiQQpsYZmd5NgrtrS0+De3HnU51S+BjRhDb+Uf5QmvtXQScpKmJVOyjhlfwIRw7pJ6k0cCvchjNuI9qZZkFWdG9VZRt3GxT70VjBu1oV0TtkKX2EjpUNbq92EqUSusPlFvq7WhhzwUMC2ci+IHrdgmso23NOKst7okxmn1Cccv3L8R486vWWN7GK7gThMXheD/VI3nz0ixITKbZrcraHknZMLBcz+jh1JH6Wnt2Bf53rVj9IrF2LHWFkZiep3128dcYFXGo7FpVmzeX7HIYjPlnbLXUuOOcRHlkejIs72diQtiS3vTkReOKuJHbVTZqCEHjQuzUj1QjToW2cLYt3XB2NcW6jI8xCvVhOgew2bJBT5rM2SmV0T6AYxLzPPM0Yk2QW6VmNL3SN7Y/AlraiO8Wtf666KyOSqCd6JNpOJBkdOU2aBNtDjPk4pBDrd/p/79okvYLnHExTP0RFwwhnq6gw3OQ9n9kmNOZnzMT+4xsyRy68RO9KITg4gjhgvtF9pj6IQnNTImVranpwF0YCMO4EClh+qYx8a0tkErPmmW6mfjB7tG195MZFtqpkbvj87TcMy8+1lUQB5FdGNa4o6xxihhlHFCTmRlr8LpudYIHdh6GJTOq4C4DBK4gvWfEWS6Me4axahQ4cDDngc6sRmMTTPvptdNpNWbWg5gAIdwFMPYjcM4hH0SjR3oQCKQoFlKnTC8ydeOYaivOVHyEJYLtBlEDB2IISFR7kZ8LWYoftRHPLdvqu3l2qQ7oFX/7xEOxpbuEYseBitsBLNeuShe20dpxVu6VxeL5uylZwW1mUNFebJI7p6w+YYZVpRek7nKXDBZqjdX3TJiCOeg1sn6VZKZxftYkzN98n9UvVy6ld5tfW5G/i5ch/nqroePYwAeDiGLM0H8pqQKz+/v+WlvkmhhR9kbYHkMP2hnF23q+gvyrKhHtCZCubfV7EM4JTL1gLKGMZqS2phtvLt7yd0Jv3XA6nAc+0Vq9rAmfzj71O9jNC9z453vtLysNc+221oT7qmW7u8o79R2aolK98YOjr9L6d7Y8XACYjo4ntXMPOjSq5wy6bdZ3OmYOWspao1t5axJJ6/NDEE5sSH2sXqwi2UM/OkBd66m08J55/lP/f3Gup+c30ZJ9VsJ7JQMuoZ7Fkpt5+uW84ty16kTvaY5O9/40fsW5axX3NUyvPBP11K0O7W3d/BVN56GXA7G3EwMpoY2cWYUcdtKktSBbzO3X1sBfLgibGPqSUyIe3h5lMHjciWojq4rftgP5jSnqMtezvWCwZ0Jud2Iy23UvOT12sY411yoh6+0dWC5/hkS1DEpL7X02vtosOLYKQZOM9c2OiQ2x66o4m1aUh6YC7iWbtYren2uRmuOPg+VbOKz6PjSQjk1owmvWXg0KODZzELpN6OJpW812fSHhXLSL1k0spk5MFku9BJbLBaYthQ5zhtwmt21J24mIyu4+Y7SNz9Ccnq7z5sBsrLn8+PO2k39dDpdwXDms37xhvU36nT/8u1P/oLrlL4qZoDA3MfbbvzkTxurhvLy/wFIf9HzAX1e'), 
	type='distance')
# This step was created by selecting the extensometer, i-inspect and then using dimension 
# Must show the distance change relative to first stage in %
MCAD_ELEMENT=gom.script.inspection.inspect_dimension (
	distance_restriction='xyz', 
	elements=[gom.app.project.actual_elements['Extensometer 1']], 
	nominal_value=0.0, 
	nominal_value_source='fixed_value', 
	properties=gom.Binary ('eAHtWmtsW2cZfjJtJQ3tuo4yCgw4c5PGWZfrupa5SdNbSgvtVq1ZNhVRy4lPHDPHdn1pkrFST/2xalRM/GDlHyAuQuo0bfuzoYG4SEgTEkUgwSYQIFTuqlS0HwwmbUHP+53X3zn2seMk6/iDrSjHPt/33t/nfb/3eDw32ztxT988Do3tPTD2AMYLiXSpGIuNZdxZN1s6Vsjl3UIp7RZhXm1Yg/Ud6MB6dKwH8PKb5+IzAD5x/1HneG66NJcouM7QwOC9zqFcaTo97+w0+y5fasfdsn85r7ZBAFN9ly+1YR2AG4HKFIpYQBGfPnv50g24E8D7gEoKOcyiD/djEp+FiymU8ABcTMNFAS6ymIKLI3eS0i2eGH5RNoJ37gKwCagkkUYCKRSQwCziSAi9MhLIII4p5JBBDgWc+ANluA3AWp8M+6v3Bx8m1VsB3CwmaKMG8trTduU9/PxRAOtDOFoev/jqSng891z7ovP4N7rJQ21Uq9UsEphHHAuIi6ZZpODiyi/J7wiATqAyLnaNIYaDSCOFIbk+ELAP9c1iWu6XhVIJafluQixWhouYaP8BI4lcr/V74EIl8+3moqaRrRP1L/9LUfsAvD/EqlmxGMWtjZZ1X1iJJ/3RMgTQhnWePI00ikhjEmlkkEZJfDqLHJJw8edz5DsJoH/VHqX3J0K4HfV4/f0jdOM+ALs9P28HsBmozGFG8jCOvGRkUT6VEBdbxXE3koijKHmahYtz117r5P5GdmbkMn6pOfflJPa4++II9R0GcHtA3zHMV3nTWozQ+mje69Ec2kbuA4CgAim2+yI22sa74Uhy4Ubeux0gZomvisgjg4R4hZwfhYszE6T5IQDvBSrMKLtKrXlC8IqUNnvWJI7cBFSKYPZmkMErn7m2nXcOAYg04cd4oKXT3k7afRpxlJEVX2Q81HSRRFSwVRF3EjlBtJPPko8gZ+W8CkYVzWYCJWFrFrOea7+1h2RopDUicgkFESGF6Gvc74FhBWJMmvq2ALWkgMmcGG0GCSSRk0955IROCSf+01xQVNpJWvPGCjqDtFjYbwUjfB5lzwo/HG6JuMK/JU5pKemVi80JeGbUSPUTKEn9oXwWmXnNvDFmpK9MDD90HSqhZh2jdlKiQzN0UqrhI4jDog7d/9TO5sp6rogCLIghhJkoVNAq9qcsSap9tcRPyBoKc0pSdKsJfEkPpol5/Xux9v/5vd/fzDWdAGMyRAJGLdWlOkn89WstKUTWG0LJsRIUvGx76q2Vq2IU8L/a8TuJHKq+MZQ3U4RIozHy2zfIX1VXU+6XakFvUuFj1bQauEjq+wGMimHX4B4/++Vcf+eGq9eaW52QTxMZZMj9lIIqZjDjy55HvjJOOgRh8/rn6K4Kv2kcqkzjeSkxZeS9gHUxh40z18MZjeWYE0cQcljobIl76QnK0RhmvZRRr/lzUbOQPSpNlEUSL/aS3H0AugJ1z9+5Hanm8z4kBPdZAXldS812sY//hFIwcSkJeVgnoDJT+w2icL+pO2pTnbWkIMhbCHTSR38e5hIrQ8NOenHU63LDciHITfuhs6PkRajoACr7fBJp3d0gYMLaTPDhtTqJOc3uJP7657/L7z8OcF3AN6wjWVlFKKO+cS+8iecMhqSEw6NrKIdjTgcV00X7dz1UXfmrN8irtT5A9fJHy7TQLuFtgbRNgiAWBQ56d7/0ZXJhH3HA0zrmVQGDFwxdnryYVvwUxkGTk69nr274GKloYfev17jNe2kfl5bBgu/rP6CkavWGHcgnAWxZknQRjyCNPPICc3FpRNlNmCBU1xRx6td+pmX5voSHn6cS7FXY+uzZyE9hRtaq//aCn0pD0dXvfqsYy5pekdfP95OUgqFWfttA2e6RJ12tNor5/3qLotKXzhJ82FIZBLBdR+3Z+ek+yqInZK0gxwL76k/chy5QBp49TnrYMe6dLAcQfPdWPze/Cu5ibKHCv8fOLIhr9LTbzK4sCHRvEq8epUzaYtjT7oN4EIeRREyy/ASACS8n9GRcK0dzqe3d2n1nX/rNy6QbFlJawBbfbB4Hzq3AzWtRGcYMSphFBrvBaxPmvGZLzp7SkSzjpxFEcMrrJLnLcIrAkSaegW+aeK4bRAT9QocxuCDl2JErphRX6P5+cD5TRAS70YG1cLx3HnfBkVRz8Dk44GmQRdFFr5x7GH2sRVzJJrAXc9L55rELDs5UaQ2j3ycDJeoP6Ml0S2IBjm8V5VMEJF1XJhXUOYbtEnXkYVdYHWPw93C7RKthsaNpSWgr9sYZ0SAJtrDMTnIcEFtaWvzOX496fXOkGLZgCgPVP8oTXGvpxOSsS0smZR0zvoCUcO6VepNHDAPIYz7kPqmWZBWnOo1WUbdpsU+jFYwbtaFdE7RCr9hI6VDW2vtBKmErrD5hd2u1oYccFFAWzkXxg1ZsE9nGWxpx1lu9EuO0esrnF+7fgmnf26yxPQxXcKeJi4L3P1nn+dNSbIjMptnt9VrelMwgeDJn9FDqMD2tHQdD7/vtGL5iJXast4WRmF5n/Xax4FmV8WhsmhWbD1UtstJMeafstdq4Y1yEeSQ88mxvZ+KC2NLVcuQFo4r4UR9VNmrIQePCrFQP1KKORbYg9u1YNvZ1BroMB9FqNSG6R7BNcoHX2gyZ+RKRfgTTEvM8c/SgU5BbJab0/ZI3Nn+CmtoIr9W18bqwbA6L4N3oFKl4UOQUe95rEy3Oc7BlkMPfv1P/IdElaJcoouIZeiIqGEM9/YMNTizZ/ZJjTqZwzE/uMdMecuvBbgygB6OIIoLzXee7IuiBIzUyIlY2eceoGUE3tuAgDlZ7qO4lbExrG7TilWapfjZ+sGt07fVEttVmavj+8DwNxsy7n0UF5FFEH8oSd4w1RgmjjDNsIit7Fc63tUboSNXBqHReBURlkMAVrP+MINONcdckJoUKBx72PNCDbWBsmok0vW4irdFccQQjOIz7MIG9OILDOCDR2I1uxDwJWqXUA8ObfO0YhvqaEyUPYTlPm1FE0I0IYhLl/oivxwzFj8aI5++b6nu5TukOaNX/e4SDsdV7xKKHwQobwaxXfhSv76O04q3eqytFc/bS84LazKGiXFkk95+weYcZVpRek7nKXDBZqs+W+mTEEMxBrZONqyQzi09MTc4Myv+werl6K73b+lyP/F2+DkvVXQd3YAQODiOL0178JqUKL+3vpWlvlWhhRzngYXkE3+tiF23q+gtyrahHtCZC+Z8nsw/hlMjUA8oaxGhKamO2+e6+VXcn/F0Aq8NxjInU7EZM/nD2qb+YaF3m5jvfaXlZa57psrUm2FOt3t9h3qnv1GLV7o0dHN+r6d7Y8XACYjo4ntXMPOjiq5wy6e9N/NMxc9ZS1JrawVmTTl5bGYJyYkPsY/XQjvSP9/rnajotXHKe/+TfFjf96NxOSqq/G2CnZNA12LNQajtft5xf/NSKOPt+k6PPW5SzPoSuleGFf/gtRbtTe/uUvOaJpyGXgzE3E4OpoU2cGUXctI4kdeDbytOvHQA+WBW2OfU4UuIePjzK4DF5JKiObih+0A/mNKeoy17O7wWDOyl5uhGVp1FLktdxrTkAhZVR0xzy6FrA1XSr9mHw+MOoNfo83rGdzqL7i8vl1IomfODBJr2AZzLLpa+OaKaJpW812fr75XLSHyQ058Qmy3Khl9jsEOo7k+S4pOs1zwwbkxR6IvH/nufrHyY5fc7OGX1w9XE5S5vf/mwdotPpCj4P5LX+SIWVMOyc/cotT/yM65S+OZPpYyHzZNz2xSd/3Fw1VNr/C7XnqO4Bx+8='), 
	type='strain')
#Tried using these recalculations to make it work without success (this worked when doing it manually)
gom.script.sys.recalculate_project (with_reports=False)

gom.script.diagram.export_diagram_contents (
	cell_separator=';', 
	codec='utf-8', 
	decimal_separator='.', 
	file='D:/Measurements/DB-S355-0-4-nr_1-02oct2020.csv', 
	header_export=True, 
	line_feed='\n', 
	text_quoting='"', 
	type='checks')





