# -*- coding: utf-8 -*-

import gom
import os
Allstages=gom.app.project.stages

#i=0				
#for step in Allstages:
#	i+=1
#	print('stage1=',i)
#				
OUTPUT_NAME='_THICKNESS-STRAIN_'

i=0				
for step in gom.app.project.stages:
	i+=1
	gom.script.sys.show_stage (stage=step)
##	print('stage1=',cur_stage)
	gom.script.diagram.export_diagram_contents (
				cell_separator=',',
				codec='utf-8',
				decimal_separator='.',
				file= os.path.join(os.path.dirname(gom.app.project.get ('project_file')),
					gom.app.project.get ('name')+OUTPUT_NAME+str(i)+'.csv'),
				header_export=True,
				line_feed='\n',
				text_quoting='',
				type='curves')