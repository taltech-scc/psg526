'''
author: Mihkel.korgesaar@taltech.ee



'''

import gom
import os

for stage in gom.app.project.stages:
	if stage.get ('is_active'):
		gom.script.sys.show_stage (stage=stage)

		gom.script.diagram.export_diagram_contents (
			cell_separator=',',
			codec='utf-8',
			decimal_separator='.',
			file= os.path.join(os.path.dirname(gom.app.project.get ('project_file')),gom.app.project.get ('name')+'_Diagram_'+stage.get ('name')+'.csv'),
			header_export=True,
			line_feed='\n',
			text_quoting='',
			type='curves')
