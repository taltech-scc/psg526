# -*- coding: utf-8 -*-
#Extract_component_surface_info_v1_28052020
'''
Created by Mihkel K.

For specific steps exports aramis component data (pull the interesting component
on the GUI window to activate it) to external .csv file. User can specify the 
steps when the results are extracted. Now aramis does not support numpy, 
so the step numbers list is generated in a different computer with numpy support using e.g. 
np.arange(10,300,10). The generated list can be copied to edit script window 
and then executed. 

v1-28-05-2020


1. Teeb CSV faili valitud steppidest surface dataga
2. Surface component peab eksisteerima
'''


import gom

#Specify the steps you want the output
out_vector=[ 10,  20,  30,  40,  50,  60,  70,  80,  90, 100, 110, 120, 130,
	   140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260,
	   270, 280, 290,300,310,313]


for step in out_vector:
	gom.script.sys.show_stage (stage=gom.app.project.stages['Stage '+str(step)])
	gom.script.sys.export_gom_xml (
		angle_unit='default', 
		decimal_places=4, 
		delimiter=';', 
		elements=[gom.app.project.inspection['Surface component 1.epsY']], 
		export_stages_mode='current', 
#Specify the file names and folder
		file='D:/Measurements/pilt/Surface component '+str(step)+'.epsY.csv', 
		format=gom.File ('csv_stages_surface_components.xsl'), 
		length_unit='default')
