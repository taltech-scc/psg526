# -*- coding: utf-8 -*-
"""
Create a contour plot from ARAMIS component exported to CSV file

-v1 14:39 09.04.2020
    Only one component at a time

-v2 16:50 28.05.2020
    Takes the surface info over the one test at specific steps
TODO 2020-11-26 16:00:39
    Lisa siia kirjedlus milline see sisend fail olema peab!!
        - what kind of aramis output?


@author: mihke
"""
import sys
import os
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
import py_general as guf



# Data
# os.chdir("PEEQ_on_surf")   
fnames=glob.glob('*.csv')

for i,fnames in enumerate(fnames):
    print(i)
    
        
    nr_of_files=np.size(fnames)
    df = pd.read_csv(fnames, skiprows=(4),header=1,delim_whitespace=False,sep=';')

    x=np.linspace(min(df.x),max(df.x),100)
    y=np.linspace(min(df.y),max(df.y),20)
    X2, Y2= np.meshgrid(x, y)
    
    P2 =scipy.interpolate.griddata((df.x,df.y),df.major_strain, (X2, Y2),method='linear',fill_value=-1000)
    # P2 =scipy.interpolate.griddata((df.x,df.y),df.major_strain, (X2, Y2),method='nearest',fill_value=-1000)
    #%%
    fig1, ax1 = plt.subplots(figsize=(18, 5))
    # Sellega paneb paika milliseks pilt jääb
    contourlevels=np.arange(0,165,15)

    #TODONeither option means that values outside the countourlevels are not given color. However, now the interpolation
    #doesnt work so well and thus, some color is given - not sure now how to overcome this. 
#One option would be to consider only values close to actual data points
    CS = ax1.contourf(X2, Y2, P2, contourlevels,extend='neither')
    ax1.contour(X2, Y2, P2,contourlevels,colors=('k',),linewidth=3)  # add contour lines, # CS4 = ax2.contour(X, Y, Z, levels,colors=('k',),linewidths=(3,), origin=origin)'
    ax1.plot(df.x,df.y,'.k',picker=1) # These are actual data points
    ax1.plot(X2,Y2,'.r',picker=1)     # These are interpolate point locations.
    # Make a colorbar for the ContourSet returned by the contourf call.
    cbar = fig1.colorbar(CS)
    # cbar = fig1.colorbar(CS,ticks=np.linspace(vmin, vmax, contourlevels))
    cbar.ax.set_ylabel('Plastic strain')
    
    
    # ax1.set(xlim=[-1.3902, 17.5643],ylim=[-48.3916, 39.6956])
    fig1.set_size_inches(guf.cm2inch(18, 5))
    plt.tight_layout()
    ax1.set(title=fnames,ylabel='y',xlabel='x (-)')
    plt.savefig('Cont_'+fnames+'.png',dpi=300,transparent=False)
    # plt.close(fig1)
# ax1.set(xlim=[-1.3902, 17.5643],ylim=[-48.3916, 39.6956])    
# ax1.set(xlim=[-1.3902, 8.83],ylim=[-2, 20])    
ax1.axis('equal')
#% 
# """
# plot the xy data in single plane to study the the distance between xy points
# """
    # fig2, ax2 = plt.subplots(figsize=(14, 3))
#----------------------------------------------       
# THis adds the step points to plot
# ax1.plot(df.x,df.y,'.',color=[1,1,1])
# fig1.set_size_inches(guf.cm2inch(14, 5))
#----------------------------------------------   
    # if i==0:
    #     fig, ax = plt.subplots(figsize=(14, 3))
    #     ax.plot(df.x,df.y,'.k',picker=5)
        






# def onpick(event):
#     thisline = event.artist
#     xdata = thisline.get_xdata()
#     ydata = thisline.get_ydata()
#     ind = event.ind
#     points = tuple(zip(xdata[ind], ydata[ind]))
#     print('onpick points:', points)

# fig.canvas.mpl_connect('pick_event', onpick)


