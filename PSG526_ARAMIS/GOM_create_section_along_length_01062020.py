'''
In this script the cross-section is created across the specimen thickness through the specific point.
The point used is the frac_location_1 defined in the Extensometer_mk script. The point component with the same
name could be created here as well before running this script.

1. Cross-section is created through the fracture point (frac_location_1).
2. Thickness reduction along the cross section is defined as a curve
4. Component name must be defined
5. Two suboptions: section along length OR width. One or another is commented out. ELEMENT_NAME parameter must be changed accordingly.
6. Stage must be manually set to one in the beginning (or with code, but not implemented now :)

2020-12-14 14:16:13
	TO save data, use the dedicated scripts under datasaving script folder
'''




import gom
import os

#These parameters need to be defined
Component_name='K3-A2'
#This command is needed to create 3D view in cross-section

gom.script.selection3d.select_all_points_of_element (elements=[gom.app.project.actual_elements[Component_name]])

#import gom

#This assumes that frac point is already defined. The code line is created here by left clicking and selecting
#insert-element value (F2)
x_frac=gom.app.project.actual_elements['frac_location_1'].center_coordinate.x
y_frac=gom.app.project.actual_elements['frac_location_1'].center_coordinate.y
z_frac=gom.app.project.actual_elements['frac_location_1'].center_coordinate.z


gom.script.cad.show_element_exclusively (elements=[gom.app.project.actual_elements[Component_name]])


# print ("Frac coord:", x_frac)
# print ("Extx coord:", x_ext)
#
#
#-------------------------------------------------------------------------------
# CROSS SECTION ALONG lENGTH
#Create a section across the fracture point along the specimen length
# The surface component must be active
#MCAD_ELEMENT=gom.script.section.create_single_section_in_view_direction (
#	name='Thick_sec_LENGTH',
#	point1=gom.Vec3d (x_frac, y_frac+20., z_frac),
#	point2=gom.Vec3d (x_frac, y_frac-20., z_frac),
#	project_onto_theoretical_surface=True,
#	properties=gom.Binary ('eAG1mG2IXGcVx39ba7pdm8bUt1JEp5PEbJrO7mbTRp1ss2nejCWxoW7TEjTD7M7d3TE7L5mZze5WY24/GVAofrARPymxIgQL+SBWROoXQaSkIGhF8YOo6IdCJYItQhv5n+c+c++d3J3dSJ1h2WfufZ7z8j/n/M+5d6pRK5x8eGSZo4cfPXT4CaZa5WqnXSweXghqQb1zotVoBq1ONWjjPgNsYOMQQ2xkaCNwsPja6XngM48fz32+MdtZKreC3PjYrk/njjY6s9Xl3B537tqVQXbb+Vv5DNwPTI9cuzLAR4G7IKxQpU2TBcqsUKJNlWcIOH/y2pXbuA94H4SHUruO06BCwKkHvKR7zaQB7gbeC2GbGmUWWOBXX3zjId05CuT76CtRo0qdanSyRJVZSixSp03AAgEzdAioMHxBtt0F3A7hNA0aLHD6RekRIAPhRS3HgY9A6A7XKJngKnPUqRFQp0OJFgFnWaRqqwrl8/2FEw5KuMcvFj5Dg1pX8Av7Jeb9wAbDo0PL/Jtj+Pc6f7uLW8hAtqnzVA3jJA5OQ5PFCIdfTKzL1A8Dd6ZMbTNPgyX+cqm/gAjICZC/CV/lT5k5s69F2fxy6wDvq6JVpUGdpyxeDwAfgHDOvBjhcab5UhTVJwiYJbDzdWYIOGa5Jfwsog4t+2y2TMsDm1I2SWu5K7FBixVe+XF/B6NgjgAfglBFMG3JVmI3FUpMU2aGM5Q4ZyVQZdqy6blPrkvwMHBPpmDVnbyNUfprXSJ9sDxKJ22PjDm7U6Z+wtWRYaCqc5+3bvT+v/joz+/Vnq0gFDNcE9JyV+5U+Pt31+WQh/1mpOoIcle8z739v7viHEh+BvmTpaFc35ypu0nDYuMT7o9vSr933UN50Kpf0ZTDJ2hYQXYYuyTpB4FJA3YDDyfV38r6h7e9/kZ/1NvMRDSkmmn8WoZ6lqjQYDGKyLenJGewq/yfk3tDXVk9VcUJy0ZpizSjhA1YYvP8/yMYq9uxZIEQf9Wtn6ia6wS89DXZsTprR7Xoo5bMMF+Fc7QMojoVflKQuM8B2yCcshgXKXLEGH7c1se69XyAsrURXdW6V9pBayHK4Gd/KStUuLJEOuIgEM73XmGY4Pv+RG+pqzW1jMZblBB9Ox3HX80KSWzDrqcl8R6whqp11C5uTD57ebt+Z9VCWlstatIXJqVLVDEE4YGERb6NbzINavUiH619kFTTCl3p+ld/qutZsZFXddsVU9mrB7L8W4vK7nvrR/+6/Of79/Obc88/9oUzk+HVy3f8YOf3Xv7b3ae+Lu2fAlmZyoy0dmFbiopL7UipWLFkfGaDLMo5REMhHdusU091d/72Tela31DjUU3m6qzJ7vCOEeoHjb9iDjoS3f3mt6RFQ9GhCPNi1IMcW6lwVqyVusklS0OXG4AXX9/0cUkZc1FMWeSrphmRTskKMqb+6y/LUh/zVcepx4Ata4puc4YqTZpGsiXmCSh3S8CHps3Z3yWVavwSLE9flRNKdnX8/Zv1KwtkP8C8s5KUsqrpPu5JFB2ybvDV+uqoRHkq9kNMPLDFo7BGFeWMep3vOP9+W6Yqlrk19JRoRvwTD1C9w8/zI7LlQVD+dPvXidS5m0emo9+QDdPA6Yi5phyLhWOkv4Xu7/6r9CnlFqH+vnJ+xULjR7p+uKodKbwVXjsum/yAEzP2kzzJZ6lQNI45BZyMauLYu2z/hZf+8DPJzUop3z5v/Kd/HvzjDrj+HsIJ5ulQY4F9aO3SXGs9W2g8zhnjK61Vbx0eIc8u8uSs+rRLV85Gw7KkOQvyjJoc5eCKDQM5W6mkdMLvG2WGNm3y7GOIO8lF3yYPkjMWzPFlcixZM9apgj3qKfvUCbVTI2iBJRudm+wlx/murAlGEzbIotGUnyq3CivkErtkn2dAyQ2sG8u3Ig9Z1klHvCP2sUhygtxrXk0Yjm4gyjHNXKKHStMWZhNfISvP9NgpLytoyFYFa++Y3U/2SF118fCnPDrxqUK0I7ZTp5JSCgmbimxhhrHun3xN743lFGnbPKDK0D6xSYs5012gQ4MmRcZospxxX1I7tktTz2q7hNusYb/aDuWej4/f45FvRf8rUSZ7fOqG53iEZ29M5JGTqL4TsJKB3zmbiMSdbhguJJ7Mi/YiQLGT9UImHcuCXXde7cq8n/Qpe0eMbpb8XmxjROJc9BmTtm31PHPZ50+tN8/eLZxcJqZtLRjvxLHP2uFxUhVk3c9CSnzjpy5XYar6bZlZFb+P0U4fV5d/Ptd6ay/mjjS77Llldtma6uM5hru8LP7Ms9NqX2s/bqiX6RWVuHzWclbPFDvYatzoLZb1o1Y3cf2kPY2R7/V1vfXUT95Ws0oPgrNUWY4GsZhJ9STi8jA5Icv/cfMljcswwxYZRWLY2El+Jqf9NiWbL6WxYS/tlDc6414NSdsO9jHGDiYZJs/FbRe35dlBzrpQ3lB2DKO56hG2s4UjHOlOKdvXwFhoO7bSylep/+3i0G+P8lidzO0Td7m+/p2apoWPARt7phxXmXrnoF43s0czg5+g+w2z/nmgiVCLfb60OzkbxW8sPCZrPxF6K53kimGvjI1H7pUXklbKZ0m/6bWpH5Z7Bekli8Lfoc3AK/1FEQ7+F45hUvoBRp8='),
#	rotation=gom.Vec3d (0.02353781052, -0.3369546945, -7.104140111e-05),
#	scale=13.83545971,
#	translation=gom.Vec3d (-11.31405463, -5.589045037, 123.4017401))
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# CROSS SECTION ALONG WIDTH
#Create a section across the fracture point along the specimen WIDTH
MCAD_ELEMENT=gom.script.section.create_single_section_in_view_direction (
	name='Thick_sec_WIDTH',
	point1=gom.Vec3d (x_frac+10, y_frac, z_frac),
	point2=gom.Vec3d (x_frac-10, y_frac, z_frac),
	project_onto_theoretical_surface=True,
	properties=gom.Binary ('eAG1mG2IXGcVx39ba7pdm8bUt1JEp5PEbJrO7mbTRp1ss2nejCWxoW7TEjTD7M7d3TE7L5mZze5WY24/GVAofrARPymxIgQL+SBWROoXQaSkIGhF8YOo6IdCJYItQhv5n+c+c++d3J3dSJ1h2WfufZ7z8j/n/M+5d6pRK5x8eGSZo4cfPXT4CaZa5WqnXSweXghqQb1zotVoBq1ONWjjPgNsYOMQQ2xkaCNwsPja6XngM48fz32+MdtZKreC3PjYrk/njjY6s9Xl3B537tqVQXbb+Vv5DNwPTI9cuzLAR4G7IKxQpU2TBcqsUKJNlWcIOH/y2pXbuA94H4SHUruO06BCwKkHvKR7zaQB7gbeC2GbGmUWWOBXX3zjId05CuT76CtRo0qdanSyRJVZSixSp03AAgEzdAioMHxBtt0F3A7hNA0aLHD6RekRIAPhRS3HgY9A6A7XKJngKnPUqRFQp0OJFgFnWaRqqwrl8/2FEw5KuMcvFj5Dg1pX8Av7Jeb9wAbDo0PL/Jtj+Pc6f7uLW8hAtqnzVA3jJA5OQ5PFCIdfTKzL1A8Dd6ZMbTNPgyX+cqm/gAjICZC/CV/lT5k5s69F2fxy6wDvq6JVpUGdpyxeDwAfgHDOvBjhcab5UhTVJwiYJbDzdWYIOGa5Jfwsog4t+2y2TMsDm1I2SWu5K7FBixVe+XF/B6NgjgAfglBFMG3JVmI3FUpMU2aGM5Q4ZyVQZdqy6blPrkvwMHBPpmDVnbyNUfprXSJ9sDxKJ22PjDm7U6Z+wtWRYaCqc5+3bvT+v/joz+/Vnq0gFDNcE9JyV+5U+Pt31+WQh/1mpOoIcle8z739v7viHEh+BvmTpaFc35ypu0nDYuMT7o9vSr933UN50Kpf0ZTDJ2hYQXYYuyTpB4FJA3YDDyfV38r6h7e9/kZ/1NvMRDSkmmn8WoZ6lqjQYDGKyLenJGewq/yfk3tDXVk9VcUJy0ZpizSjhA1YYvP8/yMYq9uxZIEQf9Wtn6ia6wS89DXZsTprR7Xoo5bMMF+Fc7QMojoVflKQuM8B2yCcshgXKXLEGH7c1se69XyAsrURXdW6V9pBayHK4Gd/KStUuLJEOuIgEM73XmGY4Pv+RG+pqzW1jMZblBB9Ox3HX80KSWzDrqcl8R6whqp11C5uTD57ebt+Z9VCWlstatIXJqVLVDEE4YGERb6NbzINavUiH619kFTTCl3p+ld/qutZsZFXddsVU9mrB7L8W4vK7nvrR/+6/Of79/Obc88/9oUzk+HVy3f8YOf3Xv7b3ae+Lu2fAlmZyoy0dmFbiopL7UipWLFkfGaDLMo5REMhHdusU091d/72Tela31DjUU3m6qzJ7vCOEeoHjb9iDjoS3f3mt6RFQ9GhCPNi1IMcW6lwVqyVusklS0OXG4AXX9/0cUkZc1FMWeSrphmRTskKMqb+6y/LUh/zVcepx4Ata4puc4YqTZpGsiXmCSh3S8CHps3Z3yWVavwSLE9flRNKdnX8/Zv1KwtkP8C8s5KUsqrpPu5JFB2ybvDV+uqoRHkq9kNMPLDFo7BGFeWMep3vOP9+W6Yqlrk19JRoRvwTD1C9w8/zI7LlQVD+dPvXidS5m0emo9+QDdPA6Yi5phyLhWOkv4Xu7/6r9CnlFqH+vnJ+xULjR7p+uKodKbwVXjsum/yAEzP2kzzJZ6lQNI45BZyMauLYu2z/hZf+8DPJzUop3z5v/Kd/HvzjDrj+HsIJ5ulQY4F9aO3SXGs9W2g8zhnjK61Vbx0eIc8u8uSs+rRLV85Gw7KkOQvyjJoc5eCKDQM5W6mkdMLvG2WGNm3y7GOIO8lF3yYPkjMWzPFlcixZM9apgj3qKfvUCbVTI2iBJRudm+wlx/murAlGEzbIotGUnyq3CivkErtkn2dAyQ2sG8u3Ig9Z1klHvCP2sUhygtxrXk0Yjm4gyjHNXKKHStMWZhNfISvP9NgpLytoyFYFa++Y3U/2SF118fCnPDrxqUK0I7ZTp5JSCgmbimxhhrHun3xN743lFGnbPKDK0D6xSYs5012gQ4MmRcZospxxX1I7tktTz2q7hNusYb/aDuWej4/f45FvRf8rUSZ7fOqG53iEZ29M5JGTqL4TsJKB3zmbiMSdbhguJJ7Mi/YiQLGT9UImHcuCXXde7cq8n/Qpe0eMbpb8XmxjROJc9BmTtm31PHPZ50+tN8/eLZxcJqZtLRjvxLHP2uFxUhVk3c9CSnzjpy5XYar6bZlZFb+P0U4fV5d/Ptd6ay/mjjS77Llldtma6uM5hru8LP7Ms9NqX2s/bqiX6RWVuHzWclbPFDvYatzoLZb1o1Y3cf2kPY2R7/V1vfXUT95Ws0oPgrNUWY4GsZhJ9STi8jA5Icv/cfMljcswwxYZRWLY2El+Jqf9NiWbL6WxYS/tlDc6414NSdsO9jHGDiYZJs/FbRe35dlBzrpQ3lB2DKO56hG2s4UjHOlOKdvXwFhoO7bSylep/+3i0G+P8lidzO0Td7m+/p2apoWPARt7phxXmXrnoF43s0czg5+g+w2z/nmgiVCLfb60OzkbxW8sPCZrPxF6K53kimGvjI1H7pUXklbKZ0m/6bWpH5Z7Bekli8Lfoc3AK/1FEQ7+F45hUvoBRp8='),
	rotation=gom.Vec3d (0.02353781052, -0.3369546945, -7.104140111e-05),
	scale=13.83545971,
	translation=gom.Vec3d (-11.31405463, -5.589045037, 123.4017401))
#-------------------------------------------------------------------------------

ELEMENT_NAME='Thick_sec_WIDTH'

#Show only Thick_sec created here
gom.script.cad.show_element_exclusively (elements=[gom.app.project.actual_elements[ELEMENT_NAME]])

MCAD_ELEMENT=gom.script.inspection.inspect_dimension (
	elements=[gom.app.project.actual_elements[ELEMENT_NAME]],
	nominal_value=0.0,
	nominal_value_source='fixed_value',
	properties=gom.Binary ('eAHtmVtsXFcVhj9XkKa5kIYUKBTBMHFiu6ljx00DTOI4dxJI2ihJnShSMrI9Y3uIPTOZGdd225DpUyO1atSHUp5IKy5CClRUgChCFF5AFSWpQIIiEBcpUF4iFYpEEVIb9K999pwz4+OxnRTBA7Ys75mz97qvf629zpHCeGf/Peun2Lt7+67dhzhSGshVyqnU7rHseDZfOVgqFLOlSi5bxv20sIjlS1jCcpYsBzo/8+rJUeBT9x1IHC4MVyYHStlET/eGTyb2FirDuanEJnfu8sXF3G3nF/LTsg4YfOLyxRbuAm6DaoYcA4xQYoBx0gwwRIUJBhgjzRAFxihQ4vgfLl+8ifcDt0B1hALjrGdn7fmGY6L6XuA9Jl4L7wok29Zy5WZ9/giwPIZjyOPnF66Hx7G/L7229tlvdYnHncCqGB7jDDBFmmnSpmmeEbJc+YX47QdaoXrEdEqRYg85Ruix9a46+0jfPMP2fMIoVcjZd/1msQmypEz7DzhJbH1L1FGPV8e+2lzUHPkZor723xR1PfC+GKvmzWIStzFalj12PZ6MRsuHgWUBzzJFxhgw75XJ8SBZzvSLw4eApVCVl8JdByiQIcvxO2VoxZ32aX0rsAiq42QR5QnGeezlS9/QM88xyxhZywVF5jjam6fCV7aJo6dQpkLJdB+h/dc6H8R7lRZ96gHFQITaKDmTKk2OYdJMkA84FJmgYhL9aIt4LANRqw5SsPw6+ZwoWqpXF2vp0zAUtcwoBSa58nRzAi3VcyKwBaRvHYGK4YDkCzNE6yxeVyGDi/ajZ8XGZ5tHg/sY5LNkbd8hsgyTtfPSM8t+84bsJ02iwLXSfJMEVtTJJK4DNYpCoWl+9p3mCgYW8iGrsBlEdkpzNxnSDBrEnSLNAxY0OQbN4ec/Pi/C7SCUiyGseJK2oZX+lBdJ7yxvpX7bI2FOr5Ooa8F01/p2F0PAP681/j+3/Qe3a08ryIoxEsjSUlfqZPjLM/NSyJt9pqWU3iUEnWOcf+v6VXEKRH8W8zsLQ6m+MpZ3kYL5xgfcb98Uf6+6N+VOxiwsT5nCBylYQlboflrUdwJ9ZthF3BNlv5D11266+npzq5cZMhM5LCj8VIJ6lMhQYCLwyBeOiM7iGvO/9m2u6pvZQ1WYMEXFsKIYBGyWSVaO/iecMbsck+YI4ZcKk/RVpGV54VHJMSdaea9FI8xnoRoPmShPhu92ity9wJpZy/H+Wj7vYICyldsUWjdSC1uTR34sMytxJYl4hE6gOtr4De1kv+xPNKa6ILlkMF6qa48OvBLnklCGWduja32PfKlN3OJyoZ7beFDWzvaJl6BiCVR3RCTyhW+FgYmKnsBHa+8k5bRcl37jc9/T93G+UVHK264Qyl7ZEaffXFD255v/9tTDq5LbXn70JxdfvJzcdmFpyx2rjn67b+uFQ9aufcKV5rrIqOcua6eD5FJpUihmLBgfXCSJEq7hrLrGzMusU0drO3/5pnipvMtmWqtFfbcVcYduY7x04vWNeuKtGo3VYaNd4W0D1NsMv0IM2hM8ffIpnd8L7Aq4pIIa5NBKiTNtpdQ1F3EcatgAPHd1xUdFpdt5sU4inzXFAHTSlpAh9L/xQ0nqfT6zjQi6gE8Dq+ckXeYUOYoUDWTTjJJloJYC3jVlTv8qynTCvq9w7HlxUm+kir9tpT7FGdk3MG9PR6nMKrr3e9SKzrKuVdT6+S6R8lDsm5iwYQubR7Uqvtb5ivOPtySqfJmYg0+aYoA/YQPV2Px8fr1k8ZcuX78O1p2b2TLtfVwyDAInA+Q6ElxWuqn/7ax9br6qP6XYoqq/h89Mm2t8S9fMripHcm+GVw9IJt/ghBeo+7mffWRIGcYcB/qDnPCXrUY5mksdPm08d/aF33xfdONCypfPa/9qHgcvLoMTi6huYZQK44yxFa1dmGutK4Da44Q17Apr5VuFXpJsIEnCsk+79M3poFkWNSdBki6joxictmYgYSullE74fV0MUaZMkq0sIRH5LXIXCcPBBA+RYNLKsc512sVH8adaqJ1qQjuZtOa5yGYSnIlQ20JXRA5J1VWnq1IuwzSJyC7J6FFQlLN2AZZ+KTZa5IlLuCPUM0W0i9xsmm0xW7qmKMEgI5ExgzitZjjyK+tKN6G09MzYxU1ZrL3d9jxaJ/Wt84k/5e0TnuoMdoRy6lSUSmdEphSrGaK79idd6/eGdFKU7cqm7NA+IUqJEePdaZWpSIpuikzFPBfViu3SIGK2XbLbsNl+th2KP++fcE+99TrN/p6OZG18Xk8lbkeoT9zTRm3k/QQlJoxz2fzn+xEfF6XgfybINe+9vO3uCbzdGDGyt9NXlTHLdIx3H7CeTeju2vXOoGkfIU/KxhiKLEkWp0toqw2xz6O2it+xcFs1ZoqP53o/yS7xWeC+9afmmwXvlJ1uNKaUo3GeiI+qsC+UPTYYJq2JjarQ69rp/eriz8daIzKEyFaPfZsWjH2tdZ1GgvZa5RC+J1lnyKS1b4jcoEhY38uwxaxuPR20GnJ7iSV9l+VNmD/1moYR2qjrfPOpGb1Wk0pXVQ1Hp4JWMcR5DdNcHEZ7eOnfY7rU26WddvOMPNFu2Ck9o/eRMmnrgMWxYOM05ZfOuOGVuHWwlW466KOdJOfWnFuTpIOEVcmkWdkhjDq/XtpYzR721PqotjlsLGs7tNLK1zP/2fkh3OP3OmSrt2QYg41x97+Si/G5HJeJ0tHHZKM272QWlShSZj0TFneKNUWJokxjeTd0VGyElvZj3AR91n2VaLdRhnaoRiuCXEemU4MMGhWNXMI7QQfrUGy6obS87iJttjFpL73s41762c5+9rHLorGNNlKBBPOl1IHjLb7S0t0ldYssBDr0kaSNJCmL7Wicz0SKuT0U7WhmdnCtVrdly//74Ub8ECKFcMHdEULM0KoeV5Rx6s3dPvU77rZy/oTuQP4FW/Tu5hBEk1T1WUObdBPyc4FmV3S9ZvwgVJVlyq0ey7GMoatqkvwfffWjTsqNAR7qjfKQxMLY8BVGMHuIZzAbWfcuxkF1z6nopdNfpRcyavOXXKdc3HuYKL+1r90oPz9ZVVLJgA6qBC/usiYIEzSFQPOxJ8XTDyC9jocj5w/bsEMA5FraZ+wdlsZ5Go1o7a/luiTqMqomwkHFHy998Zva4e3gSqfcmwvedvmAcQCjpqfE1Zxk8uOUZpNtDdpEf6Mbuc2TvhoDAXGeticWymk+mmhcJngv8fWxhdKfjyYh/VCTtb9fKCf/Zq6ZT1zRC7nISypVarBaM+LoU3xm+gXvx/zcd2bXpDmlOquSVbZn7xA5/45IE576HutwZO/aHh9UmmVr7V+pKq3jOrSXbn300r8BhNE1rAGPnA=='),
	type='thickness_reduction')

