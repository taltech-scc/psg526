# -*- coding: utf-8 -*-
# The script creates an extensometer around user defined fracture point. This point is created first and then extensometer is created so
#that fracture point remains in the middle of the extensometer. Create this point manually and give the name: frac_location_1

#Script for extensometer
#
'''
v1-2020-12-26 20:18:14
	Suitable for measurements with old ARamis.

	
	
'''

import gom
import os

facet_size=15
compute='more_points'

# Asks a user to insert the legth of extensometer
RESULT=gom.script.sys.execute_user_defined_dialog (content='<dialog>' \
' <title>Enter extonsemeter length</title>' \
' <style></style>' \
' <control id="OkCancel"/>' \
' <position></position>' \
' <embedding></embedding>' \
' <sizemode>fixed</sizemode>' \
' <size height="111" width="273"/>' \
' <content columns="2" rows="1">' \
'  <widget type="label" row="0" rowspan="1" column="0" columnspan="1">' \
'   <name>label</name>' \
'   <tooltip></tooltip>' \
'   <text>Extensometer</text>' \
'   <word_wrap>false</word_wrap>' \
'  </widget>' \
'  <widget type="input::number" row="0" rowspan="1" column="1" columnspan="1">' \
'   <name>extensometer_length</name>' \
'   <tooltip></tooltip>' \
'   <value>50</value>' \
'   <minimum>0</minimum>' \
'   <maximum>1000</maximum>' \
'   <precision>2</precision>' \
'   <background_style></background_style>' \
'   <unit>LENGTH</unit>' \
'  </widget>' \
' </content>' \
'</dialog>')
user_extenso_name='User_extenso_'+str(int(RESULT.extensometer_length))
#Create an offset point from frac_location_1
MCAD_ELEMENT=gom.script.primitive.create_offset_point (
	direction={'direction': gom.Vec3d (0.0, -1.0, 0.0), 'inverted': True, 'point': gom.Vec3d (0.0, 0.0, 0.0), 'type': 'projected'}, 
	name=user_extenso_name+str('_offpoint'), 
	offset=RESULT.extensometer_length/2, 
	point=gom.app.project.actual_elements['frac_location_1'], 
	properties=gom.Binary ('eAG1mF2IXGcZx3+b1rhdG+O2fhQRO0wSM2k7u5vt92SbbZsPoyQ21G1aCvYwu3Nmd+zOR2Zmu7vVmFO8MBdC8ULrpaCCECj0QqyI6I1QROKF+IHihSgoWGjJhVWhjfyf97xzzpk9+5FSZ1j2nZn3fT7+z//5eM9cu1k+d+/EGqdOPHL8xOPMdauNfq9SObEcNsNW/2y33Qm7/UbYw71G2M2eMcbYw9geYNd/vhosAZ9+7Ezh8+16f7XaDQvTU4cfLJxq9+uNtcL97tyVy6Pcbeev5zVyO1C9cuXyCJ8AboaoRoMeHZapsk5AjwbPE3Lh3JXLu/g48AGIjmd2naFNjZCn7/CSbjOTRvgg8D6IejSpsswyr33hjXv0yymguIW+gCYNWjTikwEN6gSs0KJHyDIhC/QJqVG6KNtuBm6EaJ42bZZ55mXpMUCiUS2ngY9B5A43CUxwg0VaNAlp0SegS8h5VmjYqkb1wo6Ee/wS4Qu0aQ4Ef/9hifkQsNvw6NM1/xYp/UHG3ejiFjGSb+oSDcM4jYPT0GElxuHnMzsy9aPATRlTeyzRZpW/vrS1gJHokqybAUU55av8qbJo9nWpml9uHeJ9VbQatGnxpMXrDuBWiBbNiwkeY54vxlF9nJA6oZ1vsUDIaeOW8FNE0zQfN6YVgb0Zm6S1OpDYpss6v/rh1g7GTJkAPgKRkmDeyBZwNzUC5qmywLMEPGcp0GDe2PTi/TsSXAJuyRWsvJO3CUp/a0mkD5ZH6ZztkTHn75Spn3J5ZBgo69zr39eG/1965Ke3ac9+EIo5rglpuSt3avz9OztyyMO+EakWgtwl74tvv3tXnAPp1yh/NhrK9fFc3R3aFhtPuD+9Jf3edQ/lMct+RVMOn6VtCdln6iVJPwbMGrC7uTet/nrWP9j1+htbo95jIS5Dypn2L2WorxI12qzEEfn2nOSMDpS/OXsk0jebU1U1Yc1K2gqdmLAhq4wv/T+CsbkdqxYI1a+W9RNlc4uQV78mO7at2j5qaYb5LFykaxC1qPGjssR9DjgA0ZzFuEKFk1bhp219epDPj1K1NqJvtR6WdsxaiBj8wi8EsxJXlkhHEgSipeFvKBF+z58YTnW1pq6V8S4BKt9Ox5lf54UkseHwU5J4C1hD1TpuF9dmX/juQX3Oy4WstmbcpC/OSpdKxRhEj6Ys8m18r2lQq1fx0doHSTmt0AVXv/Jjff8AaF8mNvKqZbtUyuRdENNbDUFkqBkdnt8tOwrOp0i+Zk89Odj527eka2djhfcrzZa6ye7zjpW0D1sFSarAyfjXb3xTWjSWHI+9rsRdwNULUXfdmpmbHfI0DLITePn1vbdLypTDMWOR520nTvvAUiIpvld/Jks96hsHmrgPfxbYt63oHs/SoEPHylzAEiHVAQl9aHqc/11aqQYgwfLUK9IkuqnnPjyuT3kg+xHinfW0lE1N93FPo+iQdaOn1q9MSpQvhn6MSEamZBjVsCCmqdv4mv+vt2WqYlnYRk9AJ64AyQgzPH58a0K23AXiz6CDnM2c2zi0nPq6bJgHnolrx5yrI9EU2Xd58HnrVfaUuEWkvy9fWLfQ+KFqK1zVEBTeGr8/I5v8iJHUzCd4gs9Qo2JZ/jRwLs6J0++x/Rdf/eNPJDePUr6BXfvv1jz4x/vh6g1EMyzRp8kyR9Ha0VxrTfcaUAuWZfr0EEXOx2OpTjlNRQpWlUV8ZWTf9h2myKTJEQfXrR0XbKWU0g5/fpIFevQocpQxbqIQvzvcRcGqYIEvUWDV2qFOle2yJfapF2mnhsAyqza8djhCgQsDWTNMpmyQRZMZP5VuNdYppHbJPl8BJTe0fiifK9xjrJOOZEfiY4X0DHfEvJoxHN1IIqw0Gy+bBzU0wio7pXHKsExk6bt0Pyqnul+FfSwwNfiTPdm9iZwKPeuaYq/2KeO7LJrmsvWbDhWm6LCW87uk9m2XZoPNdsm3uuGz2Q7xxmPo9zjuODx8TBM8ysYi+bWY8lyo7KOeers9yZSgHTrpkO/G/2sbsH3OZgrVPjdOllN324pdpRUfeSbUsvEq2/fO48O5v6f9zd+RIJ8nfxh3h9ZGLBRXh6s6ZMh6jKoi7jBtGbumB4i8Wy6+V3g5tg7jmcYrf4fHS7zIk5CPWDI9OV4oew/EGG3PvCyrlKEbWZU8EZEGzwu300dgOK+T2pGtLvddd3XZn+njBUqDeq36WeROywWt/bihXqaHRKrUdeO8pvpD7Lfa6C2W9ZOWN0n+ZD1NIjbs6+b7dsrgo+w3q3QVq9NgLR7Ekkqqu4CLX3pClv/T5ksWlxIli4wiUbIaIz/Tjw56BDZfSmPbHpuJbzrjHs5I2yGOMsUhZilR5NKBSweKHKJgXahoKCf3k4c4yD5OcnIwpRzcBmOh7aqVVj5L/WcXh632iP/qZG6fKoLr669NaFr4JLBnaMpxGa1bv3rdwn2aGfwEvZNhVp1XDxvT97K/PJiej5LnBv7eln8vu/U3vWsPjP/zBlnqr8V5D6MCahYPsVg3Wtc5jr+Ztlw4SFvyMJNo9H/cOigYATWN'))

gom.script.sys.show_stage (stage=gom.app.project.stages['Stage 2'])

#Create an extensometer
MCAD_ELEMENT=gom.script.deformation.create_extensometer (
	computation=compute, 
	direction={'direction': gom.Vec3d (0.0, 1.0, 0.0), 'point': gom.Vec3d (0.0, 0.0, 0.0), 'type': 'projected'}, 
	facet_size=facet_size, 
	image_point={'pixels': [{'image': {'camera': 0, 'measurement': gom.app.project.measurement_series['Deformation 1'].measurements['D1']}, 'pixel': gom.Vec2d (1032.043656, 790.953216)}, {'image': {'camera': 1, 'measurement': gom.app.project.measurement_series['Deformation 1'].measurements['D1']}, 'pixel': gom.Vec2d (877.1271259, 793.1736617)}], 'point': gom.app.project.actual_elements[user_extenso_name+str('_offpoint')]}, 
	incremental_facet_matching=False, 
	length=RESULT.extensometer_length, 
	measurement_series=gom.app.project.measurement_series['Deformation 1'], 
	name=user_extenso_name,
	pattern_recognition='standard', 
	properties=gom.Binary ('eAG1mFtsHGcVx39uS0hNQ+qWS4W4DBuHOG03dtz7xonT5kKKEhoFN60q0dXaO2sv9V6yu67tgsnkqRECVTxAeURqQZUiVeoTRVUFL0iIh/BQcRGIBwQIJCqK8kBRpTbV/3zz7cysx06Cyq4sfztzvvOd8z//c5mZaTWKp+/Zs8KxIw8ePnKKmU6l3uuWSkcWw0bY7J3stNphp1cPu7jPEFvYNsww2xjeBrx08DvlBeCLj5wIvtKq9ZYrnTCYnNj7QHCs1avVV4L73L6LF7Zyl+2/ls/QZ4DKvy5eGOLTwE0QVanTpc0iFVYp06XOM4Ssnb544To+BXwEosMZqRO0qBLyxO1e021m0hAfBT4EUZcGFRZZ5Jdffetu3ZkEPglRyCIhDcp2v848TRqENOlRpkPIGZao26pKZU1W3ATcANEsLVos8uTL0miuR1u9CXImUT5Hi0Zf8Y8OSs3NwBYzrkeHOk3mGfu99t/gQIwYyjd1gbo5XKZOjTJLNHEntFmiZ6b+fOqqTA1AKKVMlSHeIKE/x7kfS9UpYBdEM3ZQiRLHCZk3rKpM2hXFxaGnnS2aPLxOm+Rm6FChTo+VZ+XiKCBLtP4EcCNE6+1441trn09LJPB2WaDFMn95fnOnh6LzUjAFilHKacWgYs4o6BWLhVuH+PjM0YudeuysjrkduBWieQNkD48wy9cITe4UITVC26/YhBw3cirmIko6T0bM7T3AxyES8WeRa2XuokqZWSrM8RRlnjba15k10j533+bOxmQcA27JVaxck4GJY39tSqWPgHfstMnImDN3SOMXgEIcLWWa+/z38uD/8w++fptkFN2bcy0QOHJX7lT5+w+vyiEdvT1XXZMWnTjVn3v3f3fFOZD+bOVPxhy5PpJ7dpuWxcZz5I9v63zvuofykBUZRVMOn6RladZj4nlpPwRMG7BbuCd9/LWsX7ruzbc2R73LXFztRPPWr2SoL0ZVWizFEfnBjPRs7R/+7+l9ka5sTFWVnhWrnEu0Y8KGLDOy8P8IxsZ2LFsgVCab1kOUgE1CXn1WdlyxfvuopXPRZ+E8HYNIRe8nRan7MrAzUxiPWiNxJfF4P58fokKX0K5qPajtkDUTMfjcLwSzEleW6IwkCEQLg1cYI3zR7xhMdTWpjnWLDmXrEu6ME7/OC0liw97HpfEW1x4s1eOudHn63Au79DsvF7KnNeLGfHZaZ6lUDEP0UMoi37q32wlq7yo+WvsgKacVuvKlb/5U1+8HyWViI6+aJqVSJu/KMb1Vw0WGqtHhmS2yw7c8+Zrd9Vhf8jdv66yrGyW8X2m21Ex3j/espH3MKkhSBY7Gd7/7PZ1yDDgce12Ku4CrF6LuqvUfN6LkndDPTuDlN7d/TlomHI4Zizxv23Haly0lkuJ76Wey1KO+frSJW+eXgB1XVN3lKeq0aVuZK7NASKVPQh+aLmd+mz5Uc5ZgefwVnSS6qU0eHNGvPJB9139vNa1lQ9N93NMoOmTduKn1K+NS5Yuh7/zJZJYMoOrvYpq6ja/5/3lXpiqWwRXOKdOOK0AydQxODN/fI1vuBPGn30FOZvatnzOOfVs2zAJPxrVjxtWRaILst9j/vfkqu0vcItLfN9ZWLTR+DtoMVzUEhbfK707IJj9iJMPkozzKw1QpWZY/AZyOc+L4B2z/2Vf/8Jr05lHKN7DL72zOg398GC5dTzTFAj0aLHIArR3NtdYYrJkysJorWivfeuynwF4KBJZ9ktKVM/GEKW3OggLjpkccXLV2HNhKKaUdXm6cObp0KXCAYW4kiL9t7iSwKhjwdQKWrR1qV9EesMQ+9SJJaggssmyTb5t9BKz1dU0xnrJBFo1n/FS6VVklSEnJPl8BpTe0fijfStxtrNMZiUTiY4n0DLfPvJoyHN1IEjDLfKqL6aQd1FJfISvP9LAnL6tozFUGS3bC7qe7lK66ePhdHp1kVzGWSOzUrrSWYsqmEjuYY6L/J1+zsomeEl3ryMoMyamadJi3s4vWy9qUmKDNSs59ae2ZlB7INpISbjXDfiMJcc/Hx8t45Dvx/2ou9rLc7VR/CVmNkZRPDsem4T6Zg9/TNpOodrpxtJh6BC/RMEyqFnUhk41l0a47r/bm3k/7lC+RoJunfxDbBJGEi54xWds25pljXzKPOO4pH3bGuF0bAz8oBB1Hs14UrSIlrMiT8AgqP/Lub4xhmlXJixfh4ePq+Oc5NJh7Se3IVpd7r7m6jGb6eMBYvy6rfha4w3Jfaz9uqJc1rZbup2ac1VS/m1Grjd5iWT9ueZPkT9bTBN9BX9fXuPw820zfqFmlR7EadVbiQSyppHoWcGxLT8jyf9J8yeIyxphFRpEYs+okP9OvDrqUbb7UiS17KyV2aI97n6LTdnOACXYzzRgFzu88v7PAbgLrQgVDOXk+2c8udnCUo/0pZdcVMBbajlda+Sz1v10cNpMRW9XJnJxqmuvrfxvVtPBZYNvAlOPyz7/qmrtXM4OfoK9mmFXn1Quz9HPZnx9Iz0fJewP/3Jb/XHbrG93L94/88/r3AWQdEVMB2nU='))

#Create a dimension: distance between extensometer points
MCAD_ELEMENT=gom.script.inspection.inspect_dimension (
	distance_restriction='xyz', 
	elements=[gom.app.project.actual_elements[user_extenso_name]], 
	nominal_value=RESULT.extensometer_length, 
	nominal_value_source='fixed_value', 
	properties=gom.Binary ('eAHtml+MVFcdxz80tAIF21KsVauOwy47W7p/i7QOLAsICAqtFkIbjEx2d2Z3R2ZnpjOzsFutnYYHiTYmxpj6prXaB2matC+NiYnVxKSJEaOJpk8aQ2rUkNTUxEYTi/n+zj177529szvLUvXBmQB37jnn9//3/f3OOZyozPSd/Gj/HIcP7jtw8CFO1MaKjXo2e7BUmCmUG5+uVaqFWqNYqOM+a1jLpg1sYBMbNgHfePa509PAJx48ljpemWycG6sVUsODQ/enDlcak8W51H1u3aWL67jX1sMr3+21d3t/cOUGe2j7WdMD9HRduriGjcBaaE5QZ546n33i0sUbuBu4HZpTVJihnwcZ5/MUmKDBQxSYpECNAmUmKHD0blG6NRBDf/znNjRyD7AFmnmKjDFFjTFmyDFm9GYZo0SOCSqUqFDj1O8lwx3A+ogMH18YH3pEVDcD7zYTyHjus3fN5Xfp94eATQkcQx6/+va18Lj5b9NXd33m57eLh7dRq1YzjDFHjnlypmmZKQpc/rX4HQW6oHnC7JolyyGKTDFszwdi9pG+ZSZtfNYoNSjau5NmsVkKZE379zpJ7Hl91ANPNUvPLS1qkfIiUf/43xS1H3hPglXLZjGJ2xotG796LZ6MRsswyIaLPHmWInWKjFOkRJGG+XSGCnkKvH5efMeBgVV7VN4/mcDtWMDrzx+UG/cDewI/7wDuhOY5pi0Pc1QtI+v2q0HObJXjXvLkqFuelilw/o3XLO/b2VmRq/iV5lpXsdjT6qdHpO9u4K6YvgeZW+AtaylCF0fzvoDm8HZxHwRDBVFcF4nYzBqNJiPJU2s1dhcIs8xXdaqUGDOviPNjFHj8pGi+H7gZmsqocJa35inDK1G6M7CmcORGaNZR9pYo8ern3tihkcNAegl+igdZuhislN0nyTFL2XxRClCzQJ6MYatH3HEqhminXxAfQ87mBS+YVHSLBZSCrRlmAtd+f6/IyEg3mcgNaibCFJnXtD4AwyZmTJn6jhi1vIHJOTPaNGPkqdivKhWj0+DUP5YWlOY6kfZ5Ewo6TdEsHLWCE77KbGCFV3Z3RNzDf0hc0krSy08vTSAwo4/UKIGG1R/JFyKznpU3zozylYvhh9+BStgFclxTUTtu0ZFj3OrgGauLyrYyeV7uk4oPAN2xXItWi6MLNPYzZrGmrNNzK7Wwcj75M1km46rQQva5eKE53fqGDIXv+RWbF8mtWi1v12LV+9gvJbt3n+8gQhnaVu+ro08+2yNu26xzaLVSlJvH4CdGxUtZvAGa+617cBL5XL/F8lt4IIn07PNPFUWImHvzSz/U+/tB82K+UeyWbZbiQtRzVglsHecs2htM89hNkiPlOpKmq9zRVQ8vzPzNW+LVGfZ4vaLRMmm0G7z9HXHcAsLPBSsfCka//k1xEXYdCLTOBnXV+UOpr25PGalfSRyCqLDPC1du+bCoeDCJzvdxWw1AKGcwpfgWfOV588eS1Fu9Lep9Eti6LOk6ZyhSpWqkc1b8hGAuCL1r6jz62yjT2QDXHnlRSggfBbd7b9OvJCN7pHl7Pkqlreje71GrOMu6+qTnFwdEqj1ohxVL3bUiTfXEY9Hf/9VOVNGeo8HVfy5Nf+NmWLue5m6maTBDiT3o2ZlPzyovwseUlRuZy5WbEdIMkSZlXtUsvXk0QEtRcxKkGTA6iqp5833KnuQqrfDzBtBeo06aPWxgPangW+UeUubCFF8ghTob9VwF+qyGC9eEcZopxO7jnKF4lV2keHyB1m4GIjJIooGYnnJjnnlSkVmSz2eW6Bas65ZuWXYwyKDxCGeEOmYRjjhvldhlWu02O7r4TyF+HpVCW0oX9RnSK4+6GeWOxgfN1jVmbX3d3nms0sg4U5G9kka3Mhn5ao6n7u0WUu8LfBlqIApRGfsi1LNsZYLBhT+yQnxuSCdr3aL8l7d5skiNKePdZ+hZJcsgVeYSxkW1YbO0L2o3SxaZNK+0m6Go9J4L58St3GeW9XQka+t4nErSjFCfpNFWbcK4qAXxkU/0pKznpBeqFphP8NVZiyQhw4TFXJ/9rXXqu9WNKp6kYZJkoeZDieNRzZNnrFxzhyg+GssW0cMdZIrLBB/LcR+JhhsPq7DeDVm2dsdWdpoF18uyq40p5WiS7zqLqjAaZA/vb4dL3gOt+R8iWxz7dq4Y+7pi1StFZqFqCN3TbDf80bMvsm6vJKQfYdJiWb1sL12G3F5ih+PKmzB/4pqGkduq62LETM6zpeh1mVTaV+tEZi5oP0Kc1ybNxWm0L5T+w6ZL3C4ZMuYZeSJjCCk9XZfpKr523+qqxLFiO0rlnda4nYu49bKHQXoZJUOaC90XutP0krIamTYru7xT/Rmhh60c4pB5VN+eZWwsazu00pOvZ/6380M4x891yBa3ZBiDrXH3v5JxyRnbLt98TLZqcz2zqEaVOv3MWtwp1hQlijKdx6g2qFfRWY3Hfn88kGLUOq8aGTv30QxVYkWQ68a0apxxo6IzrrDP7GU7ik13uiKvu0hrt0ceYYQjPMBJ9nGUIxywaOyhh2wgQaeUenG8xVdauv2JWvpKoMMoaXpIk7XYjsb5YqRY3kPRvmVxB9dlHZds+X8/rMYPIVL4jifeC7sKHsawKlZ7HA99pnlhtoXvk7rxa0dudbBzhtDKl7o9hagt/n6XphFlU926duWl4t5lpD8T7bc9QDzffE1sXxGVRTrpd/kxZP8m1cbVR/x/Wp93ImtXrsNyNTbFRxghxRHKnA0iNW8Vd3l/L097m0WLer3BALfT/KhbOeFq+Ev27LFOyCxcit6DqOfQSYPDfskax2NJGsbs0qv7V92J6D5LleA4B01qZYjLH3U2/qavc5mXXnm95VWFeb47rDDx/mn1/k7yzuKuLLvQqalb03c1nZoQV6cdrlvTztKd/bz+jE6U/D1p9OTK9WQetSZ2Ro/AOjlI0+mMsM9dW7ju8w8fE5UVnwl/5U9Xt/zk/H2S1N93qSty6BrvTyR1eEYbcn75U9fEOXKX7M/sPWd/edIqw0t/iVpKdpf24e1OcGnij9IduQrO3EoMpYZv2NyBzY0bRdIfGuqeZjZow791QrR0deY+fx3d1dSbncD7OqSeY8rcowuIEl98qyPx435wOzePujlTJvSCw50pOyHP2I1G+1PgwDr+Ut1tdpLKqGsJtU2tcaXYqX0UPKLvxe+MvrZyap3L9HxtpZw60USH5mrIazxfWin9TjQJ6YeabPvdSjn5i7SlbOa2KSEXeUnNjqC+Ky+Oy7re59nifa7SQqHv7qGf+YDIpYFbcP+TQqzCXfHxyNxtwz6odKekZ3+5qkqYtKd+9dYv/0LzPH2vmEvJM8FezO9PTv90adVorvs3Yv64OwG2LQ=='), 
	type='distance')

#Create a dimension: Relative distance between extensometer points
MCAD_ELEMENT=gom.script.inspection.inspect_dimension (
	distance_restriction='xyz', 
	elements=[gom.app.project.actual_elements[user_extenso_name]], 
	nominal_value=0.00000000e+00, 
	nominal_value_source='fixed_value', 
	properties=gom.Binary ('eAHtml2MlFcZx380tAIFaRFr1arjsMvOlu4nSOvA8iUgKLSmENpgZLK7M7s7MjsznZmF3WrtNFxIbGPihal3Wj96UZom9KYxMbGamPRGTE00vdIYUqOGpKYmNppYzP8579n3fWff2Z1lqXrhTIB33nPO8/38n+ecw8nKdN+pT/XPcuTQ/oOHHuZkbbTYqGezh0qF6UK58YVapVqoNYqFOu6zitVsWMc6NrBuA3D5hefPTAGffeh46kRlonF+tFZIDQ8OPZA6UmlMFGdT97t1Vy6tYbuth1e/32vv9r1w7RZ7aPtZ1QP0dF25tIr1wGpojlNnjjpffPLKpVu4F/gANCepME0/DzHGlykwToOHKTBBgRoFyoxT4Ni9onRHIIb++M+daOQ+YDM08xQZZZIao0yTY9TozTBKiRzjVChRocbp30uGu4C1ERk+Mz8+9KiobgLebyaQ8dxn36qr79PvjwMbEjiGPF7/7o3w+Nvt1et9rz+9XTy8jVq1mmaUWXLMkTNNy0xS4Oqvxe8Y0AXNk2bXLFkOU2SSYXs+GLOP9C0zYeMzRqlB0d6dMovNUCBr2n/ISWLPa6MeeKZZen5xUYuUF4j6x/+mqP3ABxOsWjaLSdzWaFn/9I14MhotwyAbLvDkOYrUKTJGkRJFGubTaSrkKfDmBfEdAwZW7FF5/1QCt+MBrz9/TG48AOwJ/LwDuBua55myPMxRtYys268GObNVju3kyVG3PC1T4MJbb1jet7OzIlfxK821rmKxp9XPjkjf3cA9MX0PMTvPW9ZShC6M5v0BzeFt4j4IhgqiuCYSsZlVGk1GkmdWa+weEGaZr+pUKTFqXhHnxynwxCnR/AhwOzSVUeEsb83ThleidHdgTeHIrdCso+wtUeK1L721QyNHgPQi/BQPsnQxWCm7T5BjhrL5ohSgZoE8GcNWj7hjVAzRzrwkPoaczYteMKnoFgsoBVvTTAeu/dE+kZGRbjORG9RMhEkyb2h9AIZNzJgy9V0xankDk/NmtClGyVOxX1UqRqfB6X8sLijNNSLt8yYUdIqiWThqBSd8lZnACq/u7oi4h/+QuKSVpFefXZxAYEYfqVECDas/ki9EZj0rb5wZ5SsXw4+8B5WwC+S4pqJ2zKIjx5jVwbNWF5VtZfK80icVHwS6Y7kWrRbH5mkcYNRiTVmn51ZqYeV86heyTMZVofnsc/FCc6r1DRkKP/QrNi2QW7Va3q7FqvfxX0l27z7fQYQytK3e1/c+9YMecdtqnUOrlaLcPAY/uVe8lMXroHnAugcnkc/1jZbfwgNJpGeff6ooQsTc21/7sd4/AJoX841it2yzFBeinrNKYOs4b9HeYIrHb5McKdeRNF3ljq56ZH7mb94Rr86wx+sVjZYJo93g3e+J42YQfs5b+XAw+q1vi4uw62CgdTaoq84fSn11e8pI/UriEESFfV66tvETouLBJDrfx201AKGcwZTiW/CV5+2fSlJv9bao9zlgy5Kk65ylSJWqkc5Z8ROCuSD0rqnz2G+jTGcCXHv0spQQPgpu992pX0lG9kjz7lyUSlvRvd+jVnGWdfVJz5cHRKo9aIcVS921Ik31xGPR3//VTlTRnqXB9X8uTn/9Jli9luZupmgwTYk96NmZT88qL8LHlJUbmcuVmxHSDJEmZV7VLL15LEBLUXMSpBkwOoqqOfN9yp7kKq3w8wbQXqNOmj2sYy2p4FvlPlLmwhRfIYU6G/VcBfqshgvXhHGaKcTu47yheJVdpHhintZuBiIySKKBmJ5yY545UpFZks9nlugWrOuWbll2MMig8QhnhDpmEY44b5XYZVrtNju6+E8hfh6VQltKF/UZ0iuPuhnljsYHzdY1Zmx93d55rNLIGJORvZJGtzAR+WqOp+7tFlLvC3wZaiAKURn7ItSzbGGcwfk/skJ8bkgna92i/Je3ebJIjUnj3WfoWSXLIFVmE8ZFtWGztC9qN0sWmTCvtJuhqPSeC+fErdxnlvV0JGvreJxK0oxQn6TRVm3CuKgF8ZFP9KSs56QXqhaYS/DVOYskIcO4xVyf/a116rvVjSqepGGSZKHmQ4njUc2TZyxfc4coPhrLFtHDHWSKywQfy3EfiYYbD6uw3g1ZtnbHVnaaBTfLsiuNKeVoku86i6owGmQP72+HS94DrfkfIlsc+3YuG/u6YtUrRWa+agjd02wz/NGzL7JurySkH2HCYlm9bC9dhtxeYofjypswf+KahpHbqutCxEzOs8XodZlU2lfrRGY2aD9CnNcmzcVptC+U/sOmS9wuGTLmGXkiYwgpPV2X6Sq+dt/qqsSxYjtK5Z3WuJ2LuPWyh0F62UuGNBe7L3an6SVlNTJtVnZ5p/ozQg9bOMxh86i+PUvYWNZ2aKUnX8/8b+eHcI6f65AtbskwBlvj7n8l45Iztl2++Zhs1eZmZlGNKnX6mbG4U6wpShRlOo9RbVCvorMaj/3+eCDFXuu8amTs3EczVIkVQa4b06oxxoyKzrjCPrOXbSg23emKvO4ird0eeYQRjvIgp9jPMY5y0KKxhx6ygQSdUurF8RZfaen2J2rpK4EOe0nTQ5qsxXY0zhcixdIeivYtCzu4Luu4ZMv/+2ElfgiRwnc88V7YVfAwhlWx2uN46DPNC7MtfJ/Ujd84cquDnTWEVr7U7SlEbfH3uzSNKJvq1rUrLxX3LiP9mWi/7QHi+eZrYvuKqCzSSb/LjyH7N6k2rjzi/9P6vBdZu3wdlqqxKT7JCCmOUuZcEKl5q7hL+3tp2lstWtTrDQa4neYn3coJV8NftmePdUJm4VL0HkQ9h04aHPZL1jgeS9IwZhdf3b/iTkT3WaoEJzhkUitDXP6os/E3fZ3LvPjKmy2vKsyL3WGFifdPK/d3kncWdmXZ+U5N3Zq+K+nUhLg67XDdmnaW7uznzed0ouTvSaMnV64n86g1vjN6BNbJQZpOZ4R97trCdZ9/+LSoLPtM+Bt/ur75Zxful6T+vktdkUPXeH8iqcMz2pDzK5+/Ic6Ru2R/Zu85+8uTVhle/kvUUrK7tA9vd4JLE3+U7shVcOZWYig1fMPmDmxuXS+S/tBQ9zQzQRv+nZOipasz9/nr3l1NvdkJfLhD6jkmzT26gCjx1Xc6Ej/uB7dz86ibM2VCLzjcmbQT8ozdaLQ/BQ6s4y/V3WYnqYy6llDb1BrXip3aR8Ej+l78zuhrK6fWuUzPN5fLqRNNdGiuhrzGi6Xl0u9Ek5B+qMnW3y2Xk79IW8xmbpsScpGX1OwI6rvy4rik632eLdznKi0U+u4e+rmPilwa2Ij7nxRiFe6KT0Tmbh32QaU7JT37y1VVwqQ99Wt3fP2Xmufpe8VcSp4N9mJ+f3Lm54urRnPNvwGVRri6AfIf'), 
	type='strain')

#Export to external csv file
gom.script.diagram.export_diagram_contents (
	cell_separator=';', 
	codec='utf-8', 
	decimal_separator='.', 
	file=os.path.join(os.path.dirname(gom.app.project.get ('project_file')),
		gom.app.project.get ('name')+'_Extenso_'+str(int(RESULT.extensometer_length))+'.csv'),
	header_export=True, 
	line_feed='\n', 
	text_quoting='"', 
	type='checks')



