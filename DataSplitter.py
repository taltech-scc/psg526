# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 08:46:06 2020

@author: A.Š.
Kood käib läbi kõik kaustad ja alamkaustad ning otsib .csv faile,
millel on tulpasid rohkem kui neli (local and global extensometer).
Sellise faili olemasolul luuakse sinna samasse kausta kaks faili,
milledesse kopitakse tulbad vastavalt [0, 1, 3, 5] ja [0, 2, 4, 5]

Uute failide nimi tuleb esialgse faili nimest (-39 tähemärki) 
(read 37 ja 39)
MIHKEL KÃRGESAAR-see tuleks vb eemaldada (teeks koodi natuke robustsemaks)

Esialgne fail kustutatakse.
"""
import os
import csv
from glob import glob
import pandas as pd
import numpy as np
import sys

cwd = os.getcwd()
filetype = '*.csv'
all_files = [file
                 for path, subdir, files in os.walk(cwd)
                 for file in glob(os.path.join(path, filetype))]

for file in all_files:
    with open(file) as csv_file:
        df = pd.read_csv(csv_file, delimiter=';')
        if len(df.columns)>4:
            path=csv_file.name                      # FILE PATH
            head_tail=os.path.split(path)           # SPLIT PATH (file location + file name)
            os.chdir(head_tail[0])                  # GO TO FOLDER
            #print(head_tail[1][:-39])
            with open(head_tail[1][:-39]+'_LONGextenso.csv', "w", newline='') as result:
                (df.iloc[:, [0,2,4,5]]).to_csv(head_tail[1][:-39]+'_LONGextenso.csv', encoding='utf-8', index=False)
            with open(head_tail[1][:-39]+'_SHORTextenso.csv', "w", newline='') as result:
                (df.iloc[:, [0,1,3,5]]).to_csv(head_tail[1][:-39]+'_SHORTextenso.csv', encoding='utf-8', index=False)
            csv_file.close()
            os.remove(head_tail[1])   
        else:
            pass
                
        