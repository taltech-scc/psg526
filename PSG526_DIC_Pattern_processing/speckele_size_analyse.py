'''
Developed by Mihkel Kõrgesaar
Last version -v4
03-05-2020

Last version -v5
01-09-2020
    if called on main then uses the last development. This is definition speckle_size_im_fork,
    which uses a kernel step size of 2. This resolves the binning problem in histograms, 
    while having a small effect on the resulting average speckle size estimation.  


'''

import os
import subprocess
import sys
import glob
import py_general as guf
print (__name__)
from contextlib import closing
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
from scipy.stats import norm
from scipy import ndimage

#os.chdir('C:\Users\mihke\OneDrive - TalTech\5-Projects\2019-ETAG Starting GRANT-application 2019\DIC pattern processing')

#----------------------------------------
# fname='Kk_4_2.5bar_4.5_1.5_PS15x11.png'
# grey=0.7
# # scaled 2
# im_height=11.21  #mm
# im_width=15.14   #mm
# #----------------------------------------




def speckle_size(pic_name,grey,im_height,im_width):
    
    fname=pic_name
    # grey=0.99
    # # scaled 2
    # im_height=200  #mm
    # im_width=200   #mm
    
    
    
    initial_im=cv.imread(fname,0)
    
    
    
    #COnvert the image to black and white, since The morphological technique 
    #yields the best results for binary images (Bossuyt 2006). With 55% value I get the black pixel count same as Sven 15%
    (thresh, im_bw) = cv.threshold(initial_im, grey*255, 255, cv.THRESH_BINARY)    #255=white, 0=black
    
    
    max_speck=28; 
    min_speck=1; 
    speck_range=max_speck-min_speck+1  
    ##Black pixels in the reference image
    size =initial_im.size
    count_binary_black =(size-np.count_nonzero(im_bw))  #Total nr of Speckle px in binary image
    count_binary_black_pcnt =(size-np.count_nonzero(im_bw))/size  
    
    
    Pi_count_range=np.zeros([speck_range,3]); Pi_count=np.zeros([speck_range,2]); px_in_range_pcnt=np.zeros([speck_range,2]);
    Pi_cum=np.zeros([speck_range,2])
    D_px=np.flipud(np.linspace(min_speck,max_speck,max_speck-min_speck+1))  # Pixel Diameter list (diameter is edge length)
    Pi_count[:,0]=D_px
    Pi_count_range[:,0]=D_px
    px_in_range_pcnt[:,0]=D_px
    # sys.exit()
    #% Plot the binary image (commented out if dont want the figure)
    #--------------------------
    # titles = ['Originaal pilt','Binaarne pilt: limiit ' + str(grey) + '*255, musti piksleid '+str(int(count_binary_black_pcnt*100))+'%']
    # images = [initial_im, im_bw]
    # fig, ax = plt.subplots()
    # for i in range(2):
    #     plt.subplot(1,2,i+1),plt.imshow(images[i],'gray')
    #     plt.title(titles[i])
    #     # plt.xticks([]),plt.yticks([])  
    # plt.savefig(('Binaarne-' + fname[:-4] + '.png'),dpi=500,transparent=False)
    #--------------------------
    #%%
    
    #Initialize some arrays
    
    Pi_count_range=np.zeros([speck_range,3]); Pi_count=np.zeros([speck_range,2]); px_in_range_pcnt=np.zeros([speck_range,2]);
    Pi_cum=np.zeros([speck_range,2])
    D_px=np.flipud(np.linspace(min_speck,max_speck,max_speck-min_speck+1))  # Pixel Diameter list (diameter is edge length)
    Pi_count[:,0]=D_px
    Pi_count_range[:,0]=D_px
    px_in_range_pcnt[:,0]=D_px
    # sys.exit()
    #% Plot the binary image
    
     
    im_r=4
    im_c=int(np.size(D_px)/im_r)
    
    
    
    # These numbers must conform to the D_px array dimensions
    # fig, ax = plt.subplots(im_r,im_c)
    i=-1   
    for k in range(im_r):
        for j in range(im_c):
            i+=1
            D=D_px.astype(int)[i]
            ellipse=cv.getStructuringElement(cv.MORPH_ELLIPSE,(D,D))
            dilated_im = cv.dilate(im_bw, ellipse,iterations = 1)
            eroded_im = cv.erode(dilated_im, ellipse,iterations = 1)
            # ax[k,j].imshow(im_bw,'gray',alpha=0.1)
            # ax[k,j].imshow(eroded_im,'gray',alpha=0.3)
            # axc=ax[k,j]
            # axc.set_xticks([]);axc.set_yticks([]);
            # axc.set(title='D='+str(D))
            Pi_count[i,1]=size-np.count_nonzero(eroded_im)  #black pixel counter
            if i==0:
                Pi_count_range[i,1]=size-np.count_nonzero(eroded_im)
            else:
                Pi_count_range[i,1]=size-Pi_count[i-1,1]-np.count_nonzero(eroded_im)
                Pi_count_range[Pi_count_range < 0] = 0
    # plt.savefig(('Kernelid-' + fname[:-4] + '.png'),dpi=500,transparent=False)
    pixel_array=np.empty([0,0])
    for (D,count) in zip(Pi_count_range[:,0], Pi_count_range[:,1]): 
         pixel_array=np.append(pixel_array,np.full(int(np.floor(count)),D))      
    
         
         
    # #% Postprocess the morphology operation results
    # fig1, ax1 = plt.subplots(1,2,figsize=(5, 3))        
    # #plt.subplot(2,2,1)
    # px_in_range_pcnt[:,1]=Pi_count_range[:,1]/count_binary_black
    # ax1[0].bar(px_in_range_pcnt[:,0],px_in_range_pcnt[:,1])
    
    
    
    # #Histogram test in separate plot
    # n, bins, patc= ax1[0].hist(pixel_array,np.flipud(D_px),density=True,facecolor=[0.8,0.8,1],
    #     edgecolor='k',rwidth=1,linewidth=0.5)
    # # n, bins, patc= ax1[0,0].hist(pixel_array,23,density=True,facecolor=[0.8,0.8,1],
    # #     edgecolor='k',rwidth=0.98)
    # # Add a normal distribution fit to data
    # mu, std = norm.fit(pixel_array)
    # # Plot the PDF.
    # xmin, xmax = ax1[0].get_xlim()
    # x = np.linspace(xmin, xmax, 100)
    # p = norm.pdf(x, mu, std)
    
    pxl_length=im_height/np.size(im_bw,0) # Image height/height in px       Pixli pikkus mm
    
    
    
    # ax1[0].plot(x, p, 'k', linewidth=1)
    
    
    # ax1[0].set(ylabel='Percentage [%]',
    #              xticks= np.flipud(D_px[::2]).astype(int))
    # ax1[0].set_title(label=fname,fontsize=7)
    
    # #Textbox contaiing the information------------------------------------------
    # props = dict(boxstyle='round', facecolor='white', alpha=0.1)
    # textstr = '\n'.join((
    #     r'$\mu_{spec}=%.3f$ px' % (np.around(mu,2), ),
    #     r'$\mu_{spec}=%.3f$ mm' % (np.around(mu*pxl_length,3), ),
    #     r'$\sigma_{spec}=%.3f$' % (np.around(std,2), ),
    #     r'$CV^2_{spec}=%.3f$' % (np.around(std**2/mu**2,2), ),
    #     r'$1px=%.3f mm$'  % (pxl_length, )))
    # ax1[0].text(0.65, 0.95, textstr, transform=ax1[0].transAxes, fontsize=6,
    #         verticalalignment='top', bbox=props)
    
    # # coefficienf of variation calculated according to Feldman p35 Applied Probability and Stochastic Processes
    # # Squared coefﬁcient of variation greater 1.0 implies a signiﬁcant amount
    # # of variability.
    # # END Textbox contaiing the information--------------------------------------
    
    
    
    # Rsum=np.cumsum(np.flipud(px_in_range_pcnt[:,1]*100))
    # ax1[1].plot(np.arange(min_speck,max_speck+1,1),Rsum,'-o')
    # ax1[1].set(ylim=[0, 100],
    #              xticks=np.arange(min_speck,max_speck,2),
    #              ylabel='cumulative percentage [%]')

    pixel_array_mm=pixel_array*pxl_length
    # fig1.set_size_inches(guf.cm2inch(16, 6))
    # plt.tight_layout()
    # plt.savefig(('image analysis-' + fname[:-4] + '.png'),dpi=500,transparent=False)
    np.save((fname[:5] + '_pixel_array_mm'), pixel_array_mm)
    return pixel_array


''' this function is used in processing the data
C:\Work\1_Research\PSG526\Mustri_analüüs\05.05 Töödeldud B&W

'''
def speckle_size_im(pic_name,grey,im_height,im_width,max_speck):

    fname=pic_name
    initial_im=cv.imread(fname,0)

    #COnvert the image to black and white, since The morphological technique 
    #yields the best results for binary images (Bossuyt 2006). With 55% value I get the black pixel count same as Sven 15%
    (thresh, im_bw) = cv.threshold(initial_im, grey*255, 255, cv.THRESH_BINARY)    #255=white, 0=black
    
    
    
    min_speck=1; 
    speck_range=max_speck-min_speck+1  
    ##Black pixels in the reference image
    size =initial_im.size
    count_binary_black =(size-np.count_nonzero(im_bw))  #Total nr of Speckle px in binary image
    count_binary_black_pcnt =(size-np.count_nonzero(im_bw))/size  
    
    
    Pi_count_range=np.zeros([speck_range,3]); Pi_count=np.zeros([speck_range,2]); px_in_range_pcnt=np.zeros([speck_range,2]);
    Pi_cum=np.zeros([speck_range,2])
    D_px=np.flipud(np.linspace(min_speck,max_speck,max_speck-min_speck+1))  # Pixel Diameter list (diameter is edge length)
    Pi_count[:,0]=D_px
    Pi_count_range[:,0]=D_px
    px_in_range_pcnt[:,0]=D_px
    # sys.exit()
    #% Plot the binary image (commented out if dont want the figure)
    #--------------------------
    titles = ['Originaal pilt','Binaarne pilt: limiit ' + str(grey) + '*255, musti piksleid '+str(int(count_binary_black_pcnt*100))+'%']
    images = [initial_im, im_bw]
    fig, ax = plt.subplots()
    for i in range(2):
        plt.subplot(1,2,i+1),plt.imshow(images[i],'gray')
        plt.title(titles[i])
        # plt.xticks([]),plt.yticks([])  
    plt.savefig(('Binaarne-' + fname[9:-4] + '.png'),dpi=500,transparent=False); plt.close(fig) 
    #--------------------------
    #%%
    
    #Initialize some arrays
    
    Pi_count_range=np.zeros([speck_range,3]); Pi_count=np.zeros([speck_range,2]); px_in_range_pcnt=np.zeros([speck_range,2]);
    Pi_cum=np.zeros([speck_range,2])
    D_px=np.flipud(np.linspace(min_speck,max_speck,max_speck-min_speck+1))  # Pixel Diameter list (diameter is edge length)
    Pi_count[:,0]=D_px
    Pi_count_range[:,0]=D_px
    px_in_range_pcnt[:,0]=D_px
    # sys.exit()
    #% Plot the binary image
    

    
    
    
    # These numbers must conform to the D_px array dimensions
    eroded_im_list=[] 
    for i,Dpx in enumerate(D_px):
            # D=D_px.astype(int)[i]
            ellipse=cv.getStructuringElement(cv.MORPH_ELLIPSE,(int(Dpx),int(Dpx)))
            dilated_im = cv.dilate(im_bw, ellipse,iterations = 1)
            eroded_im = cv.erode(dilated_im, ellipse,iterations = 1)
            eroded_im_list.append(eroded_im)
            Pi_count[i,1]=size-np.count_nonzero(eroded_im)  #black pixel counter
            if i==0:
                Pi_count_range[i,1]=size-np.count_nonzero(eroded_im)
            else:
                Pi_count_range[i,1]=size-Pi_count[i-1,1]-np.count_nonzero(eroded_im)
                Pi_count_range[Pi_count_range < 0] = 0
    
    pixel_array=np.empty([0,0])
    for (D,count) in zip(Pi_count_range[:,0], Pi_count_range[:,1]): 
         pixel_array=np.append(pixel_array,np.full(int(np.floor(count)),D))      
    
    fig, ax = plt.subplots(2,2)
    ax[0,0].imshow(im_bw,'gray',alpha=0.1); ax[0,0].imshow(eroded_im_list[0],'gray',alpha=0.3)  
    ax[0,0].set(title='D='+str(D_px[0]))
    ax[0,1].imshow(im_bw,'gray',alpha=0.1); ax[0,1].imshow(eroded_im_list[5],'gray',alpha=0.3)  
    ax[0,1].set(title='D='+str(D_px[5]))
    ax[1,0].imshow(im_bw,'gray',alpha=0.1); ax[1,0].imshow(eroded_im_list[10],'gray',alpha=0.3)  
    ax[1,0].set(title='D='+str(D_px[10]))
    ax[1,1].imshow(im_bw,'gray',alpha=0.1); ax[1,1].imshow(eroded_im_list[20],'gray',alpha=0.3)  
    ax[1,1].set(title='D='+str(D_px[20]))
    plt.savefig(('Kernelid-' + fname[9:-4] + '.png'),dpi=500,transparent=False)
    plt.close(fig)     
         
    #% Postprocess the morphology operation results
    fig1, ax1 = plt.subplots(1,2,figsize=(5, 3))        
    #plt.subplot(2,2,1)
    px_in_range_pcnt[:,1]=Pi_count_range[:,1]/count_binary_black
    ax1[0].bar(px_in_range_pcnt[:,0],px_in_range_pcnt[:,1])
    
    
    
    #Histogram test in separate plot
    n, bins, patc= ax1[0].hist(pixel_array,np.flipud(D_px),density=True,facecolor=[0.8,0.8,1],
        edgecolor='k',rwidth=1,linewidth=0.5)
    # n, bins, patc= ax1[0,0].hist(pixel_array,23,density=True,facecolor=[0.8,0.8,1],
    #     edgecolor='k',rwidth=0.98)
    # Add a normal distribution fit to data
    mu, std = norm.fit(pixel_array)
    # Plot the PDF.
    xmin, xmax = ax1[0].get_xlim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)
    
    pxl_length=im_height/np.size(im_bw,0) # Image height/height in px       Pixli pikkus mm
    
    
    
    ax1[0].plot(x, p, 'k', linewidth=1)
    
    
    ax1[0].set(ylabel='Percentage [%]',
                  xticks= np.flipud(D_px[::2]).astype(int))
    ax1[0].set_title(label=fname,fontsize=7)
    
    #Textbox contaiing the information------------------------------------------
    props = dict(boxstyle='round', facecolor='white', alpha=0.1)
    textstr = '\n'.join((
        r'$\mu_{spec}=%.3f$ px' % (np.around(mu,2), ),
        r'$\mu_{spec}=%.3f$ mm' % (np.around(mu*pxl_length,3), ),
        r'$\sigma_{spec}=%.3f$' % (np.around(std,2), ),
        r'$CV^2_{spec}=%.3f$' % (np.around(std**2/mu**2,2), ),
        r'$1px=%.3f mm$'  % (pxl_length, )))
    ax1[0].text(0.65, 0.95, textstr, transform=ax1[0].transAxes, fontsize=6,
            verticalalignment='top', bbox=props)
    
    # coefficienf of variation calculated according to Feldman p35 Applied Probability and Stochastic Processes
    # Squared coefﬁcient of variation greater 1.0 implies a signiﬁcant amount
    # of variability.
    # END Textbox contaiing the information--------------------------------------
    
    
    
    Rsum=np.cumsum(np.flipud(px_in_range_pcnt[:,1]*100))
    ax1[1].plot(np.arange(min_speck,max_speck+1,1),Rsum,'-o')
    ax1[1].set(ylim=[0, 100],
                  xticks=np.arange(min_speck,max_speck,2),
                  ylabel='cumulative percentage [%]')

    pixel_array_mm=pixel_array*pxl_length
    fig1.set_size_inches(guf.cm2inch(16, 6))
    plt.tight_layout()
    plt.savefig(('image analysis-' + fname[9:-4] + '.png'),dpi=500,transparent=False); plt.close(fig1) 
    np.save((fname[:5] + '_pixel_array_mm'), pixel_array_mm)
    return pixel_array
 
 
 
'''
this function is used in further developing the original function speckle_size_im.
v03-18-08-2020
Here the kernel step is 2 pixels. This makes the histogram look more natural. 
'''
def speckle_size_im_fork(pic_name,grey,im_height,im_width,max_speck):


    fname=pic_name
    initial_im=cv.imread(fname,0)

    #COnvert the image to black and white, since The morphological technique 
    #yields the best results for binary images (Bossuyt 2006). With 55% value I get the black pixel count same as Sven 15%
    (thresh, im_bw) = cv.threshold(initial_im, grey*255, 255, cv.THRESH_BINARY)    #255=white, 0=black
    
    
    
    min_speck=2 
    # speck_range=max_speck-min_speck+1  
    D_px=np.flipud(np.arange(min_speck,max_speck+2,2))
    speck_range=len(D_px)
    ##Black pixels in the reference image
    size =initial_im.size
    count_binary_black =(size-np.count_nonzero(im_bw))  #Total nr of Speckle px in binary image
    count_binary_black_pcnt =(size-np.count_nonzero(im_bw))/size  
    
    
    Pi_count_range=np.zeros([speck_range,3]); Pi_count=np.zeros([speck_range,2]); px_in_range_pcnt=np.zeros([speck_range,2]);
    Pi_cum=np.zeros([speck_range,2])
    # D_px=np.flipud(np.linspace(min_speck,max_speck,max_speck-min_speck+1))  # Pixel Diameter list (diameter is edge length)
    Pi_count[:,0]=D_px
    Pi_count_range[:,0]=D_px
    px_in_range_pcnt[:,0]=D_px
    #% Plot the binary image (commented out if dont want the figure)
    #--------------------------
    titles = ['Originaal pilt','Binaarne pilt: limiit ' + str(grey) + '*255, musti piksleid '+str(int(count_binary_black_pcnt*100))+'%']
    images = [initial_im, im_bw]
    fig, ax = plt.subplots()
    for i in range(2):
        plt.subplot(1,2,i+1),plt.imshow(images[i],'gray')
        plt.title(titles[i])
        # plt.xticks([]),plt.yticks([])  
    plt.savefig(('Binaarne-' + fname[9:-4] + '.tif'),dpi=500,transparent=False); plt.close(fig) 
    #--------------------------
    #%%
    # These numbers must conform to the D_px array dimensions
    eroded_im_list=[] 
    for i,Dpx in enumerate(D_px):
            # D=D_px.astype(int)[i]
            ellipse=cv.getStructuringElement(cv.MORPH_ELLIPSE,(int(Dpx),int(Dpx)))
            dilated_im = cv.dilate(im_bw, ellipse,iterations = 1)
            eroded_im = cv.erode(dilated_im, ellipse,iterations = 1)
            eroded_im_list.append(eroded_im)
            Pi_count[i,1]=size-np.count_nonzero(eroded_im)  #black pixel counter
            if i==0:
                Pi_count_range[i,1]=size-np.count_nonzero(eroded_im)
            else:
                Pi_count_range[i,1]=size-Pi_count[i-1,1]-np.count_nonzero(eroded_im)
                Pi_count_range[Pi_count_range < 0] = 0
    
    pixel_array=np.empty([0,0])
    for (D,count) in zip(Pi_count_range[:,0], Pi_count_range[:,1]): 
         pixel_array=np.append(pixel_array,np.full(int(np.floor(count)),D))      
    
    fig, ax = plt.subplots(2,2)
    ax[0,0].imshow(im_bw,'gray',alpha=0.1); ax[0,0].imshow(eroded_im_list[0],'gray',alpha=0.3)  
    ax[0,0].set(title='D='+str(D_px[0]))
    ax[0,1].imshow(im_bw,'gray',alpha=0.1); ax[0,1].imshow(eroded_im_list[5],'gray',alpha=0.3)  
    ax[0,1].set(title='D='+str(D_px[5]))
    ax[1,0].imshow(im_bw,'gray',alpha=0.1); ax[1,0].imshow(eroded_im_list[10],'gray',alpha=0.3)  
    ax[1,0].set(title='D='+str(D_px[10]))
    ax[1,1].imshow(im_bw,'gray',alpha=0.1); ax[1,1].imshow(eroded_im_list[13],'gray',alpha=0.3)  
    ax[1,1].set(title='D='+str(D_px[13]))
    plt.savefig(('Kernelid-' + fname[9:-4] + '.tif'),dpi=500,transparent=False)
    plt.close(fig)     
    # sys.exit()
    #% Postprocess the morphology operation results
    fig1, ax1 = plt.subplots(1,2,figsize=(5, 3))        
    #plt.subplot(2,2,1)
    px_in_range_pcnt[:,1]=Pi_count_range[:,1]/count_binary_black
    # ax1[0].bar(px_in_range_pcnt[:,0],px_in_range_pcnt[:,1])
    
    
    
    #Histogram test in separate plot
    n, bins, patc= ax1[0].hist(pixel_array,np.flipud(D_px),density=True,facecolor=[0.8,0.8,1],
        edgecolor='k',rwidth=1,linewidth=0.5)
    # n, bins, patc= ax1[0,0].hist(pixel_array,23,density=True,facecolor=[0.8,0.8,1],
    #     edgecolor='k',rwidth=0.98)
    # Add a normal distribution fit to data
    mu, std = norm.fit(pixel_array)
    # Plot the PDF.
    xmin, xmax = ax1[0].get_xlim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)
    
    pxl_length=im_height/np.size(im_bw,0) # Image height/height in px       Pixli pikkus mm
    
    
    
    ax1[0].plot(x, p, 'k', linewidth=1)
    
    
    ax1[0].set(ylabel='Percentage [%]',
                  xticks= np.flipud(D_px).astype(int))
    ax1[0].set_title(label=fname,fontsize=7)
    
    #Textbox contaiing the information------------------------------------------
    props = dict(boxstyle='round', facecolor='white', alpha=0.1)
    textstr = '\n'.join((
        r'$\mu_{spec}=%.3f$ px' % (np.around(mu,2), ),
        r'$\mu_{spec}=%.3f$ mm' % (np.around(mu*pxl_length,3), ),
        r'$\sigma_{spec}=%.3f$ mm' % (np.around(std*pxl_length,3), ),
        r'$CV^2_{spec}=%.3f$' % (np.around(std**2/mu**2,2), ),
        r'$1px=%.3f mm$'  % (pxl_length, )))
    ax1[0].text(0.65, 0.95, textstr, transform=ax1[0].transAxes, fontsize=6,
            verticalalignment='top', bbox=props)
    
    # coefficienf of variation calculated according to Feldman p35 Applied Probability and Stochastic Processes
    # Squared coefﬁcient of variation greater 1.0 implies a signiﬁcant amount
    # of variability.
    # END Textbox contaiing the information--------------------------------------
    
    
    
    Rsum=np.cumsum(np.flipud(px_in_range_pcnt[:,1]*100))
    ax1[1].plot(np.flipud(D_px),Rsum,'-o')
    ax1[1].set(ylim=[0, 100],
                  xticks=np.arange(min_speck,max_speck,2),
                  ylabel='cumulative percentage [%]')

    pixel_array_mm=pixel_array*pxl_length
    fig1.set_size_inches(guf.cm2inch(16, 6))
    plt.tight_layout()
    plt.savefig(('image analysis-' + fname[9:-4] + '.tif'),dpi=500,transparent=False); #plt.close(fig1) 
    np.save((fname[:5] + '_pixel_array_mm'), pixel_array_mm)
    return pixel_array
   
if __name__=="__main__":
    File=glob.glob('TP*.png')
    fname=File[0]
    grey=0.7
    # scaled 2
    im_height=14.81  #mm
    im_width=22.22   #mm
    #----------------------------------------
    n=speckle_size_im_fork(fname,grey,im_height,im_width,28)

