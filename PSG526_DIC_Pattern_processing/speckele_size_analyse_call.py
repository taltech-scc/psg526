

import os
import subprocess
import sys
import glob
import py_general as guf
import speckele_size_analyse as speck
print (__name__)
from contextlib import closing
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
from scipy.stats import norm

#os.chdir('C:\Users\mihke\OneDrive - TalTech\5-Projects\2019-ETAG Starting GRANT-application 2019\DIC pattern processing')

#----------------------------------------
# File=glob.glob('*/TP*png')

# sys.exit()
File=glob.glob('TP_3*/TP*png')
# guf.splitpath(File[0])


# sys.exit()
for fname in File:
    fol,_,fi=guf.splitpath(fname)
    os.chdir(fol)
    grey=0.7
    # scaled 2
    im_height=14.81  #mm
    im_width=22.22   #mm
    #----------------------------------------
    
    speck.speckle_size_im(fi,grey,im_height,im_width,42)
    os.chdir("..")