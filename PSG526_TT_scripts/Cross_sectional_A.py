<<<<<<< HEAD
# -*- coding: utf-8 -*-
# '''
# Created on Wed Jun 10 13:48:09 2020 by M.Korgesaar
# Original folder where this was tested out: 
# C:\Users\Mihkel\OneDrive - TalTech\5-Projects\2019-ETAG Starting GRANT-application 2019\4-Work\TensileTests_AND_Simulations\PSG526_Tests\06.03.2020-kk3


# The purpose of the code:
# 1. Calculate cross sectional area based on thickness reduction and width of the specimen
# 2. Greate s-e true curve and write to excel file

# Steps
#     1. Read excel data that is combined from .csv files written in Aramis
#     2. Postprocess the data (here separate vars for coordinate and Eps3)
#     3. Plot the data at specific step
#     4. Save the resulting plot as .png file
# '''

import os
import sys
import glob
# sys.path.append('C:/Work/1_Research/PSG526/Python_files')
# sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
import combine_CSV as mkcom
import python_plotting as pyp
import py_general as pyg
print (__name__)
import PLOT_variable_along_section as secplot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import time
import scipy.integrate
import Write_MaterialCurve_for_INP_29062020 as mc
import PLOT_FD_v2_29052020 as FD

# secplot.plot_along_section('KK3-thickness-reduction-along-width*.xlsx',np.arange(1,450,5))
Fnames=[glob.glob('KK3-thickness-reduction*')]
data = pd.read_excel(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
coord=pd.DataFrame(data.iloc[:,1::2])
eps3=pd.DataFrame(data.iloc[:,2::2])    #This is thickness reduction in %

# ORIGINAL CROSS SECTION 
W=20.81
T=2.04
E=204000
A0=W*T
L0=50
#Aramis data for reduced with (doesnt capture full width)
# Calcuate thickness as function of deformation
t_time=(1-eps3/100)*T

# THis creates a eps33 plot along the section
# secplot.plot_along_section('KK3-thickness*',np.arange(1,460,20)) #[1,15,50]

eps3[eps3<0]=0

# sys.exit()
# Calculate the cross sectional area at given instance
CSarea=[]
# CSarea=np.empty([600, 2])
for i in np.arange(0,len(eps3.iloc[1,:])-1,1):
    CSarea.append(max(scipy.integrate.cumtrapz(t_time.iloc[:,i], coord.iloc[:,i])))

# Area reduction as a % and then this % used to estimate the actual area at any instance
Actual_Area= CSarea/CSarea[0]*A0   


# '''
# Plot the true stress strain estimate based on the necking data fit
# '''

#e0=(sy/A)**(1/n)-eplat
sy= 545.0000
eplat= 0.0180
n= 0.1174
A= 864.7923
Q= 482.8460
C1= 18.3824
alfa= 0.9537

s_estimate=mc.s_combined(np.arange(0,1,0.008),sy,eplat,n,A,Q,C1,alfa)




epsy_L = pd.read_csv('frac_location_1_epsY.csv',skiprows=(0),header=0,delim_whitespace=False,sep=';')
FDfile=[glob.glob('KK3-f-d-data.csv')]
# F=FD.force_disp_only(FDfile,[456]); #plt.close(plt.gcf())
F=FD.true_s_e(FDfile,T,W,E,sy,L0); # force estimated based on the 
s_true=F.iloc[:-1,0]/Actual_Area





fig=plt.gcf(); ax=fig.gca()
if ax==[]:
    plt.close(fig)
    fig, ax = plt.subplots(figsize=(12, 6)) 

ax.plot(np.arange(0,1,0.008),s_estimate,linewidth=1,label='manual estimate') 
ax.plot(epsy_L.iloc[:-1,1]/100,s_true*1000,label='s(fd)-eps_L(aramisDIC)') 
ax.set(xlim=[0, 0.8],ylim=[0, 1000])

'''
Load the equivlaent plastic strain measured by aramis
'''
eps_mises  = pd.read_csv('frac_location_1.phiM.csv',skiprows=(0),header=0,delim_whitespace=False,sep=';')
ax.plot(eps_mises.iloc[:-1,1]/100,s_true*1000,label='s(fd)-Mises eq (dic)') 
ax.legend(loc='best')

plt.savefig('SE_from_AREA.png',transparent=False)



b=pyg.splitpath(os.getcwd())
df=pd.concat([eps_mises.iloc[:-1,1]/100,s_true*1000], axis=1)
=======
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 13:48:09 2020 by M.Korgesaar
Original folder where this was tested out: 
C:\Users\Mihkel\OneDrive - TalTech\5-Projects\2019-ETAG Starting GRANT-application 2019\4-Work\TensileTests_AND_Simulations\PSG526_Tests\06.03.2020-kk3


The purpose of the code:
1. Calculate cross sectional area based on thickness reduction and width of the specimen
2. Greate s-e true curve and write to excel file

Steps
    1. Read excel data that is combined from .csv files written in Aramis
    2. Postprocess the data (here separate vars for coordinate and Eps3)
    3. Plot the data at specific step
    4. Save the resulting plot as .png file
"""

import os
import sys
import glob
# sys.path.append('C:/Work/1_Research/PSG526/Python_files')
# sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
import combine_CSV as mkcom
import python_plotting as pyp
import py_general as pyg
print (__name__)
import PLOT_variable_along_section as secplot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import time
import scipy.integrate
import Write_MaterialCurve_for_INP_29062020 as mc
import PLOT_FD_v2_29052020 as FD

# secplot.plot_along_section('KK3-thickness-reduction-along-width*.xlsx',np.arange(1,450,5))
Fnames=[glob.glob('KK3-thickness-reduction*')]
data = pd.read_excel(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
coord=pd.DataFrame(data.iloc[:,1::2])
eps3=pd.DataFrame(data.iloc[:,2::2])    #This is thickness reduction in %

# ORIGINAL CROSS SECTION 
W=20.81
T=2.04
E=204000
A0=W*T
L0=50
#Aramis data for reduced with (doesnt capture full width)
# Calcuate thickness as function of deformation
t_time=(1-eps3/100)*T

# THis creates a eps33 plot along the section
# secplot.plot_along_section('KK3-thickness*',np.arange(1,460,20)) #[1,15,50]

eps3[eps3<0]=0

# sys.exit()
# Calculate the cross sectional area at given instance
CSarea=[]
# CSarea=np.empty([600, 2])
for i in np.arange(0,len(eps3.iloc[1,:])-1,1):
    CSarea.append(max(scipy.integrate.cumtrapz(t_time.iloc[:,i], coord.iloc[:,i])))

# Area reduction as a % and then this % used to estimate the actual area at any instance
Actual_Area= CSarea/CSarea[0]*A0   


'''
Plot the true stress strain estimate based on the necking data fit
'''

#e0=(sy/A)**(1/n)-eplat
sy= 545.0000
eplat= 0.0180
n= 0.1174
A= 864.7923
Q= 482.8460
C1= 18.3824
alfa= 0.9537

s_estimate=mc.s_combined(np.arange(0,1,0.008),sy,eplat,n,A,Q,C1,alfa)




epsy_L = pd.read_csv('frac_location_1_epsY.csv',skiprows=(0),header=0,delim_whitespace=False,sep=';')
FDfile=[glob.glob('KK3-f-d-data.csv')]
# F=FD.force_disp_only(FDfile,[456]); #plt.close(plt.gcf())
F=FD.true_s_e(FDfile,T,W,E,sy,L0); # force estimated based on the 
s_true=F.iloc[:-1,0]/Actual_Area





fig=plt.gcf(); ax=fig.gca()
if ax==[]:
    plt.close(fig)
    fig, ax = plt.subplots(figsize=(12, 6)) 

ax.plot(np.arange(0,1,0.008),s_estimate,linewidth=1,label='manual estimate') 
ax.plot(epsy_L.iloc[:-1,1]/100,s_true*1000,label='s(fd)-eps_L(aramisDIC)') 
ax.set(xlim=[0, 0.8],ylim=[0, 1000])

'''
Load the equivlaent plastic strain measured by aramis
'''
eps_mises  = pd.read_csv('frac_location_1.phiM.csv',skiprows=(0),header=0,delim_whitespace=False,sep=';')
ax.plot(eps_mises.iloc[:-1,1]/100,s_true*1000,label='s(fd)-Mises eq (dic)') 
ax.legend(loc='best')

plt.savefig('SE_from_AREA.png',transparent=False)



b=pyg.splitpath(os.getcwd())
df=pd.concat([eps_mises.iloc[:-1,1]/100,s_true*1000], axis=1)
>>>>>>> 6d40c6ea6afa1b47bb905a13ff2b431a2f555725
df.to_excel('True-s-e_fromARAMIS-Atrue-' + b[2] + '.xlsx',header=('e_true_p','s_true'))