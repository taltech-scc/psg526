import matplotlib.pyplot as plt
import numpy as np
import sys
import Write_MaterialCurve_for_INP_29062020 as mat
# C:\Users\mihke\OneDrive - TalTech\Software_support\Python\1_PSG526\PSG526_TT_scripts


se_bruce=np.loadtxt('Bruce_data.txt',skiprows=0)
ep=se_bruce[:,1]

# sys.exit()


sy=329.0000
eplat= 0.022
n= 0.16
A= 800
Q= 268.4746
C1= 4.1080
alfa= 1.1149


fig2, ax2 = plt.subplots(figsize=(5, 4))

#ax2.plot(es.e_true_p,es.s_true,'-k')

# ax2.plot(ep,s_combined(ep,sy,eplat,n,A,Q,C1,alfa),'x-k',markersize=3,label='Combined')
ax2.plot(ep,se_bruce[:,0],'.k',label='Bruce plastic');
ax2.plot(ep,mat.s_swift(ep,sy,eplat,n,A),'-r',label='Swift');
# ax2.plot(ep,s_voce(ep,sy,eplat,Q,C1),'.-g',label='Voce');
ax2.set(xlim=[0, 0.6],ylim=[0, 1000])
#
smodel=mat.s_swift(ep,sy,eplat,n,A)
smodel=np.array(smodel)


#Writes a Material curve .txt file
###########################
nw=open('MatCrv.txt',"w")
for i in range(len(ep)):
    nw.write("{:14.4f}, {:9.4f} \n".format(smodel[i],ep[i]))
nw.close()
###############################

nw=open('MatCrv.py',"w")
nw.write('StressPStrain = ((')
for i,epi in enumerate(ep):
    print(i)
    if i<len(ep)-1:
        nw.write('%5.1f, %6.4f),(' % (10**6*smodel[i],ep[i]))
    else:
        nw.write('%5.1f, %6.4f))\n' % (10**6*smodel[i],ep[i]))
nw.write('#sy= {:6.4f}\n'.format(sy))
nw.write('#eplat= {:6.4f}\n'.format(eplat))
nw.write('#n= {:6.4f}\n'.format(n))
nw.write('#A= {:6.4f}\n'.format(A)) 
nw.close() 
    ##
