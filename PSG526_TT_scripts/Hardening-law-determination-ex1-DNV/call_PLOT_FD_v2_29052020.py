# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:22:36 2020
The purpose of the code
1. Engineering stress strainc curve based on force and extension data
Required input:
    1) Force data
    2) Extension 
        - relative is the % values - used to confirm that calculated stress-
        strain (from FD) is the same with one obtained from Aramis information )
        - w/o relative is the extensometer data in mm
3. Generate f-d curve
    - FD.force_disp(Fnames,t,w,E,sy,L0,np.arange(10,100,10))     
2. Generate True stress strain curve
    - FD.force_disp(Fnames,t,w,E,sy,L0)


v2-29-05-2020
    - All input data in one csv file obtained with Extensometer_mk.py script
    - User needs to input specimen dimension and virtual gauge section lengt
"""

import os
import sys
import glob
# sys.path.append('C:/Work/1_Research/PSG526/Python_files')
# sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
import combine_CSV as mkcom
import python_plotting as pyp
import py_general as pyg
import PLOT_FD_v2_29052020 as FD
print (__name__)
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import time



Fnames=[glob.glob('Bruce-f-d-data.csv')]
# User input parameters & Specimen dimensions
t=8.04; w=12.55; A0=t*w; L0=50

#Elastic modulus and yield stress comes from the ARAMIS
E=204000
sy=328
#----------------------------------------
FD.force_disp(Fnames,t,w,E,sy,L0) #----> true stress strain curve
# FD.force_disp(Fnames,t,w,E,sy,L0,np.arange(10,100,10))
# F=FD.force_disp_only(Fnames,[456,400])
# FD.true_s_e(Fnames,t,w,E,sy,L0)