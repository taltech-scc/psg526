# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 13:54:07 2020

@author: mihkel
Takes the list of CSV files and combines them into one pandas dataframe and 
excel file that is saved afterwards


Be careful, because when filenames are odered by number, it is assumed that number 
begins from 1.
-v2 08:48 03.04.2020
    Extra input variable to deal with not odered data files
-v3 08:48 03.06.2020
    SPecify the data directory, if not specified, then stays in the current dir
"""
import sys
import os
import glob
import pandas as pd
import numpy as np
# sys.path.append('C:/Work/1_Research/Bending/python')


# os.chdir("data")

def combine_CSV(*args):
    # if len(args) > 1:
    #     data_dir=args[1]
    #     os.chdir(data_dir)
    fnames=glob.glob('*.csv')
    nr_of_files=np.size(fnames)
    # print(len(argv))
    # ---------------------------------------------------------------------------------------
    # Use this if files are ordered by filename,1,2,3,...
    li=[]
    if len(args) ==0:
        fnames_ordered=[]
        for i in range(nr_of_files):
            # print(glob.glob('* '+str(i+1)+'.csv'))
            fnames_ordered.append(glob.glob('* '+str(i+1)+'.csv'))  #The format key for the file name
        # fnames_ordered2= [glob.glob('* '+str(i+1)+'.csv') for i in range(nr_of_files)]   #1 liner
        
        
        
        for i in range(nr_of_files): 
            df = pd.read_csv(fnames_ordered[i][0], skiprows=(0),header=0,delim_whitespace=False,sep=',')
            li.append(df)
    # ---------------------------------------------------------------------------------------
# This is the default option    
    if len(args)>0:
        # print(argv)
    # ---------------------------------------------------------------------------------------
    # Use this if you dont care about file ordering 1,10,20,30,
        for file in fnames:  
            df = pd.read_csv(file, skiprows=(0),header=0,delim_whitespace=False,sep=',')
            li.append(df)
    # ---------------------------------------------------------------------------------------
    
    frame2= pd.concat(li, axis=1, ignore_index=False)
    frame2.fillna(0)
    
    # os.chdir('..')
    # frame2.to_pickle('Combined.pkl')   
    frame2.to_excel('Combined.xlsx')    


if __name__ == "__main__":
    combine_CSV()
    # combine_CSV('data',)   #here the data is the directory name