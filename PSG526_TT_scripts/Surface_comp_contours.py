# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 13:54:07 2020

@author: mihkel
Takes the list of CSV files and combines them into one pandas dataframe and 
excel file that is saved afterwards


Be careful, because when filenames are odered by number, it is assumed that number 
begins from 1.
-v2 08:48 03.04.2020
    Extra input variable to deal with not odered data files

"""
import sys
import os
import glob
import pandas as pd
import scipy.interpolate
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
# sys.path.append('C:/Work/1_Research/Bending/python')


# os.chdir("data")

# def combine_CSV(outfile_name,*argv):
    
os.chdir("component_data")
fnames=glob.glob('*.csv')
nr_of_files=np.size(fnames)

df = pd.read_csv(fnames[0], skiprows=(4),header=1,delim_whitespace=False,sep=';')



X, Y= np.meshgrid(df.x, df.y)
Z = scipy.interpolate.griddata((df.x,df.y),df.epsilon_x, (X, Y),method='nearest')




fig2 = plt.figure()
ax2 = plt.axes(projection="3d")
CS = ax2.scatter(df.x, df.y,df.epsilon_x,'o')

# ax.set_aspect('equal')

# ax.set(xlim=[-60.-68],ylim=[0, np.ceil(ymax[1])])



# # ax.set_aspect('equal')
# ax.plot_trisurf(df.x, df.y, df.z)


# sys.exit()

# from mpl_toolkits import mplot3d

# import numpy as np
# import matplotlib.pyplot as plt

# fig = plt.figure()
# ax = plt.axes(projection="3d")




# def z_function(x, y):
#     return np.sin(np.sqrt(x ** 2 + y ** 2))

# x = np.linspace(-6, 6, 30)
# y = np.linspace(-6, 6, 30)

# X, Y = np.meshgrid(x, y)
# Z = z_function(X, Y)

# ax = plt.axes(projection='3d')
# ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
#                 cmap='winter', edgecolor='none')
# ax.set_title('surface');


# sys.exit()    

# # print(len(argv))
# # ---------------------------------------------------------------------------------------
# # # Use this if files are ordered by filename,1,2,3,...
# # li=[]
# # if len(argv) == 0:
# #     fnames_ordered=[]
# #     for i in range(nr_of_files):
# #         # print(glob.glob('* '+str(i+1)+'.csv'))
# #         fnames_ordered.append(glob.glob('* '+str(i+1)+'.csv'))  #The format key for the file name
# #     # fnames_ordered2= [glob.glob('* '+str(i+1)+'.csv') for i in range(nr_of_files)]   #1 liner
    
    
    
# #     for i in range(nr_of_files): 
# #         df = pd.read_csv(fnames_ordered[i][0], skiprows=(0),header=0,delim_whitespace=False,sep=',')
# #         li.append(df)
# # # ---------------------------------------------------------------------------------------

# # if len(argv) > 0:
# #     # print(argv)
# # # ---------------------------------------------------------------------------------------
# # # Use this if you dont care about file ordering 1,10,20,30,
# #     for file in fnames:  
# #         df = pd.read_csv(file, skiprows=(0),header=0,delim_whitespace=False,sep=',')
# #         li.append(df)
# # # ---------------------------------------------------------------------------------------

# # frame2= pd.concat(li, axis=1, ignore_index=False)
# # frame2.fillna(0)

# # os.chdir('..')
# # frame2.to_pickle(outfile_name + '.pkl')   
# # frame2.to_excel(outfile_name + '.xlsx')    


# # if __name__ == "__main__":
# #     combine_CSV('testdata','notcombined')