'''
author: mihkel.korgesaar@taltech.ee
date: 29-06-2020 16:31
Purpose: Writes the true stress strain curve in format suitable for abaqus input file
-v02-24082020
    - When called in main, added the write material curves in python input file format
'''

import matplotlib.pyplot as plt
import numpy as np


# The list of e values where to compare data
ep=[0.0000,0.0025,0.00679,0.0100,0.015, 0.0200,0.0270, 0.0359,0.0387,0.0466,0.0576,0.0692,0.0805,0.0915,0.1026,0.1135,0.1283,0.1443,0.1607,0.1789,0.1980,0.2200,0.2453,0.2760,0.3073,0.3482,0.3867,0.4354,0.4746,0.5185,0.5731,0.6387,0.7010,0.80,0.9,1.,1.1,1.4]


pi=np.pi
it=0


#%% These define the plasticity models with plateau
def s_voce(ep,sy,eplat,Q,C1):
    s_voce=[]
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_voce.append(sy)
        else:
            s_voce.append(sy+Q*(1-np.exp(-C1*(ei))))
    return s_voce

def s_swift(ep,sy,eplat,n,A):
    s_swift=[]
    e0=(sy/A)**(1/n)-eplat
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_swift.append(sy)
        else:
            s_swift.append(A*(ei+e0)**n)
    return s_swift

def s_combined(ep,sy,eplat,n,A,Q,C1,alfa):
    # global ep
    s_model=[];s_swift=[];s_voce=[]
    e0=(sy/A)**(1/n)-eplat
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_model.append(sy)
            s_swift.append(sy)
            s_voce.append(sy)
        else:
            s_swift.append(A*(ei+e0)**n)
            s_voce.append(sy+Q*(1-np.exp(-C1*(ei))))
            s_model.append(alfa*s_swift[i]+(1-alfa)*s_voce[i])
    # n=open('MatCrv.txt',"w")
    # for i in range(len(ep)):
    #     n.write("{:14.4f}, {:9.4f} \n".format(s_model[i],ep[i]))
    # n.close()
    return s_model

if __name__ == "__main__":

# Properties from Matine paper
    sy_m=275*0.99
    K_m=630
    n_m=0.21
    eplat_m=0.012