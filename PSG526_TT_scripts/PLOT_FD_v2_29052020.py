# -*- coding: utf-8 -*-
# """
# @author: mihke
# Created on Thu Mar 12 10:22:36 2020
# The purpose of the code
# 1. Engineering stress strainc curve based on force and extension data
# Required input:
#     1) Force data
#     2) Extension
#         - relative is the % values - used to confirm that calculated stress-
#         strain (from FD) is the same with one obtained from Aramis information )
#         - w/o relative is the extensometer data in mm
# 2. Generate True stress strain curve
# Effect of interpolation size (strain tensor negborhood)

# v2-29-05-2020 (mihkel)
#     - All input data in one csv file obtained with Extensometer_mk.py script
#     - User needs to input specimen dimension and virtual gauge section lengt

# v3-18.06.2020 (mihkel)
#     - separate function for true stress strain curve
    
    
# Where to copy extra scripts called: 
#     1. Make the file .pth in this folder in C:\Users\Mihkel\Anaconda3\Lib\site-packages\mk_example.pth
#     2. Copy the paths where your extra scripts are to this folder
# """

import os
import sys
import glob
# sys.path.append('C:/Work/1_Research/PSG526/Python_files')
# sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
import combine_CSV as mkcom
import python_plotting as pyp
import py_general as pyg
print (__name__)
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import time





def force_disp(Fnames,t,w,E,sy,L0,*args):
# FD.force_disp(Fnames,t,w,E,sy,L0) - call with following input 
# FNAMES - the csv file with disp, engineering strain and force
# t - thickness
# w - width 
# E - elastic modulus
# sy - yield Stress
# L0 - initial virtual gauge length
#-------------------------------------------------------------------------

    # if step data is given engineering stress-strian curve is created at specific step and saved
    if len(args)==1:
        step=args[0]
    else:
        step=None


    A0=t*w
    # SEPARATOR SIGN CHANGED
    # Read in the data and process data
    #-------------------------------------------------------------------------
    data=pd.read_excel(Fnames[0][0],names=["ind", "d", "dL", "F"],header=0)    
    
    
    
    
    # for aramis data use this
    # data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False)
    try:
        d=pd.DataFrame(data.iloc[:,1]);    dL=pd.DataFrame(data.iloc[:,2]);    F=pd.DataFrame(data.iloc[:,3])
    except:
        data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
        d=pd.DataFrame(data.iloc[:,1]);    dL=pd.DataFrame(data.iloc[:,2]);    F=pd.DataFrame(data.iloc[:,3])
 
    
    
# '''
# Initially this was needed but actually it seems to be fine without, because sometimes in the first tests
# Endel started the test before starting the aramis measurement. That is why the high force value, it is not a gimmick values
# but really a force that is applied to the specimen, thus it should not be corrected. 
# # The force data is normalized against first force value
#     # if F.iloc[0,0] < 0:
#     #     F=F+abs(F.iloc[0,0])
#     # else:
#     #     F=F-abs(F.iloc[0,0])  
#     #d = pd.read_csv(Fnames[1][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
#     #dL=pd.read_csv(Fnames[2][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
#     #-------------------------------------------------------------------------
# '''
# Check if there is already a figure present

    # The force data is normalized against first force value
    # if F.iloc[0,0] < 0:
    #     F=F+abs(F.iloc[0,0])
    # else:
    #     F=F-abs(F.iloc[0,0])  

    #d = pd.read_csv(Fnames[1][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
    #dL=pd.read_csv(Fnames[2][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
    #-------------------------------------------------------------------------
    # Check if there is already a figure present
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(2,1,figsize=(14,8))

    #%% Plot Force-displacement
    label_name=Fnames[0][0][-13:]
    a=ax[0]
    a.plot(d.iloc[:,0],F.iloc[:,0]/1000,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=label_name)
    a.legend(loc='best',prop={'size': 5})
    a.set(ylabel='Force (kN)',xlabel='Displacement (mm)')
    # ax[0].grid(b=True, which='major')
    # xmax=a.get_xlim(); ymax=a.get_ylim(); a.set(xlim=[0, np.ceil(max(d.iloc[:,0]))+2],ylim=[0, np.ceil(ymax[1])]); a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))

    #%% Plot Engineering stress-strain on a second subfigure
    # Use the initial cross sectional area of the specimen specified by the user.
    s_eng=F.iloc[:,0]/A0
    e_eng=((d.iloc[:,0])/L0)*100
    a=ax[1]
    a.plot(e_eng,s_eng,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=label_name+' (engineering)')

    #The aramis output (engineering strain from vritual strain gage) is plotted on top to confirm the calculation with measurements
    a.plot(dL.iloc[::2,0],s_eng[::2],'or',markersize=1,Markeredgecolor='none',label='Aramis %')
    
    a.set(ylabel='Stress (MPa)',xlabel='Strain (%)')

    # fig.set_size_inches(pyg.cm2inch(12, 6))
    # plt.tight_layout()
    # b=pyg.splitpath(os.getcwd())
    # plt.savefig('FD_'+ b[2]+ '.png',transparent=False)
    # plt.close(fig)

    """
    Created on Thu Apr 24 2020
    True stress-strain curve based on the engineering stress strain curve defined above
    in a separate figure
    """
    if len(args)==0:
        # In this part i define the elastic modulus if not defined yet
        s_true=s_eng*(1.+e_eng/100)
        e_true=np.log(1.+e_eng/100)

        # fig2, ax2 = plt.subplots(figsize=(8, 8))
        # ax2.plot(e_true*100,s_true,'-ob',linewidth=0.5,markersize=1,label='True stress strain',picker=1)



        # def onpick(event):
        #     thisline = event.artist
        #     xdata = thisline.get_xdata()
        #     ydata = thisline.get_ydata()
        #     ind = event.ind
        #     points = tuple(zip(xdata[ind], ydata[ind]))
        #     print('onpick points:', points)

        # cid=fig2.canvas.mpl_connect('pick_event', onpick)



        e_true_p=np.log(1.+e_eng/100)-sy/E   #The 536 number is taken from the figure.

        # Figure formatting----------------------------------
        a.plot(e_true_p*100,s_true,'-b',linewidth=0.5,markersize=1,label='True stress strain')
        xmax=a.get_xlim(); ymax=a.get_ylim();
        a.set(xlim=[0, np.ceil(max(e_eng))+2],ylim=[0, np.ceil(ymax[1])])
        a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))
        
        a.legend(loc='best',prop={'size': 5})
        fig.set_size_inches(pyg.cm2inch(6, 10))
        
        b=pyg.splitpath(os.getcwd())
        a.set(title=b[0][-5:])
        plt.tight_layout()
        plt.savefig('FD_'+ b[2]+ '.png',transparent=False)
        plt.savefig('FD_'+ b[2]+ '_PDF.pdf',transparent=False)
        # # in a separate figure
        # fig2, ax2 = plt.subplots(figsize=(5, 4))
        # ax2.plot(e_true_p,s_true,'-b',linewidth=0.5,markersize=1,label='True stress strain')
        # ax2.set(xlim=[0, 1],ylim=[0, 1000])
        # ---------------------------------------------------
        # Save the true-stress strain data to excel as well
        df=pd.concat([e_true_p,s_true], axis=1)
        df.to_excel('True-SE_eng_' + Fnames[0][0][:-4] + '.xlsx',header=('e_true_p','s_true'))

    else:
        print(type(step))
        # if type(step) == int:
        fig2, a = plt.subplots(figsize=(7, 7))
        a.plot(e_eng,s_eng,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=Fnames[0][0][:-4])
        a.plot(dL.iloc[step,0],s_eng[step],'.r',markersize=3,Markeredgecolor='none',label=('Aramis step='+str(step)) )
        a.set(ylabel='Stress (MPa)',xlabel='Strain (%)')
        a.legend(loc='best')
        #Tick information
        xmax=a.get_xlim(); ymax=a.get_ylim()
        a.set_xticks(np.linspace(0,np.ceil(xmax[1]),6)); a.set_yticks(np.linspace(0,np.ceil(ymax[1]),6));
        fig2.set_size_inches(pyg.cm2inch(6, 6))
        # plt.tight_layout()
        plt.savefig('FD_step'+ str(step) + '.png',transparent=False)
        plt.savefig('FD_step'+ str(step) + '.pdf',transparent=False)
        # else:
            # for i in step:
                # fig2, a = plt.subplots(figsize=(5, 4))
                # a.plot(e_eng,s_eng,'-',linewidth=0.5,markersize=1,Markeredgecolor='none')
                # a.plot(dL.iloc[i,0],s_eng[i],'or',markersize=3,Markeredgecolor='none',label='Aramis %')
                # a.set(ylabel='Stress (MPa)',xlabel='Strain (%)')
                # savena='FD_step_{:03d}_.png'.format(i)
                # plt.savefig(savena,transparent=False)
                # plt.close(fig2)

def engineering(Fnames,t,w,E,sy,L0,*args):
# FD.force_disp(Fnames,t,w,E,sy,L0) - call with following input 
# FNAMES - the csv file with disp, engineering strain and force
# t - thickness
# w - width 
# E - elastic modulus
# sy - yield Stress
# L0 - initial virtual gauge length
#-------------------------------------------------------------------------
    # if step data is given engineering stress-strian curve is created at specific step and saved
    if len(args)==1:
        step=args[0]
    else:
        step=None
    A0=t*w
    # SEPARATOR SIGN CHANGED
    # Read in the data and process data
    #-------------------------------------------------------------------------
    data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
    d=pd.DataFrame(data.iloc[:,1]);    
    dL=pd.DataFrame(data.iloc[:,2]);    
    F=pd.DataFrame(data.iloc[:,3])
# '''

    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(12, 6))

    # sys.exit()
    #%% Plot Force-displacement
    # a.plot(d.iloc[:,0],F.iloc[:,0]/1000,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=Fnames[0][0][:-4])
    # a.legend(loc='best')
    # a.set(ylabel='Force (kN)',xlabel='Displacement (mm)')
    # ax[0].grid(b=True, which='major')
    # xmax=a.get_xlim(); ymax=a.get_ylim(); a.set(xlim=[0, np.ceil(max(d.iloc[:,0]))+2],ylim=[0, np.ceil(ymax[1])]); a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))

    #%% Plot Engineering stress-strain on a second subfigure
    # Use the initial cross sectional area of the specimen specified by the user.
    label_name=Fnames[0][0][-13:]
    s_eng=F.iloc[:,0]/A0
    e_eng=((d.iloc[:,0])/L0)*100
    ax.plot(e_eng,s_eng,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=label_name+' (eng)')

    #The aramis output (engineering strain from vritual strain gage) is plotted on top to confirm the calculation with measurements
    ax.plot(dL.iloc[::2,0],s_eng[::2],'or',markersize=1,Markeredgecolor='none',label='Aramis %')
    
    ax.set(ylabel='Stress (MPa)',xlabel='Strain (%)')
    xmax=ax.get_xlim(); ymax=ax.get_ylim(); 
    ax.set(xlim=[0, 35],ylim=[0, 500]); 
    ax.set_xticks(np.linspace(0,np.ceil(xmax[1]),6))

    fig.set_size_inches(pyg.cm2inch(7, 6))
    plt.tight_layout()
    b=pyg.splitpath(os.getcwd())
    plt.savefig('FD_'+ b[2]+ '.pdf')
    # plt.close(fig)

#%%
def force_disp_only(Fnames,**input):
# The ways to call this function
# F=FD.force_disp_only(Fnames,step=[300])         --- FNAMES is the experimental data
# F=FD.force_disp_only(Fnames,FDsim=[Fsim,dsim])  --- if you want to add simulation data and its not added already in the calling function



    # if 'FDsim' in input:
    #     FDsim=input['FDsim'] 

    
    # if 'step' in input:
    #     step=input['step'] 
    #     # print('Fsim',FDsim) 
    # else:
    #     step=None    
    
    # INPUT: Fnames=[glob.glob('KK3-f-d-data.csv')], 
    # if step data is given engineering stress-strian curve is created at specific step and saved
    # if len(args)==1:
    #     step=args[0]
    #-------------------------------------------------------------------------
    # Read in the expeirmental data and process data
    # displacement in the virtual gauge
    # relative displacement in the virtual gauge
    # measured force
    # Reading in data from .csv file if data in separate cells:
    
    
    data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False)
    try:
        d_test=pd.DataFrame(data.iloc[:,1]);    dL_test=pd.DataFrame(data.iloc[:,2]);    F_test=pd.DataFrame(data.iloc[:,3])
    except:
        data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
        d_test=pd.DataFrame(data.iloc[:,1]);    dL_test=pd.DataFrame(data.iloc[:,2]);    F_test=pd.DataFrame(data.iloc[:,3])  
    

    #-------------------------------------------------------------------------
    # The force data is normalized against first force value
    #F=F+abs(F.iloc[0,0])

    #-------------------------------------------------------------------------
    # Check if there is already a figure present
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(12, 6))
        print('create from scratch')
    else:
        ax=ax[0]
        print('plot on exisiting')
    a=ax

    # Plot Force-displacement (see plot tuleb juba kutsungist, pole vaja eraldi uuesti lasta)
    # if 'FDsim' in input:
    #     FDsim=input['FDsim'] 
    #     AbaqusCurve=ax.plot(FDsim[1],FDsim[0],'.-',label='Abaqus FD sim')
    # else:
    #     AbaqusCurve=None
    #     print('Abaquse sisend puudub')
    
    label_name=Fnames[0][0][-13:]
    a.plot(d_test.iloc[::2,0],F_test.iloc[::2,0]/1000,'.',linewidth=0.5,markersize=3,color='blue',Markeredgecolor='none',label=label_name)
    
    if 'step' in input:
        step=input['step'] 
        stepCurve=a.plot(d_test.iloc[step,0],F_test.iloc[step,0]/1000,'.r',markersize=10,Markeredgecolor='none',label=('Aramis break point='+str(step)) )
    else:
        stepCurve=None
        print('Step sisend puudub')

    # labs = [l.get_label() for l in lns]
    # ax.legend(lns, labs, loc='best')   
    ax.legend(loc='best')   
    # ax[0].grid(b=True, which='major')
    a.set(ylabel='Force (kN)',xlabel='Displacement (mm)')    

    xmax=a.get_xlim(); ymax=a.get_ylim(); a.set(xlim=[0, np.ceil(max(d_test.iloc[:,0]))],ylim=[0, np.ceil(ymax[1])]); a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))
    fig.set_size_inches(pyg.cm2inch(6, 6))
   # plt.tight_layout()
    b=pyg.splitpath(os.getcwd())
    plt.savefig('FD_'+ b[2]+ '.png',transparent=False)
    plt.savefig('FD_'+ b[2]+ '_PDF.png',transparent=False)
    return (F_test/1000,d_test)
    # plt.close(fig)

def local_extensometer_disp(local_gauge_ARAMIS):

    # Reading in data from .csv file if data in separate cells:
    try:
        data = pd.read_csv(local_gauge_ARAMIS[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=',')
        d_test=pd.DataFrame(data.iloc[:,1])
        dL_test=pd.DataFrame(data.iloc[:,2])
        F_test=pd.DataFrame(data.iloc[:,3])
    except:
        data = pd.read_csv(local_gauge_ARAMIS[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
        d_test=pd.DataFrame(data.iloc[:,1])
        dL_test=pd.DataFrame(data.iloc[:,2])
        F_test=pd.DataFrame(data.iloc[:,3])        
    return dL_test
    # fig=plt.gcf(); ax=fig.axes
    # if ax==[]:
    #     plt.close(fig)
    #     fig, ax = plt.subplots(figsize=(12, 6))
    #     print('create from scratch')
    # else:
    #     ax=ax[0]
    #     print('plot on exisiting')    
    # y2ax=ax.twinx()
    # y2ax.set(ylabel='Log. strain (-)')
    # y2ax.plot(d_test.iloc[::2,0], dL_test.iloc[::2,0]*100,'-', label='Abaqus- Local Strain')





#%%
'''
19.11.2020
author: A.Š.
gg
Kood on force_disp_only edasiarendus, eesmärgiga graafikule lisada
lokaalse extensomeetri kõver.

Selle eelduseks hetkel on, et kaustas oleks kaks .csv faili ja need
mõlemad sisestatakse funktsiooni kutsuvasse faili (call_PLOT_FD_v02_29052020.py).
'''
def force_disp_only_mod(Fnames, Fnames2,*args):
    # if step data is given engineering stress-strian curve is created at specific step and saved
    if len(args)>1:
        step=args[0] 
    else:
        step=None
    # Read in the data and process data
    # displacement in the virtual gauge
    # relative displacement in the virtual gauge
    # measured force
    #-------------------------------------------------------------------------
    # LONGextenso.csv:
    data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=',', engine='python')
    d=pd.DataFrame(data.iloc[:,1]);    dL=pd.DataFrame(data.iloc[:,2]);    F=pd.DataFrame(data.iloc[:,3])
    # SHORTextenso.csv:
    localData = pd.read_csv(Fnames2[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=',', engine='python')
    Locd=pd.DataFrame(localData.iloc[:,1]); LocdL=pd.DataFrame(localData.iloc[:,2]); LocF=pd.DataFrame(localData.iloc[:,3])
    #-------------------------------------------------------------------------
    # Check if there is already a figure present
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(12, 6))
        print('create from scratch')
    else:
        ax=ax[0]
        print('plot on exisiting')
    a=ax
    a2=a.twinx()

    # Abaqus data:
    F_in=args[1]
    d_in=args[2]
    dLoc_in=args[3]
 
    # Plot Force-displacement
    lns1=ax.plot(d_in,F_in,'.-',label='Abaqus- FD simulation')
    lns2=a2.plot(d_in, (dLoc_in/5)*100,'-', label='Abaqus- Local Strain')  
    
    lns3=a.plot(d.iloc[:,0],F.iloc[:,0]/1000,'.-',linewidth=0.5,markersize=2,color='orange',Markeredgecolor='none',label='Aramis-FD- '+Fnames[0][0][9:-16])
    lns4=a.plot(d.iloc[step,0],F.iloc[step,0]/1000,'.r',markersize=10,Markeredgecolor='none',label=('Aramis break point='+str(step)) )

    lns5=a2.plot(d.iloc[:,0],LocdL.iloc[:,0],'-',linewidth=0.5,markersize=2,color='orange' ,Markeredgecolor='none',label='Aramis- '+Fnames2[0][0][9:-16]+'Local Strain')    
    
    # LEGEND
    lns=lns1+lns2+lns3+lns4+lns5
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc='center left')   
    
    a.set(ylabel='Force (kN)',xlabel='Displacement (mm)')    
    a2.set(ylabel='Local Strain (%)')


    # ax[0].grid(b=True, which='major')
    xmax=a.get_xlim(); ymax=a.get_ylim(); 
    a.set(xlim=[0, np.ceil(max(d.iloc[:,0]))],ylim=[0, np.ceil(ymax[1])]); 
    #a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))   # Mess up with x-ax when y2 used
    # a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5)) % messes up things if two y axes

    fig.set_size_inches(pyg.cm2inch(6, 6))
    plt.tight_layout()
    b=pyg.splitpath(os.getcwd())
    #plt.savefig('FD_'+ b[2]+ '.png',transparent=False)
    #plt.savefig('FD_'+ b[2]+ '_PDF.png',transparent=False)
    return F/1000
    # plt.close(fig)

    


def true_s_e(Fnames,t,w,E,sy,L0):
    #True stress strain curve obatined from cnverting engineering stress
    # strain into true stress strain curve. Applicable until point of necking
    A0=t*w
    # Read in the data and process data
    #-------------------------------------------------------------------------
    data = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
    d=pd.DataFrame(data.iloc[:,1]);   F=pd.DataFrame(data.iloc[:,3])

    # # The force data is normalized against first force value
    # if F.iloc[0,0] < 0:
    #     F=F+abs(F.iloc[0,0])
    # else:
    #     F=F-abs(F.iloc[0,0])  
    # Use the initial cross sectional area of the specimen specified by the user.
    s_eng=F.iloc[:,0]/A0
    e_eng=((d.iloc[:,0])/L0)*100

    # plt.close(fig)

    """
    Created on Thu Apr 24 2020
    True stress-strain curve based on the engineering stress strain curve defined above
    in a separate figure
    """
    # In this part i define the elastic modulus if not defined yet
    s_true=s_eng*(1.+e_eng/100)
    e_true=np.log(1.+e_eng/100)


    e_true_p=np.log(1.+e_eng/100)-sy/E   #The 536 number is taken from the figure.
    fig=plt.gcf(); a=fig.axes
    if a==[]:
        plt.close(fig)
        fig, a = plt.subplots(figsize=(12, 6))
        print('create from scratch')
    else:
        a=a[0]
        print('plot on exisiting')
            
    # Figure formatting----------------------------------
    a.plot(e_true_p[::3],s_true[::3],'ok',linewidth=0.3,markersize=0.5,MarkerFaceColor='none')
    # xmax=a.get_xlim(); ymax=a.get_ylim();
    a.set(xlim=[0, 1],ylim=[0, 1000])
    plt.xlabel('True plastic strain (-)') 
    plt.ylabel('True stress (MPa)') 
    a.grid(b=True, which='major')
    # a.set_xticks(np.linspace(0,0.4,5))
    fig.set_size_inches(pyg.cm2inch(9, 9))
    plt.tight_layout()
    # ---------------------------------------------------
    # Save the true-stress strain data to excel as well
    b=pyg.splitpath(os.getcwd())
    df=pd.concat([e_true_p,s_true], axis=1)
    df.to_excel('True-stress-strain_Test_engineering.xlsx',header=('e_true_p','s_true'))
    return (F/1000,e_true_p,s_true)   # 2020-09-15 13:25:20 This is just the output that is needed in one files.




if __name__=="__main__":
    Fnames=[glob.glob('*.csv')]
    # User input parameters & Specimen dimensions
    t=2.0; w=20.90; A0=t*w;
    # Virtual gage section length
    L0=50
    #Elastic modulus and yield stress comes from the ARAMIS
    E=204000
    sy=520
    #----------------------------------------
    # This does not give true s-e curve
    # force_disp(Fnames,t,w,E,sy,L0,np.arange(10,30,10))
    true_s_e(Fnames,t,w,E,sy,L0);
    # This gives the true stress strain curve
    # force_disp(Fnames,t,w,E,sy,L0)
