# -*- coding: utf-8 -*-
"""
Create a contour plot from ARAMIS component exported to CSV file

-v0 14:39 09.04.2020

@author: mihke
"""
import sys
import os
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
import py_general as guf



# Data
os.chdir("PEEQ_on_surf")  
fnames=glob.glob('*.csv')

for i,fnames in enumerate(fnames):
    print(i)
    
        
    nr_of_files=np.size(fnames)
    df = pd.read_csv(fnames, skiprows=(4),header=1,delim_whitespace=False,sep=';')

    x=np.linspace(min(df.x),max(df.x),1000)
    y=np.linspace(min(df.y),max(df.y),1000)
    X2, Y2= np.meshgrid(x, y)
    
    P2 =scipy.interpolate.griddata((df.x,df.y),df[list(df)[-1]], (X2, Y2),method='linear')
    #%%
    fig1, ax1 = plt.subplots(figsize=(14, 3))
    CS = ax1.contourf(X2, Y2, P2, 10)
    ax1.contour(X2, Y2, P2,10,colors=('k',))  # add contour lines, # CS4 = ax2.contour(X, Y, Z, levels,colors=('k',),linewidths=(3,), origin=origin)'
    
    
    # Make a colorbar for the ContourSet returned by the contourf call.
    cbar = fig1.colorbar(CS)
    cbar.ax.set_ylabel('Plastic strain')
    
    
    # ax[0].set(xlim=[50, 65])
    fig1.set_size_inches(guf.cm2inch(12, 3))
    plt.tight_layout()
    ax1.set(title=fnames,ylabel='y',xlabel='x (-)')
    # plt.savefig('data.png',dpi=500,transparent=False)
    
#% 
# """
# plot the xy data in single plane to study the the distance between xy points
# """
    # fig2, ax2 = plt.subplots(figsize=(14, 3))
    ax1.plot(df.x,df.y,'.',color=[1,1,1])
    fig1.set_size_inches(guf.cm2inch(14, 5))
    
    if i==0:
        fig, ax = plt.subplots(figsize=(14, 3))
        ax.plot(df.x,df.y,'.k',picker=5)
        






def onpick(event):
    thisline = event.artist
    xdata = thisline.get_xdata()
    ydata = thisline.get_ydata()
    ind = event.ind
    points = tuple(zip(xdata[ind], ydata[ind]))
    print('onpick points:', points)

fig.canvas.mpl_connect('pick_event', onpick)


