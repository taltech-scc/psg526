'''
author: mihkel.korgesaar@taltech.ee
date: 29-06-2020 16:31
Purpose: Writes the true stress strain curve in format suitable for abaqus input file
-v02-24082020
    - When called in main, added the write material curves in python input file format
'''

import matplotlib.pyplot as plt
import numpy as np


# The list of e values where to compare data
ep=[0.0000,0.0025,0.00679,0.0100,0.015, 0.0200,0.0270, 0.0359,0.0387,0.0466,0.0576,0.0692,0.0805,0.0915,0.1026,0.1135,0.1283,0.1443,0.1607,0.1789,0.1980,0.2200,0.2453,0.2760,0.3073,0.3482,0.3867,0.4354,0.4746,0.5185,0.5731,0.6387,0.7010,0.80,0.9,1.,1.1,1.4]


pi=np.pi
it=0


#%% These define the plasticity models with plateau
def s_voce(ep,sy,eplat,Q,C1):
    s_voce=[]
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_voce.append(sy)
        else:
            s_voce.append(sy+Q*(1-np.exp(-C1*(ei))))
    return s_voce

def s_swift(ep,sy,eplat,n,A):
    s_swift=[]
    e0=(sy/A)**(1/n)-eplat
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_swift.append(sy)
        else:
            s_swift.append(A*(ei+e0)**n)
    return s_swift

def s_combined(ep,sy,eplat,n,A,Q,C1,alfa):
    # global ep
    s_model=[];s_swift=[];s_voce=[]
    e0=(sy/A)**(1/n)-eplat
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_model.append(sy)
            s_swift.append(sy)
            s_voce.append(sy)
        else:
            s_swift.append(A*(ei+e0)**n)
            s_voce.append(sy+Q*(1-np.exp(-C1*(ei))))
            s_model.append(alfa*s_swift[i]+(1-alfa)*s_voce[i])
    # n=open('MatCrv.txt',"w")
    # for i in range(len(ep)):
    #     n.write("{:14.4f}, {:9.4f} \n".format(s_model[i],ep[i]))
    # n.close()
    return s_model

if __name__ == "__main__":

# Properties from Matine paper
    sy_m=275*0.99
    K_m=630
    n_m=0.21
    eplat_m=0.012


    

    # sy= 435.0000
    # eplat= 0.0275
    # n= 0.1965
    # A= 999.7916
    # Q= 181.5320
    # C1= 1.6628
    # alfa= 0.4297
    sy= 275.0000*0.99
    eplat= 0.0120
    n= 0.1702
    A= 724.2875
    Q= 62.2452
    C1= 8.9176
    alfa= 0.7513
    # sy= 275.0000
    # eplat= 0.0120
    # n= 0.21
    # A= 630
    # Q= 526.4528
    # C1= 0
    # alfa= 0.7276


    # s_swift(ep,sy,eplat,n,A)

    # s_voc=s_voce(ep,sy,eplat,Q,C1)   # Voce stress according to input
    # fig2, ax2 = plt.subplots(figsize=(5, 4))
        # Check if there is already a figure present
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax2 = plt.subplots(figsize=(6, 6))
        print('create from scratch')
    else:
        ax2=ax[0]
        print('plot on exisiting')

    #ax2.plot(es.e_true_p,es.s_true,'-k')

    # ax2.plot(ep,s_combined(ep,sy,eplat,n,A,Q,C1,alfa),'-r',markersize=3,label='Combined')
    smodel_combined=s_combined(ep,sy,eplat,n,A,Q,C1,alfa)
    ax2.plot(ep,s_swift(ep,sy_m,eplat_m,n_m,K_m),'--k',label='Swift MATINE');
    s_swift_matine=s_swift(ep,sy_m,eplat_m,n_m,K_m)
    # ax2.plot(ep,s_voce(ep,sy,eplat,Q,C1),'.-g',label='Voce');
    ax2.set(xlim=[0, 1],ylim=[0, 900])
    ax2.set(title='Material 1')
    #
    # smodel=s_swift(ep,sy,eplat,n,A)
    # smodel=np.array(smodel)
    ax2.legend(loc='best')
    ax2.grid(b=True, which='major')

    #Writes a Material curve .txt file
    ###########################
    # nw=open('MatCrv.txt',"w")
    # for i in range(len(ep)):
    #     nw.write("{:14.4f}, {:9.4f} \n".format(smodel[i],ep[i]))
    # nw.close()
    
    nw=open('MatCrv_MATINE.txt',"w")
    for i in range(len(ep)):
        nw.write("{:14.4f}, {:9.4f} \n".format(s_swift_matine[i],ep[i]))
    nw.close()
    
    nw_combined=open('MatCrv_smodel_combined.txt',"w")
    for i in range(len(ep)):
        nw_combined.write("{:14.4f}, {:9.4f} \n".format(smodel_combined[i],ep[i]))
    nw_combined.close()
    ###############################

  # nw=open('MatCrv-8inrow.py',"w")
    # nw.write('StressPStrain = ((')
    # for i,epi in enumerate(ep):
    #     print(i)
    #     if i<len(ep)-1:
    #         nw.write('%5.1f, %6.4f),(' % (10**6*smodel[i],ep[i]))
    #     else:
    #         nw.write('%5.1f, %6.4f))\n' % (10**6*smodel[i],ep[i]))
    # nw.write('#sy= {:6.4f}\n'.format(sy))
    # nw.write('#eplat= {:6.4f}\n'.format(eplat))
    # nw.write('#n= {:6.4f}\n'.format(n))
    # nw.write('#A= {:6.4f}\n'.format(A)) 
    # nw.close() 
    #



    # nw=open('MatCrv.py',"w")
    # nw.write('StressPStrain = ((')
    # for i,epi in enumerate(ep):
    #     print(i)
    #     if i<len(ep)-1:
    #         nw.write('%5.1f, %6.4f),(' % (10**6*smodel[i],ep[i]))
    #     else:
    #         nw.write('%5.1f, %6.4f))\n' % (10**6*smodel[i],ep[i]))
    # nw.write('#sy= {:6.4f}\n'.format(sy))
    # nw.write('#eplat= {:6.4f}\n'.format(eplat))
    # nw.write('#n= {:6.4f}\n'.format(n))
    # nw.write('#A= {:6.4f}\n'.format(A)) 
    # nw.close() 
    #
    
    # ax.annotate(('{:3.1f}MN'.format(5.6)),(400,F_bow),fontsize=4,xytext=(0, 2),textcoords="offset points")
    # ax.annotate(('{:3.1f}MN'.format(6.7)),(400,F_mid),fontsize=4,xytext=(0, 2),textcoords="offset points")
