# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 13:17:05 2020

@author: mihke
The objective is to find the values for plastic hardening models using optimization.
Three models are implemented. Swift, Voce and combination of them.

The values are optimized based on the comparison with experimental data defined
in the true-stress-strain.xlsx file. This file is obtained after processing the
test data (engineering stress strain converted to true stress strain).

Steps
1. Functions of material models
2. Optimization functions
3. Write results to text file
4. Write abaqus material in a script format


V02-2020-09-09 16:37:25
    -The list of compare values was extended, gives a better fit + the starting
    value and end value can be automatically assigned there
    - Obtained parameters written to text file based on the case
    - Material curve is written to text file and python file for easy access
V03-2020-09-15 16:43:17
    - Added the bounded optimization option 

This function optto find the best fit to experimental true stress strain curve

# ------------input-----------------
# e_true_p
# s_true
"""

# input
# e_true_p
# s_true
import scipy.optimize
import os
import sys
import glob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import py_general as pyg
import Write_MaterialCurve_for_INP_29062020 as mat #C:\Users\mihke\OneDrive - TalTech\Software_support\Python\1_PSG526\PSG526_TT_scripts

excel_file=glob.glob('True-s-e_fromARAMIS*.xlsx')
es = pd.read_excel(excel_file[0],  # THis file is read in
                   skiprows=(0),header=0,usecols=(1,2))
es=es.dropna()
# The list of e values where to compare data
# ep=[0.0000,0.0025,0.00679,0.0100,0.015, 0.0200,0.0270, 0.0359,0.0387,0.0466,0.0576,0.0692,0.0805,0.0915,0.1026,0.1135,0.1283,0.1443,0.1607,0.1789,0.1980,0.2200,0.2453,0.2760,0.3073,0.3482,0.3867,0.4354,0.4746,0.5185,0.5731,0.6387,0.7010,0.80,0.9,1.,1.1,1.4]
ep=np.linspace(0.02,0.22,200)   # 2020-09-09 16:39:03

fig, ax = plt.subplots(figsize=(5, 4))
# ax.plot(es.e_true_p[::300],es.s_true[::300],'.k')
pi=np.pi
it=0

sy=275
eplat =0.012
e_maxTest=0.7
ep=np.linspace(eplat,e_maxTest,200)   # 2020-09-09 16:39:03 millest milleni katse punkte loeb
# A=1000;
# n=0.15;
# e0=(sy/A)**(1/n)-eplat
# Q=80;
# C1=15;
# alfa=0.62;



#Define the range where to optimize the output function. This is defined because
# we are not aiming to capture the softening part of the curve. Similarly, plateau
# strains are excluded from the comparison.
_,emin_idx=pyg.find_nearest(es.e_true_p, eplat)
_,emax_idx=pyg.find_nearest(es.e_true_p, e_maxTest)

# Since the ep array is up to very large strains, we dont want to include everything
# to interpolation. So same values are used as in above.
_,efem_min_idx=pyg.find_nearest(ep, eplat)
_,efem_max_idx=pyg.find_nearest(ep, e_maxTest)

#%% These define the plasticity models with plateau
def s_voce(ep,sy,eplat,Q,C1):
    s_voce=[]
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_voce.append(sy)
        else:
            s_voce.append(sy+Q*(1-np.exp(-C1*(ei))))
    return s_voce

def s_voce_double(ep,sy,eplat,Q1,C1,Q2,C2,Q3,C3):
    s_voce_d=[]
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_voce_d.append(sy)
        else:
            s_voce_d.append(sy+Q1*(1-np.exp(-C1*(ei)))+Q2*(1-np.exp(-C2*(ei)))+Q3*(1-np.exp(-C3*(ei))))
    return s_voce_d

def s_swift(ep,sy,eplat,n,A):
    s_swift=[]
    e0=(sy/A)**(1/n)-eplat
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_swift.append(sy)
        else:
            s_swift.append(A*(ei+e0)**n)
    return s_swift

def s_combined(ep,sy,eplat,n,A,Q,C1,alfa):
# This combining true curves is based on the description given in D. Mohr, S.J. Marcadet / International Journal of Solids and Structures 67–68 (2015) 40–55 
# Their description is based on Sung, J.H., Kim, J.N., Wagoner, R.H., 2010. A plastic constitutive equation
# incorporating strain, strain-rate, and temperature. Int. J. Plast. 26 (12), 1746-1771.
    s_model=[];s_swift=[];s_voce=[]
    e0=(sy/A)**(1/n)-eplat
    for i,ei in enumerate(ep):
        if ei < eplat:
            s_model.append(sy)
            s_swift.append(sy)
            s_voce.append(sy)
        else:
            s_swift.append(A*(ei+e0)**n)
            s_voce.append(sy+Q*(1-np.exp(-C1*(ei))))
            s_model.append(alfa*s_swift[i]+(1-alfa)*s_voce[i])
    return s_model


#%% These are the optimization functions


def min_voce_parameter(x):
    global es,sy,eplat,ep,it,line       # Define as global
    it+=1
    Q=x[0];C1=x[1]
    # Not entire test data is used for comparison. These are interpolated stress values from test data
    s_test_interp=np.interp(ep[efem_min_idx:efem_max_idx],
        es.e_true_p[emin_idx:emax_idx], es.s_true[emin_idx:emax_idx])
    s_voc=s_voce(ep[efem_min_idx:efem_max_idx],sy,eplat,Q,C1)   # Voce stress according to input
    # This try catch removes the plotted curve (so to keep the only last value)
    try:
        l=line.pop(0); l.remove()
    except:
        pass            #at first optimisation cycle line is not defined, that is why the pass
    line=ax.plot(ep[efem_min_idx:efem_max_idx],s_voc,'-b')
    ax.set(xlim=[0, 0.25],ylim=[sy-50, 800])
    sse=np.sum((s_voc-s_test_interp)**2.)                           #Calculate the residual between voce model and test data
    ax.plot(ep[efem_min_idx:efem_max_idx],s_test_interp,'xr')       #plot the test data points included in the optimization
    return sse
def min_voce2_parameter(x):
    global es,sy,eplat,ep,it,line       # Define as global
    it+=1
    Q1=x[0];C1=x[1]
    Q2=x[2];C2=x[3]
    Q3=x[4];C3=x[5]
    # Not entire test data is used for comparison. These are interpolated stress values from test data
    s_test_interp=np.interp(ep[efem_min_idx:efem_max_idx],
        es.e_true_p[emin_idx:emax_idx], es.s_true[emin_idx:emax_idx])
    s_voc=s_voce_double(ep[efem_min_idx:efem_max_idx],sy,eplat,Q1,C1,Q2,C2,Q3,C3)   # Voce stress according to input
    # This try catch removes the plotted curve (so to keep the only last value)
    try:
        l=line.pop(0); l.remove()
    except:
        pass            #at first optimisation cycle line is not defined, that is why the pass
    line=ax.plot(ep[efem_min_idx:efem_max_idx],s_voc,'-b')
    ax.set(xlim=[0, e_maxTest],ylim=[sy-50, 1000])
    sse=np.sum((s_voc-s_test_interp)**2.)                           #Calculate the residual between voce model and test data
    ax.plot(ep[efem_min_idx:efem_max_idx],s_test_interp,'xr')       #plot the test data points included in the optimization
    return sse
    
def min_swift_parameter(x):
    global es,sy,eplat,ep,it,line
    it+=1
    n=x[0];A=x[1]
    s_test_interp=np.interp(ep,es.e_true_p[emin_idx:emax_idx], es.s_true[emin_idx:emax_idx])
    # s_test_interp=np.interp(ep,es.e_true_p[115:np.argmax(es.s_true)], es.s_true[115:np.argmax(es.s_true)])
    # fig=plt.gcf(); ax=fig.axes
    s_sw=s_swift(ep,sy,eplat,n,A)
    # s_voc=s_voce(ep,sy,eplat,Q,C1)
    try:
        l=line.pop(0); l.remove()
    except:
        pass
    line=ax.plot(ep,s_sw,'-b')
    ax.set(xlim=[0, 0.5],ylim=[sy-50, 800])
    sse=np.sum((s_sw-s_test_interp)**2.) #Calculate the residual
    return sse

def min_coupled_mat_parameter(x):
    global es,sy,eplat,ep,it,line
    it+=1
    n=x[0]
    A=x[1]
    Q=x[2]
    C1=x[3]
    alfa=x[4]
    s_test_interp=np.interp(ep[efem_min_idx:efem_max_idx],es.e_true_p[emin_idx:emax_idx], es.s_true[emin_idx:emax_idx])
    # s_test_interp=np.interp(ep,es.e_true_p[115:np.argmax(es.s_true)], es.s_true[115:np.argmax(es.s_true)])
    # fig=plt.gcf(); ax=fig.axes
    s_m=s_combined(ep[efem_min_idx:efem_max_idx],sy,eplat,n,A,Q,C1,alfa)
    # s_voc=s_voce(ep,sy,eplat,Q,C1)
    try:
        l=line.pop(0); l.remove()
    except:
        pass
    line=ax.plot(ep[efem_min_idx:efem_max_idx],s_m,'-b')
    ax.set(xlim=[0, 0.25],ylim=[sy-50, 800])
    sse=np.sum((s_m-s_test_interp)**2.) #Calculate the residual
    ax.plot(ep[efem_min_idx:efem_max_idx],s_test_interp,'xr')
    return sse

# Choose the optimization method from one of those
# best_voce = scipy.optimize.fmin(func=min_voce_parameter,x0 = [80,15])
# best_swift = scipy.optimize.fmin(func=min_swift_parameter,x0 = [0.15,500])
# best_combined= scipy.optimize.fmin(func=min_coupled_mat_parameter,x0 = [0.15,500,200,15,0.5])
bnds=((0.01,1), (100,1000), (0,1000), (0,100), (0,2))
best_combined_SLSQP= scipy.optimize.minimize(fun=min_coupled_mat_parameter,x0 = [0.15,500,200,15,0.5],method='SLSQP',bounds=bnds)


# %% Write the obtained parameters to text file 2020-09-09 16:42:36
n=open('Optimized True SE coefficients.txt',"w")
if 'best_combined' in locals():
    n.write('sy= %6.4f\n' % sy)
    n.write('eplat= %6.4f\n' % eplat)
    n.write('n= %6.4f\n' % best_combined[0])
    n.write('A= %6.4f\n' % best_combined[1])
    n.write('Q= %6.4f\n' % best_combined[2])
    n.write('C1= %6.4f\n' % best_combined[3])
    n.write('alfa= %6.4f\n' % best_combined[4])
elif 'best_combined_SLSQP' in locals():
    n.write('sy= %6.4f\n' % sy)
    n.write('eplat= %6.4f\n' % eplat)
    n.write('n= %6.4f\n' % best_combined_SLSQP.x[0])
    n.write('A= %6.4f\n' % best_combined_SLSQP.x[1])
    n.write('Q= %6.4f\n' % best_combined_SLSQP.x[2])
    n.write('C1= %6.4f\n' % best_combined_SLSQP.x[3])
    n.write('alfa= %6.4f\n' % best_combined_SLSQP.x[4])
elif 'best_swift' in locals():
    n.write('sy= %6.4f\n' % sy)
    n.write('eplat= %6.4f\n' % eplat)
    n.write('n= %6.4f\n' % best_swift[0])
    n.write('A= %6.4f\n' % best_swift[1])   
n.close()


#%% Plot the curves 2020-09-09 16:43:42
if 'best_combined' in locals():
    n= best_combined[0]
    A= best_combined[1]
    Q=best_combined[2]
    C1= best_combined[3]
    alfa= best_combined[4]
elif 'best_combined_SLSQP' in locals():
    n= best_combined_SLSQP.x[0]
    A= best_combined_SLSQP.x[1]
    Q=best_combined_SLSQP.x[2]
    C1= best_combined_SLSQP.x[3]
    alfa= best_combined_SLSQP.x[4]
elif 'best_swift' in locals():
    n= best_swift[0]
    A= best_swift[1]


ep=[0.0000,0.002,0.0025,0.00679,0.0100,0.015, 0.0200,0.0270, 0.0359,0.0387,0.0466,0.0576,0.0692,0.0805,0.0915,0.1026,0.1135,0.1283,0.1443,0.1607,0.1789,0.1980,0.2200,0.2453,0.2760,0.3073,0.3482,0.3867,0.4354,0.4746,0.5185,0.5731,0.6387,0.7010,0.80,0.9,1.,1.1,1.4]

fig2, ax2 = plt.subplots(figsize=(5, 4))

ax2.plot(es.e_true_p,es.s_true,'.k')

ax2.plot(ep,s_combined(ep,sy,eplat,n,A,Q,C1,alfa),'-b',markersize=3,label='Combined')
ax2.plot(ep,s_swift(ep,sy,eplat,n,A),'.-r',label='Swift');
ax2.plot(ep,s_voce(ep,sy,eplat,Q,C1),'.-g',label='Voce');
ax2.set(xlim=[0, 1],ylim=[0, 800])
ax2.legend(loc='best')
ax2.grid(b=True, which='major')

plt.savefig('True stress-strain.png',transparent=False)
#This for inset figures if needed to zoom
# axins = ax.inset_axes([0.5, 0.07, 0.5, 0.4])
# axins.tick_params(labelsize=5)
# axins.plot(e,s_swift,'.-r',label='Swift')
# axins.plot(e,s_model,'.-k',label='Combined')
# axins.plot(e,s_voce,'.-g',label='VOce');
# axins.set(xlim=[0, 0.1], ylim=[520,700])



#%% Write material curve to text files 2020-09-09 16:44:34
smodel=mat.s_combined(ep,sy,eplat,n,A,Q,C1,alfa)
smodel=np.array(smodel)


#Writes a Material curve .txt file
###########################
nw=open('MatCrv.txt',"w")
for i in range(len(ep)):
    nw.write("{:14.4f}, {:9.4f} \n".format(smodel[i],ep[i]))
nw.close()
###############################

nw=open('MatCrv.py',"w")
nw.write('StressPStrain = ((')
for i,epi in enumerate(ep):
    print(i)
    if i<len(ep)-1:
        nw.write('%5.1f, %6.4f),(' % (10**6*smodel[i],ep[i]))
    else:
        nw.write('%5.1f, %6.4f))\n' % (10**6*smodel[i],ep[i]))
nw.write('#sy= {:6.4f}\n'.format(sy))
nw.write('#eplat= {:6.4f}\n'.format(eplat))
nw.write('#n= {:6.4f}\n'.format(n))
nw.write('#A= {:6.4f}\n'.format(A)) 
nw.close() 