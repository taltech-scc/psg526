T = readtable('K3-A2-midsection_0226.csv', 'HeaderLines',6);
% plot(T(:,2),T(:,3))



xr=T(:,2); yr=T(:,3); e=T(:,5); 
xr = xr{:,:}; yr = yr{:,:}; e = e{:,:};

xv = linspace(min(xr), max(xr), 1000);
yv = linspace(min(yr), max(yr), 1000);
[X,Y] = meshgrid(xv, yv);
P = griddata(xr, yr, e, X, Y);


% figure(1)
% scatter3(xr, yr, e, '.')
% grid on
% 
% figure(2)
% meshc(X, Y, P)
% grid on

figure(3)
contourf(X, Y, P)
grid on

% 
% %%
% [X2,Y2] = meshgrid(xr, yr);
% P2 = griddata(xr, yr, e, X2, Y2);
% figure(4)
% contourf(X2, Y2, P2)
% grid on