# -*- coding: utf-8 -*-
"""
Created on 3-06-2020
The purpose of the code
1. Plot the aramis output value along the section

Steps
    1. Read excel data that is combined from .csv files written in Aramis
    2. Postprocess the data (here separate vars for coordinate and Eps3)
    3. Plot the data at specific step
    4. Save the resulting plot as .png file
"""

import os
import sys
import glob
# sys.path.append('C:/Work/1_Research/PSG526/Python_files')
# sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
import combine_CSV as mkcom
import python_plotting as pyp
import py_general as pyg
print (__name__)
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import time



def plot_along_section(input_file,step):   
    step=np.array(step)
    step=step-1
    Fnames=[glob.glob(input_file)]
    # Read in the data and process data
    # -------------------------------------------------------------------------
    data = pd.read_excel(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
    coord=pd.DataFrame(data.iloc[:,1::2])
    eps3=pd.DataFrame(data.iloc[:,2::2])
 

    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(6, 6))
        print('create from scratch')
    else:
        ax=ax[0]
        print('plot on exisiting')   
        
    ax.plot(coord.iloc[:,step],eps3.iloc[:,step],'.-',linewidth=0.5,markersize=4,Markeredgecolor='none',label=[step])       
    # ax.legend([step],loc='best') 
    ax.set(xlabel='Coordinate (mm)',ylabel='EPS3 (%)')
    fig.set_size_inches(pyg.cm2inch(12, 6))
    plt.tight_layout()
    b=pyg.splitpath(os.getcwd())
    plt.savefig(input_file[:3]+ b[2]+ '.png',transparent=False) 


if __name__ == "__main__":
    # fig, ax = plt.subplots(figsize=(6, 6))
    # plot_along_section('Surf1.xlsx',[100,150,200])
    plot_along_section('Surf1.xlsx',10)
    # plot_along_section('KK3-thickness-reduction-along-*.xlsx',9)
    # plot_along_section('KK3-thickness-reduction-along-*.xlsx',12)
    # plot_along_section('KK3-thickness-reduction-along-*.xlsx',[100,200,300])
    # ax.legend(loc='best') 
    # secplot.plot_along_section('Surf1.xlsx',[100,200,300])
    # secplot.plot_along_section('KK3-thickness-reduction-along-width*.xlsx',np.arange(1,300,1))
