import sys
sys.path.append('C:/Work/1_Research/Bending/python')
print (__name__)
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
from scipy.stats import norm
from scipy import ndimage
#import time

def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

def speckle_size(fname,initial_im,im_bw,im_height,im_width,max_speck,min_speck):
    max_speck=30; min_speck=3; speck_range=max_speck-min_speck+1

    size = initial_im.size/3   #Total number of pixels in the reference image
    count_binary_black =(size-np.count_nonzero(im_bw)/3)  #Total nr of Speckle px in binary image

    #Initialize some arrays
    Pi_count_range=np.empty([speck_range,2]); Pi_count=np.empty([speck_range,2]); px_in_range_pcnt=np.empty([speck_range,2]);
    Pi_cum=np.empty([speck_range,2])
    D_px=np.flipud(np.linspace(min_speck,max_speck,max_speck-min_speck+1))  # Pixel Diameter list (diameter is edge length)
    Pi_count[:,0]=D_px; Pi_count_range[:,0]=D_px; px_in_range_pcnt[:,0]=D_px; 

    im_r=4
    im_c=int(np.size(D_px)/im_r)


    # These numbers must conform to the D_px array dimensions
    fig, ax = plt.subplots(im_r,im_c)
    # Cycle over all diameter values and plot the edited images together with the kernel
    # for i,D in enumerate(D_px.astype(int)):
    #    print('D=',D,i)

    i=-1   
    for k in range(im_r):
        for j in range(im_c):
            i+=1
            D=D_px.astype(int)[i]
            ellipse=cv.getStructuringElement(cv.MORPH_RECT,(D,D)) #the input to morph_ellipse is diameter, so correct is to use 2R,2R
            Closed_im = cv.morphologyEx(im_bw, cv.MORPH_CLOSE, ellipse)
         #   plt.subplot(2,10,i+1); 
            # plt.subplot(1,speck_range,i+1)
            plt.axes(ax[k,j])
            # ax[i].plot(Closed_im,'gray')
            # ax[k,j].plot(Closed_im,'gray')
            plt.imshow(Closed_im,'gray')
         #   plt.subplot(2,10,i+1); plt.imshow(Closed_im,'gray')
            plt.title('D='+str(D),size='5')
            plt.xticks([]),plt.yticks([]) 
            Pi_count[i,1]=size-np.count_nonzero(Closed_im)/3
            if i==0:
                Pi_count_range[i,1]=size-np.count_nonzero(Closed_im)/3
            else:
                Pi_count_range[i,1]=size-Pi_count[i-1,1]-np.count_nonzero(Closed_im)/3
    # Create a row where each radius is represented as many times to create a histogram
    #pixel_array=np.empty([0,0])
    #for i,D in enumerate(Pi_count_range[:,0]): 
    #     pixel_array=np.append(pixel_array,np.full(int(np.floor(Pi_count_range[i,1])),D))  
    pixel_array=np.empty([0,0])
    for (D,count) in zip(Pi_count_range[:,0], Pi_count_range[:,1]): 
         pixel_array=np.append(pixel_array,np.full(int(np.floor(count)),D))      
    #px_in_range = np.flipud(np.diff(np.flipud(Bi_count)))
    #px_in_range_pcnt=px_in_range/size*100
    # sys.exit()
    #%% Postprocess the morphology operation results
    fig1, ax1 = plt.subplots(1,2,figsize=(6, 4))        
    #plt.subplot(2,2,1)
    px_in_range_pcnt[:,1]=Pi_count_range[:,1]/count_binary_black
    ax1[0].bar(px_in_range_pcnt[:,0],px_in_range_pcnt[:,1])#,tick_label=px_in_range_pcnt[:,0])



    #Histogram test in separate plot
    n, bins, patc= ax1[0].hist(pixel_array,np.flipud(D_px),density=True,facecolor=[0.8,0.8,1],
        edgecolor='k',rwidth=1,linewidth=0.5)
    # n, bins, patc= ax1[0,0].hist(pixel_array,23,density=True,facecolor=[0.8,0.8,1],
    #     edgecolor='k',rwidth=0.98)
    # Add a normal distribution fit to data
    mu, std = norm.fit(pixel_array)
    # Plot the PDF.
    xmin, xmax = ax1[0].get_xlim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)

    pxl_length=im_height/np.size(im_bw,0) # Image height/height in px       Pixli pikkus mm



    ax1[0].plot(x, p, 'k', linewidth=1)


    ax1[0].set(ylabel='Percentage [%]',
                 xticks= np.flipud(D_px[::2]).astype(int))
    ax1[0].set_title(label=fname,fontsize=7)

    #Textbox contaiing the information------------------------------------------
    props = dict(boxstyle='round', facecolor='white', alpha=0.1)
    textstr = '\n'.join((
        r'$\mu_{spec}=%.2f$ px' % (np.around(mu,2), ),
        r'$\mu_{spec}=%.2f$ mm' % (np.around(mu*pxl_length,3), ),
        r'$\sigma_{spec}=%.2f$' % (np.around(std,2), ),
        r'$CV^2_{spec}=%.2f$' % (np.around(std**2/mu**2,2), ),
        r'$1px=%.2f mm$'  % (pxl_length, )))
    ax1[0].text(0.65, 0.95, textstr, transform=ax1[0].transAxes, fontsize=6,
            verticalalignment='top', bbox=props)

    # coefficienf of variation calculated according to Feldman p35 Applied Probability and Stochastic Processes
    # Squared coefﬁcient of variation greater 1.0 implies a signiﬁcant amount
    # of variability.
    # END Textbox contaiing the information--------------------------------------



    Rsum=np.cumsum(np.flipud(px_in_range_pcnt[:,1]*100))
    ax1[1].plot(np.arange(min_speck,max_speck+1,1),Rsum,'-o')
    ax1[1].set(ylim=[0, 100],
                 title='Morph cumulative',
                 xticks=np.arange(min_speck,max_speck,2),
                 ylabel='cumulative percentage [%]')
    #Alternative
    #ax1[0,1].set_ylim([0, 100])
    #ax1[0,1].set_title('asdada')

    fig1.set_size_inches(cm2inch(16, 6))
    plt.tight_layout()
    plt.savefig(('image analysis-' + fname[:-4] + '.png'),dpi=500,transparent=False)