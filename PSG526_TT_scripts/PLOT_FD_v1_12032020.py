# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:22:36 2020
The purpose of the code
1. Engineering stress strainc curve based on force and extension data
Required input:
    1) Force data
    2) Extension 
        - relative is the % values - used to confirm that calculated stress-
        strain (from FD) is the same with one obtained from Aramis information )
        - w/o relative is the extensometer data in mm
2. Generate True stress strain curve
@author: mihke
Effect of interpolation size (strain tensor negborhood)
"""

import os
import sys
import glob
# sys.path.append('C:/Work/1_Research/PSG526/Python_files')
# sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
import combine_CSV as mkcom
import python_plotting as pyp
import py_general as pyg
print (__name__)
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import time



# Identify the files in the folder
Fnames=[glob.glob('Force*.csv'),glob.glob('Extension-*'),glob.glob('Extension_rel*')]

# User input parameters & Specimen dimensions
t=2.04; w=20.81; A0=t*w; L0=50

#Elastic modulus and yield stress comes from the ARAMIS
E=204000
sy=528

# Read in the data
F = pd.read_csv(Fnames[0][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
d = pd.read_csv(Fnames[1][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')
dL=pd.read_csv(Fnames[2][0],skiprows=(0),header=0,delim_whitespace=False,sep=';')

# Check if there is already a figure present
fig=plt.gcf(); ax=fig.axes
if ax==[]:
    plt.close(fig)
    fig, ax = plt.subplots(1,2,figsize=(12, 6))  

#%% Plot Force-displacement
a=ax[0]
a.plot(d.iloc[:,1],F.iloc[:,1]/1000,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=Fnames[0][0][:-4])   
a.legend(loc='best') 
a.set(ylabel='Force (kN)',xlabel='Displacement (mm)')
# ax[0].grid(b=True, which='major')
xmax=a.get_xlim(); ymax=a.get_ylim(); a.set(xlim=[0, np.ceil(max(d.iloc[:,1]))+2],ylim=[0, np.ceil(ymax[1])]); a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))

#%% Plot Engineering stress-strain on a second subfigure
# Use the initial cross sectional area of the specimen specified by the user. 
s_eng=F.iloc[:,1]/A0
e_eng=((L0+d.iloc[:,1])/L0-1)*100
a=ax[1]
a.plot(e_eng,s_eng,'-',linewidth=0.5,markersize=1,Markeredgecolor='none',label=Fnames[0][0][:-4])  

#The aramis output (engineering strain from vritual strain gage) is plotted on top to confirm the calculation with measurements
a.plot(dL.iloc[::2,1],s_eng[::2],'or',markersize=1,Markeredgecolor='none',label='Aramis %')  
a.legend(loc='best') 
a.set(ylabel='Stress (MPa)',xlabel='Strain (%)')

fig.set_size_inches(pyg.cm2inch(12, 6))
plt.tight_layout()
b=pyg.splitpath(os.getcwd())
plt.savefig('FD_'+ b[2]+ '.png',transparent=False)
# plt.close(fig)

"""
Created on Thu Apr 24 2020
True stress-strain curve based on the engineering stress strain curve defined above
in a separate figure
"""
# In this part i define the elastic modulus if not defined yet
s_true=s_eng*(1.+e_eng/100)
e_true=np.log(1.+e_eng/100)

# fig2, ax2 = plt.subplots(figsize=(8, 8))
# ax2.plot(e_true*100,s_true,'-ob',linewidth=0.5,markersize=1,label='True stress strain',picker=1) 



# def onpick(event):
#     thisline = event.artist
#     xdata = thisline.get_xdata()
#     ydata = thisline.get_ydata()
#     ind = event.ind
#     points = tuple(zip(xdata[ind], ydata[ind]))   
#     print('onpick points:', points)

# cid=fig2.canvas.mpl_connect('pick_event', onpick) 



e_true_p=np.log(1.+e_eng/100)-sy/E   #The 536 number is taken from the figure.

# Figure formatting----------------------------------
a.plot(e_true_p*100,s_true,'-b',linewidth=0.5,markersize=1,label='True stress strain')  
xmax=a.get_xlim(); ymax=a.get_ylim();
a.set(xlim=[0, np.ceil(max(e_eng))+2],ylim=[0, np.ceil(ymax[1])])
a.set_xticks(np.linspace(0,np.ceil(xmax[1]),5))

# in a separate figure
fig2, ax2 = plt.subplots(figsize=(5, 4))
ax2.plot(e_true_p,s_true,'-b',linewidth=0.5,markersize=1,label='True stress strain') 
ax2.set(xlim=[0, 1],ylim=[0, 1000])
# ---------------------------------------------------
# Save the true-stress strain data to excel as well
df=pd.concat([e_true_p,s_true], axis=1)
df.to_excel('True-stress-strain_Test_' + b[2] + '.xlsx',header=('e_true_p','s_true'))