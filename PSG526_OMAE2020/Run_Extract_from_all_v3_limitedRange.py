import glob, os
import sys
from shutil import copy2
import subprocess
#sys.path.append('C:/Work/1_Research/Bending/python')
# import python_func_list
# from write_vdisp import change_fortran   - this you need when you want to call the function directly, e.g. change_fortran(ksi,90)
import fileinput
import io
import fun_userdefined as funrep
from contextlib import closing
cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))

#%%Specify the python extract name and input variables

# extr_fname='Extract_max_E_plastic.py'
# extr_fname='Extract_sdvs.py'
extr_fname='Extract_field_ALL.py'
elementnr=1

# fold_list=['2FS-T-L5','2FS-T-L20']
# fold_list=next(os.walk(cwd), ([],[],[]))[1]


# SINGLE LEVEL
for i in fold_list:
    copy2(extr_fname, i); 
    os.chdir(i)
    funrep.change_ODB_name(i,elementnr,extr_fname)
    # os.system('Extract')
#    subprocess.Popen(['Extract.bat'])
    subprocess.run(['Extract.bat'])
#    remove the python file and extract bat file
    file_list=(extr_fname,'Extract.bat')
#    for f in file_list:
#        os.remove(f);
#    for f in glob.glob('*rpy*'):
#        os.remove(f)
    os.chdir("..")




#%% dOUBLE LEVEL
# for j in fold_list:
    # for Le in (5,20):
        # if Le==5:
            # elementnr=2  #30
        # elif Le==20:    
            # elementnr=1     #51
        # fol=j+str(Le)
        
        # if fol[0:3]=='1el':
            # pass
        # else:
            # if Le==5: elementnr=30  #30
            # elif Le==20: elementnr=51     #51
        
        # # os.makedirs(fol)
        # # copy2('vdisp-NODES-imp.for', 'ana1/vdisp-NODES-imp.for')
        # # Copy files to run directory
        # copy2(extr_fname, fol); 
        # os.chdir(fol)
        # funrep.change_ODB_name(fol,elementnr,extr_fname)
        # os.system('Extract')
    # #    remove the python file and extract bat file
        # file_list=(extr_fname,'Extract.bat')
    # #    for f in file_list:
    # #        os.remove(f);
    # #    for f in glob.glob('*rpy*'):
    # #        os.remove(f)
        # os.chdir("..")


          