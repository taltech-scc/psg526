# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *

# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100
File='2FS-T-L5.odb'




stiffs='MERGED-1.STIFFENERS'
plate='MERGED-1.PLATE'
all='MERGED-1.SET-1'


o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   



def getrid():
    getridkeys=session.xyDataObjects.keys() 
    for i in range(0,len(getridkeys)): 
        del session.xyDataObjects[getridkeys[i]] 

# Triaxiality
SDV6st=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    variable=(('SDV6', INTEGRATION_POINT), ), elementSets=(stiffs, ))    
session.writeXYReport(fileName='SDV6-stiff.rpt', appendMode=OFF, xyData=(SDV6st))
getrid()
  
SDV6pl=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    variable=(('SDV6', INTEGRATION_POINT), ), elementSets=(plate, ))    
session.writeXYReport(fileName='SDV6-plate.rpt', appendMode=OFF, xyData=(SDV6pl))  

# Triaxiality
SDV1st=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    variable=(('SDV1', INTEGRATION_POINT), ), elementSets=(stiffs, ))    
session.writeXYReport(fileName='SDV1-stiff.rpt', appendMode=OFF, xyData=(SDV1st))
getrid()
  
SDV1pl=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    variable=(('SDV1', INTEGRATION_POINT), ), elementSets=(plate, ))    
session.writeXYReport(fileName='SDV1-plate.rpt', appendMode=OFF, xyData=(SDV1pl))  
getrid()



# getridkeys=session.xyDataObjects.keys() 
# for i in range(0,len(getridkeys)): 
        # del session.xyDataObjects[getridkeys[i]] 
  
# SDV1=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    # variable=(('SDV1', INTEGRATION_POINT), ), elementSets=(
    # 'MERGED-1.STIFFENERS', )) 
# session.writeXYReport(fileName='SDV1.rpt', appendMode=OFF, xyData=(SDV1))        

# getridkeys=session.xyDataObjects.keys() 
# for i in range(0,len(getridkeys)): 
        # del session.xyDataObjects[getridkeys[i]] 

# SDV6=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    # variable=(('SDV6', INTEGRATION_POINT), ), elementSets=(
    # 'MERGED-1.STIFFENERS', )) 
# session.writeXYReport(fileName='SDV6.rpt', appendMode=OFF, xyData=(SDV6))         
# # x=[]
# vars=session.xyDataObjects.keys() 

# getridkeys=session.xyDataObjects.keys() 
# for i in range(0,len(getridkeys)): 
        # del session.xyDataObjects[getridkeys[i]] 
        

# # The same as status but based on the integration point value       
# SDV3=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    # variable=(('SDV3', INTEGRATION_POINT), ), elementSets=(
    # 'MERGED-1.STIFFENERS', )) 
# session.writeXYReport(fileName='SDV3_status.rpt', appendMode=OFF, xyData=(SDV3))          
        
        
        
        
# status=session.xyDataListFromField(odb=odb, outputPosition=WHOLE_ELEMENT, variable=((
    # 'STATUS', WHOLE_ELEMENT), ), elementSets=(
    # 'Stiffened panel-1.STIFFENEDPANEL', ))
# session.writeXYReport(fileName='status.rpt', appendMode=OFF, xyData=(status))      


   