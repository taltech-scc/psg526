# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 22:40:28 2019
Loads the numpy arrays and saves them as matlab arrays
@author: mihke
"""
import fileinput
import os
import io
import re
import glob
import numpy as np
import scipy.io
files=glob.glob('*.npy')

#j=2
#a=np.load(files[2])
#Matname=files[2][0:4]
#scipy.io.savemat(Matname + '.mat', dict([(Matname,a)]))


#my_dict = {}


for i in files:
    a=np.load(i)
    Matname=i.split('.')[0]           #use the first 4 letters
    scipy.io.savemat(Matname + '.mat', dict([(Matname,a)]))

#
#numbers1 = dict([('x6787658', 5), ('y', -5)])
#print('numbers1 =',numbers1)
