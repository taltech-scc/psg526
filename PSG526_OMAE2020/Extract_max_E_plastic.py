# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *
# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='4PB-PM-L5-sretchbending.odb'
o1 = session.openOdb(name=File)





odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   


# whole model    
# data = session.XYDataFromHistory(name='XYData-1', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD for Whole Model', steps=(
    # 'Step-1', 'Step-2', ), )
# session.writeXYReport(fileName='EP_max.rpt', appendMode=OFF, xyData=(data))          
        
xy_result = session.XYDataFromHistory(name='XYData-1', odb=odb, 
    outputVariableName='Plastic dissipation: ALLPD in ELSET L20SET', steps=(
    'Step-1', 'Step-2', ), )        
session.writeXYReport(fileName='EP20mm_set.rpt', xyData=(xy_result, ))        


   