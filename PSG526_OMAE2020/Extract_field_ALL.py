from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *
from odbAccess import openOdb
import numpy as np
import io
# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
File='2FS-T-L20.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]


# This gives the general data about the model
frames = odb.steps['Step-1'].frames
myInstance = odb.rootAssembly.instances['MERGED-1'.upper()]



numNodes = len(myInstance.nodes)
numElements = len(myInstance.elements)
numFrames = len(frames)


step='Step-1'


# fieldName='SDV6'; savename=fieldName
# Input is strings,
def extract_field_data_for_set(step,fieldName,savename,numFrames,numElements):
    # Create a field value object 34.7
    EE = odb.steps[step].frames[0].fieldOutputs[fieldName]
    # First copy the element labels to array
    # Length of the bulkDataBlocks varies, but the total size corresponds to 2*element numbers
    for i in range(len(EE.bulkDataBlocks)):
        if i==0:
            Strains=np.copy(EE.bulkDataBlocks[i].elementLabels)
        else:
            Strains=np.append(Strains,EE.bulkDataBlocks[i].elementLabels)
        # print 'i=',i
    #Here put all the requested data to array and then resize to create a matrix for output    
    for j in range(numFrames):  
        EE = odb.steps[step].frames[j].fieldOutputs[fieldName]
        for i in range(len(EE.bulkDataBlocks)):
            Strains=np.append(Strains,EE.bulkDataBlocks[i].data)
    
    s3=np.resize(Strains,(numFrames+1,numElements*2))
    s3=s3[:,s3[0,:].argsort()] # Sort the values based on the element nr. 
    np.save(savename,s3)  #Save a numpy array
    del s3,EE,Strains
    # on top of that we ne need to call in spyder to make it readable by matlab
    # import scipy.io
    # scipy.io.savemat('test.mat', dict(x=a))



field_list=['SDV6','SDV1','SDV3']

for i in field_list:
    extract_field_data_for_set(step,i,i,numFrames,numElements)

  
# for i in range(len(EE.bulkDataBlocks)):
        # Strains=np.append(Strains,EE.bulkDataBlocks[i].data)
    # print 'i=',i



# Strains=np.copy(EE.bulkDataBlocks[0].elementLabels)
# Strains=np.append(Strains,EE.bulkDataBlocks[0].data)
# s3=np.resize(Strains,(2,len(Strains)/2))



# EE = odb.steps['Punching'].frames[0].fieldOutputs['SDV6']
# np.empty([2, 2])
# Strains=np.copy(EE.bulkDataBlocks[0].elementLabels)
# Strains=np.insert(Strains,[0],EE.bulkDataBlocks[0].data,axis=1)



# Strains=np.copy(EE.bulkDataBlocks[0].data)
# Strains=np.append(Strains,EE.bulkDataBlocks[0].elementLabels)

# np.save('C:/Work/1_Research/Bending/Case4-Tbeam/2FS/10-2/test',s3)


# region_names=odb.steps['Step-1'].historyRegions.keys()  



# region_names=odb.steps['Step-1'].historyRegions.keys()          # Gives the set names for which history is requested
# # This needs to be manually checked where is the history var located. 
# #In this case, sets are, so i want the last one ['Assembly ASSEMBLY', 'ElementSet MERGED-1.PLATE', 'ElementSet MERGED-1.STIFFENERS', 'Node PUNCH-1.683']
# intreg=region_names[-1]                                     # take the name of the last item                                    
# region=odb.steps['Step-1'].historyRegions[intreg]           # Put the name into right format:  historyRegions
# # udata=region.historyOutputs.keys()  - this yields names:['RF3', 'U3'] 
# u_data=region.historyOutputs['U3'].data
# f_data=region.historyOutputs['RF3'].data

# u= session.XYData(name='u', data=u_data)
# f= session.XYData(name='f', data=f_data)

# U=u*1000
# F=f/1000.




# import visualization
# session.viewports['Viewport: 1'].setValues(displayedObject=o1)     

# # F=session.XYDataFromHistory(name='F', odb=odb, 
    # # outputVariableName='Reaction force: RF3 PI: PUNCH-1 Node 2685 in NSET RP1', 
    # # steps=('Step-1', ), )

# # D=session.XYDataFromHistory(name='D', odb=odb, 
    # # outputVariableName='Spatial displacement: U3 PI: PUNCH-1 Node 2685 in NSET RP1', 
    # # steps=('Step-1', ), )    

# Ep=session.XYDataFromHistory(name='Ep', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD for Whole Model', 
    # steps=('Step-1', ), )    
    
    
# Epstiff = session.XYDataFromHistory(name='Epstiff', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD PI: MERGED-1 in ELSET STIFFENERS', 
    # steps=('Step-1', ), )
# Eplate = session.XYDataFromHistory(name='Eplate', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD PI: MERGED-1 in ELSET PLATE', 
    # steps=('Step-1', ), )


# # session.writeXYReport(fileName='FD.rpt', appendMode=OFF, xyData=(F2, D2,Ep,Epstiff,Eplate))

# session.writeXYReport(fileName='FD.rpt', appendMode=OFF, xyData=(F, U,Ep,Epstiff,Eplate))

