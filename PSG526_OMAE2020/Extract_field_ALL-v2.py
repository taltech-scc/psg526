from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *
from odbAccess import openOdb
import numpy as np
import io
# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()


File='2FS-T-L20.odb'
# File='C:/Work/1_Research/Bending/Case1-MATINE/SP4-PM-Omega/SP4-PM-OMEGA-15/SP4-PM-OMEGA-15.odb'

stepname='S3-punching'
step=stepname

o1 = session.openOdb(name=File)
odb = session.odbs[File]
# This gives the general data about the model
frames = odb.steps[stepname].frames
# myInstance = odb.rootAssembly.instances['Stiffened panel-1'.upper()]


# ------------------DEfining the set where to extract---------------------------
# 1) Use this when extracting from the whole instance.
myInstance = odb.rootAssembly.instances['Stiffened panel-1']
# 2) Use this when extracting from an element set that is part of some instance e.g. MERGED-1.PLATE
# myInstance=odb.rootAssembly.instances['MERGED-1'.upper()].elementSets['PLATE'] 
# 3) Use this when extracting from an element set that is NOT part of some instance e.g. PLATE
# myInstance=odb.rootAssembly.elementSets['PLATE'] 


# numNodes = len(myInstance.nodes)
# numElements = len(myInstance.elements)
numFrames = len(frames)
# https://stackoverflow.com/questions/49110702/inconsistent-output-from-abaqus-odb-using-python-scripting


# Input is strings,
def extract_field_data_for_set(step,fieldName,savename,numFrames,in_instance):
    # Create a field value object 34.7
# This for the whole instance
    # EE = odb.steps[step].frames[0].fieldOutputs[fieldName] 
# This for the subset
    EE = odb.steps[step].frames[0].fieldOutputs[fieldName].getSubset(region=in_instance)
    # First copy the element labels to array
    # Length of the bulkDataBlocks varies, but the total size corresponds to 2*element numbers
    for i in range(len(EE.bulkDataBlocks)):
        if i==0:
            Strains=np.copy(EE.bulkDataBlocks[i].elementLabels)
        else:
            Strains=np.append(Strains,EE.bulkDataBlocks[i].elementLabels)
    # To get the total length of the array. Here it is 2*element nr(in order to later concacentate the arrays and sort
    numEl2=len(Strains)
        # print 'i=',i
    #Here put all the requested data to array and then resize to create a matrix for output    
    for j in range(numFrames):  
        EE = odb.steps[step].frames[j].fieldOutputs[fieldName].getSubset(region=in_instance)
        for i in range(len(EE.bulkDataBlocks)):
            Strains=np.append(Strains,EE.bulkDataBlocks[i].data)
    
    s3=np.resize(Strains,(numFrames+1,numEl2))
    s3=s3[:,s3[0,:].argsort()] # Sort the values based on the element nr. 
    np.save(savename,s3)  #Save a numpy array
    # np.save('testsdv6',s3)  #Save a numpy array
    del s3,EE,Strains
    # on top of that we ne need to call in spyder to make it readable by matlab
    # import scipy.io
    # scipy.io.savemat('test.mat', dict(x=a))



# field_list=['SDV15']
field_list=['SDV6']
field_list=['SDV6','SDV1','SDV3','SDV7','SDV8','SDV15','SDV16']
for i in field_list:
    extract_field_data_for_set(step,i,i,numFrames,myInstance)




# EE = odb.steps[step].frames[0].fieldOutputs['SDV6'].getSubset(region=myInstance)
# # First copy the element labels to array
# # Length of the bulkDataBlocks varies, but the total size corresponds to 2*element numbers
# for i in range(len(EE.bulkDataBlocks)):
    # if i==0:
        # Strains=np.copy(EE.bulkDataBlocks[i].elementLabels)
    # else:
        # Strains=np.append(Strains,EE.bulkDataBlocks[i].elementLabels)
    # numEl2=len(Strains)
    # # print 'i=',i
# #Here put all the requested data to array and then resize to create a matrix for output    
# for j in range(numFrames):  
    # EE = odb.steps[step].frames[0].fieldOutputs['SDV6'].getSubset(region=myInstance)
    # for i in range(len(EE.bulkDataBlocks)):
        # Strains=np.append(Strains,EE.bulkDataBlocks[i].data)

# s3=np.resize(Strains,(numFrames+1,numEl2))
# s3=s3[:,s3[0,:].argsort()] # Sort the values based on the element nr. 
# np.save('savetest2',s3)  #Save a numpy array
# # np.save('testsdv6',s3)  #Save a numpy array
# del s3,EE,Strains



