import glob, os
import sys
from shutil import copy2
import subprocess
# import python_func_list
# from write_vdisp import change_fortran   - this you need when you want to call the function directly, e.g. change_fortran(ksi,90)
import fileinput
import io
from contextlib import closing
cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))

#%%Specify the python extract name and input variables

# extr_fname='Extract_max_E_plastic.py'
#extr_fname='Extract_sdvs.py'
# extr_fname='Extract_FD_history.py'
extr_fname='Extract_field_vars.py'
elemntnr=0

# %%Changes the name in the python file according to folder name that is also the ODB file name
#Changes the name in the python file according to folder name that is also the ODB file name
def change_ODB_name(fname,elementnr):
    i=0
    with fileinput.FileInput(extr_fname,inplace = 1) as file:
        for line in file:
            i+=1
            if i==19:
                print('File=\''+fname+'.odb\'')
            elif i==20:
                print('elementnr='+str(elementnr))
            else:
                print(line,end=''),
    n=open('Extract.bat',"w")
#    n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2016.4.246\windows\bin\ifortvars.bat" intel64 vs2013'); n.write('\n')
    n.write('call abq6141 cae noGUI=' +extr_fname)
    n.close()  

#%%
for Le in (5,20):
    if Le==5:
        elementnr=59
    elif Le==20:
        elementnr=51
    fol='2FS-TBM-'+str(Le)
    # os.makedirs(fol)
    # copy2('vdisp-NODES-imp.for', 'ana1/vdisp-NODES-imp.for')
    # Copy files to run directory
    copy2(extr_fname, fol); 
    os.chdir(fol)
    change_ODB_name(fol,elementnr)
    subprocess.Popen(['Extract.bat'])   
    # os.system('Extract')
#    remove the python file and extract bat file
    file_list=(extr_fname,'Extract.bat')
    # for f in file_list:
        # os.remove(f);
    # for f in glob.glob('*rpy*'):
        # os.remove(f)
    os.chdir("..")


          