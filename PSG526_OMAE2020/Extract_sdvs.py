import os
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *
# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100


File='4PB-PM-L5-sretchbending.odb'
elementnr=59



o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   


    
data=session.xyDataListFromField(odb=odb, outputPosition=INTEGRATION_POINT, 
    variable=(
    ('SDV1', INTEGRATION_POINT), 
    ('SDV6', INTEGRATION_POINT), 
    ('SDV7', INTEGRATION_POINT), 
    ('SDV8', INTEGRATION_POINT), 
    ('SDV15', INTEGRATION_POINT), ), 
    elementLabels=(('PART-1-1', (str(elementnr), )), ))
    
data=session.xyDataListFromField(odb=odb, outputPosition=WHOLE_ELEMENT, variable=((
    'STATUS', WHOLE_ELEMENT), ), elementLabels=(('PART-1-1', ((str(elementnr)), )), ))    
    
#session.writeXYReport(fileName='SDVs.rpt', appendMode=OFF, xyData=(data))          
session.writeXYReport(fileName='STATUS.rpt', appendMode=OFF, xyData=(data))        
        
        