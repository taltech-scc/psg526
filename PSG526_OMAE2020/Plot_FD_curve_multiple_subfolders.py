import os
import subprocess
import sys
import glob
from shutil import copy2
import fileinput
import io
print (__name__)
from contextlib import closing
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))





# # folderlist=['KK3']
# FD=glob.glob('KK3-sim/FDKK3*')



# F=np.loadtxt(FD,skiprows=3,usecols = (1))/1000 #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
# d=np.loadtxt(FD,skiprows=3,usecols = (2))



# sys.exit()


# ax.plot(d,F,label=item)

fig=plt.gcf(); ax=fig.gca()
if ax==[]:
    plt.close(fig)
    fig, ax = plt.subplots(figsize=(12, 6))  


#Simulation results
# folderlist=['KK3-sim']
folderlist=['KK3-sim']

co=['#3a6294','#5bc68f','#cb3700','r','k']; ma=['--','-',':']  #color and linestyle arrays
for i,it in enumerate(folderlist):
    os.chdir(it)
    Files1=glob.glob('**/*.rpt')
    for j,item in enumerate(Files1):
        F=np.loadtxt(item,skiprows=3,usecols = (1))/1000 #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
        d=np.loadtxt(item,skiprows=3,usecols = (2))
        ax.plot(d,F,label=item)
    # os.chdir("..")
# ax.legend(loc='upper left',bbox_to_anchor=(0,1),prop={'size': 5})    
ax.legend(loc='best')
# ax.set(ylim=[0,400],xlim=[0, 300])    


plt.grid(which='major') 
plt.minorticks_off()       
plt.xlabel('Displacement (mm)') 
plt.ylabel('Force (kN)') 


# Experimental data
# Test = pd.read_excel('CH1-3.xlsx',header=1)

# ax.plot(Test.mm,Test.N/1000,'.')
# ,usecols = (1))/1000




# def cm2inch(*tupl):
#     inch = 2.54
#     if isinstance(tupl[0], tuple):
#         return tuple(i/inch for i in tupl[0])
#     else:
#         return tuple(i/inch for i in tupl)


# fig.set_size_inches(cm2inch(8, 8))
# plt.savefig('FD-liu1-PMmodified-extras.png',transparent=True)
