# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *



# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
File='SP4-2,5-UMAT.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]


Stepname='S3-punching'

region_names=odb.steps[Stepname].historyRegions.keys()          # Gives the set names for which history is requested
# This needs to be manually checked where is the history var located. 
#In this case, sets are, so i want the last one ['Assembly ASSEMBLY', 'ElementSet MERGED-1.PLATE', 'ElementSet MERGED-1.STIFFENERS', 'Node PUNCH-1.683']
intreg=region_names[-1]                                     # take the name of the last item                                    
region=odb.steps[Stepname].historyRegions[intreg]           # Put the name into right format:  historyRegions
# udata=region.historyOutputs.keys()  - this yields names:['RF3', 'U3'] 
u_data=region.historyOutputs['U2'].data
f_data=region.historyOutputs['RF2'].data

u= session.XYData(name='u', data=u_data)
f= session.XYData(name='f', data=f_data)

U=-u*1000     # mm
F=-f/1000.    # kN




import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)     



# Ep=session.XYDataFromHistory(name='Ep', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD for Whole Model', 
    # steps=('Step-1', ), )    
    
    
# Epstiff = session.XYDataFromHistory(name='Epstiff', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD PI: MERGED-1 in ELSET STIFFENERS', 
    # steps=('Step-1', ), )
# Eplate = session.XYDataFromHistory(name='Eplate', odb=odb, 
    # outputVariableName='Plastic dissipation: ALLPD PI: MERGED-1 in ELSET PLATE', 
    # steps=('Step-1', ), )


# session.writeXYReport(fileName='FD.rpt', appendMode=OFF, xyData=(F2, D2,Ep,Epstiff,Eplate))

session.writeXYReport(fileName='FD.rpt', appendMode=OFF, xyData=(F, U))

