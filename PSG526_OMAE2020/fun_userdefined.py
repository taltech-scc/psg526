# replace input file name inside the code and also
# the file name in the folder to reflect on the folder name
import fileinput
import os
import io
import re
import numpy as np
import glob
import scipy.io
def change_files(cpus):
    cwd=os.getcwd()
    folder=os.path.basename(os.path.normpath(cwd))
    # texttoreplace='File'
    
    # Change the filename according to pattern
    a=glob.glob('*.inp')        # get the current filename
    dst=folder+ ".inp"          # newfile name
    os.rename(a[0], dst)        # change the name
        
    # get fortran filname
    fort_name=glob.glob('*.for')   
    n=open('solveModel.bat',"w")
    n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2016.4.246\windows\bin\ifortvars.bat" intel64 vs2013'); n.write('\n')
    n.write('call abq6141 job=' +folder+ ' user=' +fort_name[0]+ ' cpus='+str(cpus)+ ' domains='+str(cpus)+' memory="1000" parallel=domain double=explicit INTERACTIVE>out.txt\n')
    n.write('REM call abq6141 cae noGUI=Extract.py\n')
    n.write('del *.pac; *.prt; *.res; *.sel; *.stt; *.mdl; *.com;*.par;*.pes;*.pmg; *.abq;\n')
    n.write('REM exit\n')
    n.close()     
            

# ------------------------------------------------------------------------------
# 1. Changes the name in the python extract file according to folder name that is also the ODB file name
#Changes the name in the python file according to folder name that is also the ODB file name
# 2. Write the extract bat

# Input
# fol = Folder name and also the ODB file name
# elementnr= element where you want to extract information (Extract_SDVS.py) 
# extr_fname=python file name that is used to extract information
def change_ODB_name(fol,elementnr,extr_fname):
    i=0
    with fileinput.FileInput(extr_fname,inplace = 1) as file:
        for line in file:
            i+=1
            if i==19:
                print('File=\''+fol+'.odb\'')
            elif i==20:
                print('elementnr='+str(elementnr))
            else:
                print(line,end=''),
    n=open('Extract.bat',"w")
#    n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2016.4.246\windows\bin\ifortvars.bat" intel64 vs2013'); n.write('\n')
    n.write('call abq2020 cae noGUI=' +extr_fname)
    n.close()  
# ------------------------------------------------------------------------------


def convert_np_2_matlab():
    files=glob.glob('*.npy')
    for i in files:
        a=np.load(i)
        Matname=i.split('.')[0]           #use the first 4 letters
        scipy.io.savemat(Matname + '.mat', dict([(Matname,a)]))

            
# n=open('solveModel.bat',"w")
# n.write('#!/bin/bash -l\n')
# n.write('#SBATCH -J alfa=' +str(angle)+ '_'+str(ksi*100)+ '\n')
# n.write('#SBATCH -e e1_%j_err\n')
# n.write('#SBATCH -o e1_%j_out\n')
# n.write('#SBATCH --mem-per-cpu=1000\n')
# n.write('#SBATCH -t 00:10:00\n')
# n.write('#SBATCH -N 1\n')
# n.write('#SBATCH --ntasks-per-node=8\n')
# n.write('#SBATCH -n 8\n')
# n.write('#SBATCH -p serial\n')
# n.write('#SBATCH -D .\n')
# n.write('unset SLURM_GTIDS\n') 
# n.write('module load abaqus/6.13-3\n')
# n.write('abq6133 job=MASTER user=vdisp-NODES-imp cpus=8 memory="1000" double=explicit INTERACTIVE>out.txt\n')
# n.write('abq6133 cae noGUI=Extract_data.py\n')
# n.write('zip RES.zip *.odb *.for *.inp *.py *.rpt *.msg *.dat\n')
# n.write('rm -f MAS* *.inp *.rpy')
# n.close()