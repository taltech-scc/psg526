# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *


# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='MASTER.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   



e1= session.XYDataFromHistory(name='EPS-in', odb=odb, 
    outputVariableName='Equivalent plastic strain: PEEQ at Element 822 Int Point 1 Sec Pt fraction = -0:774597 in ELSET ONSET', 
    steps=('Step-1', ), )
e2= session.XYDataFromHistory(name='EPS-out', odb=odb, 
    outputVariableName='Equivalent plastic strain: PEEQ at Element 1141 Int Point 1 Sec Pt fraction = -0:774597 in ELSET AWAY', 
    steps=('Step-1', ), )
T1 = session.XYDataFromHistory(name='T-in', odb=odb, 
    outputVariableName='Stress triaxiality: TRIAX at Element 822 Int Point 1 Sec Pt fraction = -0:774597 in ELSET ONSET', 
    steps=('Step-1', ), )
T2 = session.XYDataFromHistory(name='T-out', odb=odb, 
    outputVariableName='Stress triaxiality: TRIAX at Element 1141 Int Point 1 Sec Pt fraction = -0:774597 in ELSET AWAY', 
    steps=('Step-1', ), )
    

# x0 = session.xyDataObjects['E_int']
# x1 = session.xyDataObjects['E_kin']
# x2 = session.xyDataObjects['F']
# x3 = session.xyDataObjects['U']
session.writeXYReport(fileName='Triax-eps-hist.rpt', appendMode=OFF, xyData=(e1, e2, T1, T2))