import os
import subprocess
import sys
sys.path.append('C:/Work/1_Research/Bending/python')
import glob
from shutil import copy2
# import fun_replace_line_AND_filename as fun_rep
# import python_func_list
# from write_vdisp import change_fortran   - this you need when you want to call the function directly, e.g. change_fortran(ksi,90)
import fileinput
import io
import fun_userdefined as funrep
print (__name__)
from contextlib import closing
#import fun_replace_line_AND_filename as fun_rep
cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))
umat_name=glob.glob('*.for')
fold_list=next(os.walk(cwd), ([],[],[]))[1]


for i in fold_list:
    # os.makedirs(fol)
    # copy2('vdisp-NODES-imp.for', 'ana1/vdisp-NODES-imp.for')   
    # Copy files to run directory
    copy2(umat_name[0],i)
    os.chdir(i)
    funrep.change_files(16)
    # os.system('runPython')
    # subprocess.run(['solveModel.bat'], stdout=subprocess.DEVNULL)
    # https://stackabuse.com/executing-shell-commands-with-python/: allows continuing the execution of the command
    subprocess.Popen(['solveModel.bat'])    
    # subprocess.Popen(os.system('solveModel'))
    os.chdir("..")

