# replace input file name inside the code and also
# the file name in the folder to reflect on the folder name

import sys
import fileinput
import os
import io
import re
import glob
cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))
# texttoreplace='File'

# Change the filename according to pattern


def change_files(cpus):
    cwd=os.getcwd()
    folder=os.path.basename(os.path.normpath(cwd))
    # texttoreplace='File'
    
    # Change the filename according to pattern
    a=glob.glob('*.inp')        # get the current filename
    dst=folder+ ".inp"          # newfile name
    os.rename(a[0], dst)        # change the name
    umat_name=glob.glob('*.for')
        
    # get fortran filname
    fort_name=glob.glob('*.for')   
    n=open('solveModel.bat',"w")
    n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2017.6.270\windows\bin\ifortvars.bat" intel64'); n.write('\n')
    if not umat_name: 
        n.write('call abq2020 job=' +folder+ ' cpus='+str(cpus)+ ' domains='+str(cpus)+' memory="1000" parallel=domain double=explicit INTERACTIVE>out.txt\n')
    else:
         n.write('call abq2020 job=' +folder+ ' user=' +fort_name[0]+ ' cpus='+str(cpus)+ ' domains='+str(cpus)+' memory="1000" parallel=domain double=explicit INTERACTIVE>out.txt\n')         
    n.write('del *.pac; *.prt; *.res; *.sel; *.stt; *.mdl; *.com;*.par;*.pes;*.pmg; *.abq;\n')
    n.write('REM exit\n')
    n.close()   

change_files(4)