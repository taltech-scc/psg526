import sys
# sys.path.append('C:/Work/1_Research/Bending/python')
# print (__name__)
import matplotlib.pyplot as plt
import numpy as np
# import glob
# from shutil import copy2
# import fileinput
# import io

nf=0.1
pi=np.pi
T=np.arange(-0.66,2/3,0.001)

# fig, ax = plt.subplots()fr
# %Fracture data from Matine (0.5 mm element), CH,NT,SHEAR
def experimental(leglabel):
    ef=np.array([1.16,0.812,0.918])
    eta=np.array([0.406, 0.595, 0.103])
    ax.plot(eta,ef,'xb',markersize=2)


def plot_5mm(leglabel):     # % This is fracture for 5 mm
    p1 = -56.586
    p2 = 40.263
    p3 = 10.011
    p4 = 1.773
    p5 = -0.20133
    p6 = 0.65756
    ef_fit = p1*T**5 + p2*T**4 +p3*T**3 + p4*T**2 + p5*T + p6
    ax=plt.gca()
    ax.plot(T,ef_fit,label=leglabel)
    ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7]) 

def plot_20mm(leglabel):        # % This is fracture for 20 mm  
    p1 = 200.29
    p2 = 184.35
    p3 = 26.433
    p4 = -12.481
    p5 = 0.90018
    p6 = 0.69658
    p7 = 0.19975
    ef_fit =  p1*T**6 + p2*T**5 +p3*T**4 + p4*T**3 + p5*T**2 + p6*T + p7
    ax=plt.gca()
    ax.plot(T,ef_fit,label=leglabel)
    ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7]) 
def plot_DSSE():
    # current calibration
    # Lcal=0.5; tcal=3;
    # A=630
    # n=0.21   
    a=1.3762; b=1.4113; c=0.0088
    T=np.arange(0.333,2/3,0.001)
    # %Necking criterion
    g1=T*3/2+np.sqrt(1/3-3/4*T**2)
    g2=T*3/2-np.sqrt(1/3-3/4*T**2)
    p=0.01
    # % ef_pst=fracture strain in PST, used in mathcad
    d=1.70822 #%(d=1.70822solved using mathcad)
    e_DSSE=b*((0.5*((g1-g2)**d+g1**d+g2**d))**(1/d))**(-1/p)
    ax=plt.gca()
    ax.plot(T,e_DSSE,'--k',label='DSSE',lw=0.5)
    ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7]) 
def plot_HC(a,b,c,leglabel,**input):
    
# best=np.array([1.3762,    1.4113,    0.0088])
# a=best(1);b=best(2);c=best(3);
    nf=0.1
    T=np.arange(-0.66,2/3,0.001);
    
    theta=1-2/pi*np.arccos(-27/2*T*(T**2-1/3))     #Lode parameter
    f1=2/3*np.cos(pi/6*(1-theta))
    f2=2/3*np.cos(pi/6*(3+theta))
    f3=-2/3*np.cos(pi/6*(1+theta))
    
    g=((0.5*((f1-f2)**a+(f1-f3)**a+(f2-f3)**a))**(1/a)+c*(2*T+f1+f3))**(-1/nf)
    ef_fit=b*(1+c)**(1/nf)*g
    ax=plt.gca()
    if input['space']=='stress':
        ax.plot(T,ef_fit,'-',label=leglabel,lw=0.5)
    else:
        a=-(3.*T**2+T*np.sqrt(3*(4-9*T**2))-2)/(6*T**2-2)   
        e1m=ef_fit*np.sqrt(3)/2/np.sqrt(1+a+a**2)
        e2m=e1m*a   
        ax.plot(e2m,e1m,'-r',label='HC')  
def plot_CL(L,t):
    T=np.arange(-0.6,2/3,0.001)
    D=2*(1+T*np.sqrt(12-27*T**2))/(3*T+np.sqrt(12-27*T**2))
    ax=plt.gca()
    # ax.plot(T,0.5/D,'-r',label='CL 0.5')
    a=0.207488; b=1; c=0.2955;
    ec=a*(L/t)**(-b)+c
    ax.plot(T,ec/D,'-',color=[0.5,0.5,0.5],label='CL L'+str(L),lw=0.5)
    ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7]) 

def plot_EPS(L,t):
    T=np.arange(-0.6,2/3,0.001)
    ax=plt.gca()
    a=0.207488; b=1; c=0.2955;
    ec=a*(L/t)**(-b)+c
    ax.plot([-0.6,0.7],[ec,ec],'-',color=[0.5,0.5,0.5],label='EPS L'+str(L))
    ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7])     


def MMC(A,n,*args,**input):
    d=np.size(args)
    if np.size(args)==0:
        # C1=0.02166327;C2=353.494750;  C3=0.995039;
        #Matine paper
        # C1=0.1059;C2=413.82;  C3=1.102;      #Le=0.1 mm
        C1=0.0217;C2=353.49;  C3=0.995; n=0.294     #Le=0.5 mm
    else:
        C1=args[0];C2=args[1];  C3=args[2]
    T=np.arange(-0.66,2/3,0.001)
    ef=1
    ef=(A*(C3+np.sqrt(3)*(1-C3)*(1/np.cos((1/3)*np.arcsin((27/2)*T**3-(9/2)*T))-1)/
        (2-np.sqrt(3)))*((1/3)*np.sqrt(3*C1**2+3)*np.cos((1/3)*np.arcsin((27/2)*T**3-(9/2)*T))
        +C1*(T-(1/3)*np.sin((1/3)*np.arcsin((27/2)*T**3-(9/2)*T))))/C2)**(-1/n)
    ax=plt.gca()
    if input['space']=='stress':
        ax.plot(T,ef,'-r',label='MMC')
    else:
        a=-(3.*T**2+T*np.sqrt(3*(4-9*T**2))-2)/(6*T**2-2)   
        e1m=ef*np.sqrt(3)/2/np.sqrt(1+a+a**2)
        e2m=e1m*a   
        ax.plot(e2m,e1m,'-r',label='MMC')  
    #return ef,T,d
    
def FS2_ex(Le,te,n,leglabel,*args,**input):
    # % fracture strain for shells 
    #Default set based on matine-------------
     #n=0.21;
    if np.size(args)==0:
        A=630;C1=0.02166327;C2=353.494750;  C3=0.995039;  #These I have used in most papers, #Matine paper Le=0.5 mm
    else:
        C1=args[0];C2=args[1];  C3=args[2];    A=args[3];    
    MMC(A,n,C1,C2,C3,space='stress')
    Lcal=0.5; tcal=3;       #these are Matine experimental data
    nswift=n
    for i in input.items():
        Lcal=input['Lcal']
        tcal=input['tcal']
        nswift=input['nswift']
    print(Lcal,tcal)
    # if input['Lcal']==True:
    #     Lcal=1
    # else:
    # Current calibration (MATINE)
        
    # nswift=0.294
    # end of default--------------------
    
    # Swift necking
    T=np.arange(0.33,2/3,0.001)
    # T=np.arange(-0.66,2/3,0.001)
    ef2=(A*(C3+np.sqrt(3)*(1-C3)*(1/np.cos((1/3)*np.arcsin((27/2)*T**3-(9/2)*T))-1)/
        (2-np.sqrt(3)))*((1/3)*np.sqrt(3*C1**2+3)*np.cos((1/3)*np.arcsin((27/2)*T**3-(9/2)*T))
        +C1*(T-(1/3)*np.sin((1/3)*np.arcsin((27/2)*T**3-(9/2)*T))))/C2)**(-1/n)
    b=np.empty([np.size(T)])
    j=0
    for t in T:     
        if t <= 1/np.sqrt(3):
            b[j]=(9*t**2-3*t*np.sqrt(12-27*t**2)+2)/(18*t**2-2)
        else:
            b[j]=(9*t**2+3*t*np.sqrt(12-27*t**2)+2)/(18*t**2-2)
        j+=1
    e1=(2*(2-b)*(1-b+b**2))/(4-3*b-3*b**2+4*b**3)*nswift
    e2=(2*(2*b-1)*(1-b+b**2))/(4-3*b-3*b**2+4*b**3)*nswift   
# %Necking strains on triaxiality and failure strain space
    alfa=(2*b-1)/(2-b)
    eneck=2/np.sqrt(3)*np.sqrt(1+alfa+alfa**2)*e1 
    # %fracture locus for large elements
    ex=eneck+(ef2-eneck)*Lcal/tcal
    ef_l=eneck+(ex-eneck)*te/Le
    ax=plt.gca()
    ax.plot(T,eneck,'-k',label='Swift',lw=0.5)
    ax.plot(T,ef_l,'-b',label=leglabel,lw=0.5)
    
    ax.plot([-0.6,1/3],[ef_l[0],ef_l[0]],'-b',lw=0.5)
    ax.set(ylim=[0,2],xlim=[-0.6, 0.7])  
    return T,ef_l

def DNV_RP_C204(L,t):
    ax=plt.gca()
    ec=0.02+0.65*t/L
    ax.plot([-0.6,0.7],[ec,ec],'-',color=[0.5,0.5,0.5],label='DNV '+str(L))
    ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7])        
      
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)
    
    
if __name__ == "__main__":
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=cm2inch(6, 6))
        print('create from scratch')
    else:
        ax=ax[0]
        print('plot on exisiting')   
    # FS2_ex(50,30,0.16,'test',0.02166327,300.494750,0.9)
    # experimental('test')
    # FS2_ex(1,3,0.294,'test')
    # plot_HC(1.3762,1.4113,0.0088,'HC matine cal') 
    # plot_DSSE() 
    # DNV_RP_C204(50,25)
    # plot_CL(20,3); plot_CL(5,3);
    # plot_5mm('5mm criteria')
    # plot_20mm('20mm criteria')
    # MMC(630,0.21,space='strain')
    # MMC(630,0.294,space='stress')
    plot_HC(1.3762,1.4113,0.0088,'HC matine cal',space='strain') 
    # MMC(730,0.3,0.02,350,0.98,space='stress')
    # MMC(944,0.29,0.06,543,1,space='stress')
    # USED in ISSC Tsim,efsim=FS2_ex(10,7.9,0.29,'Criterion',0.06,543,1,944,Lcal=0.1,tcal=2,nswift=0.18)   # def FS2_ex(Le,te,n,leglabel,*args,**input):
    # tsim_r=Tsim[::3]
    # efsim_r=efsim[::3]
    # MMC_FLC(630,0.21)
    # plt.grid(which='major') 
    # ax.legend(loc='best')
    # ax.legend(loc='upper left',bbox_to_anchor=(0,1))   #(1,1.2))  
    # ax.set(ylim=[0,2.5],xlim=[-0.2,0.8])
    # ax.set_yticks(np.arange(0, 2.5, 0.25))
    
    # fig.set_size_inches(cm2inch(8, 6))
    # plt.savefig('Fracture criteria.pdf',transparent=True)    
    
    
#%% Plot necking curve for whole triaxiality 
# experimental('necking own')  
# plot_HC(0.93,1.3,0.02,'necking own') 
# plot_HC(1.3762,1.4113,0.0088,'HC matine cal') 
# plot_DSSE() 
# plot_20mm('HC 20 mm')
# plot_5mm('HC 5 mm')
# ax.set(ylim=[0,1.7],xlim=[-0.6, 0.7])   
# ax.legend(loc='best')
# ax.legend(loc='upper left',bbox_to_anchor=(0,1.2))   #(1,1.2))   