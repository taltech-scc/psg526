# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 08:41:51 2020

Optimize the fracture locus

@author: mihke
"""

import scipy.optimize
import os
import sys
import glob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd



pi=np.pi
it=0
# x0 = [1.5,0.5,0.07]
def objectivefcn1(x):  
    global it
    it+=1
    A=630;  n=0.21; nf=0.1;    
    a=x[0];b=x[1];c=x[2]
    
    # a=0.205
    # b=407
    # c=0.972
    #This is test data
    ef_test=np.array([1.16,0.812,0.918])
    T_test=np.array([0.406,0.595,0.103])
    
    #Check if figure exist already 
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(1,2,figsize=(5, 3))  
    
    ax1=ax[0]; ax2=ax[1];
    ax1.plot(T_test,ef_test,'ok')
    
    def ef(T): #function for fracture locus
        theta=1-2/pi*np.arccos(-27/2*T*(T**2-1/3))  #Lode parameter
        f1=2/3*np.cos(pi/6*(1-theta))
        f2=2/3*np.cos(pi/6*(3+theta))
        f3=-2/3*np.cos(pi/6*(1+theta))
        g=((0.5*((f1-f2)**a+(f1-f3)**a+(f2-f3)**a))**(1/a)+c*(2*T+f1+f3))**(-1/nf)
        ef=b*(1+c)**(1/nf)*g
        return ef
    ef_fit=ef(T_test)       #fracture strain at test triaxiality (3 points)
    
            
    T2=np.arange(-0.66,2/3,0.001)   #Create a triaxiality range
    ef_locus=ef(T2)                 #fracture locus curve at T2
    sse=np.sum((ef_fit-ef_test)**2) #Calculate the residual 

    ax1.plot(T2,ef_locus,'-')       #plot locus
    ax2.plot(it,sse,'x')            #plot iterations                    
    return sse
# iteration=iteration+1;r

# Call the optimize function
bestx = scipy.optimize.fmin(func=objectivefcn1,x0 = [1.5,0.5,0.07])
