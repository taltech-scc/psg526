% Control file
% clear all

global iteration
iteration=1

%% Optimization routine
x0 = [1.5,0.5,0.07];
bestx = fminsearch(@objectivefcn1,x0);


% bestx=[0.105921193721735 4.138217440862026e+02 1.101986138688596]


% objectivefcn1(bestx)
% bestx =
% 
%     1.3762    1.4113    0.0088