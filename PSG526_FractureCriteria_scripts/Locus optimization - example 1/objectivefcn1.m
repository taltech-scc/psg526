%% Simulation data
function sse=objectivefcn1(x)
global iteration
% Material parameters
A=630;          %K�rgesaar 2018 Table3
n=0.21;         %K�rgesaar 2018 Table3
nf=0.1; %?xed transformation coef?cient, see Mohr and Marcadet [25]) eq.5 Pack and mohr
a=x(1);%0.205;
b=x(2);%407;
c=x(3);%0.972;

 
% a=1.61;
% b=0.71;
% c=6.2e-2;

%% Load the simulation and experimental data and calculate the residual
%CH,NT,S
%This gives the fracture strain according to HC model at specific
%experimental point


%CH,NT,SHEAR
%K�rgesaar Table 4
ef=[1.16   0.812  0.918];
eta=[0.406 0.595 0.103];

% Pack eq.(6) Hosford Coulomb model
% Corresponding to experimental data points
theta=1-2/pi*acos(-27/2*eta.*(eta.^2-1/3)); %Lode parameter
f1=2/3*cos(pi/6*(1-theta));
f2=2/3*cos(pi/6*(3+theta));
f3=-2/3*cos(pi/6*(1+theta));
g=((0.5*((f1-f2).^a+(f1-f3).^a+(f2-f3).^a)).^(1/a)+c.*(2*eta+f1+f3)).^(-1/nf);
ef_fit=b*(1+c).^(1/nf).*g;



%% compare curves
%This plots the entire locus
% Corresponding to fit
T=-0.2:0.0001:2/3;
% T=0.6;
theta=1-2/pi*acos(-27/2*T.*(T.^2-1/3)); %Lode parameter
f1=2/3*cos(pi/6*(1-theta));
f2=2/3*cos(pi/6*(3+theta));
f3=-2/3*cos(pi/6*(1+theta));
g=((0.5*((f1-f2).^a+(f1-f3).^a+(f2-f3).^a)).^(1/a)+c.*(2*T+f1+f3)).^(-1/nf);
ef_test=b*(1+c).^(1/nf).*g;


% iteration=counter;
iteration=iteration+1;

figure(1);
plot(T,ef_test);hold on;
if iteration==1
plot(eta,ef,'ok');
end


sse=sum((ef_fit-ef).^2);
figure(2);hold on;
plot(iteration,sse,'x')
end

