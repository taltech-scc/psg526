# Configuration file C:\Users\Mihkel\anaconda3\Lib\site-packages\matplotlib\mpl-data\matplotlibrc
# Copy saved also here: C:\Users\mihke\OneDrive - TalTech\Software support\Python

# If you want to use your own style then define here
# C:\Users\Mihkel\Anaconda3\Lib\site-packages\matplotlib\mpl-data\stylelib
# plt.style.use('presentation')
#k
# In this file paths are specified that are loaded automatically to python
# C:\Users\Mihkel\Anaconda3\Lib\site-packages\mk_example.pth
# https://matplotlib.org/tutorials/introductory/pyplot.html

import os
import subprocess
import sys
# sys.path.append('C:/Work/1_Research/Bending/python')
import glob
from shutil import copy2
import fileinput
import io
# import fun_userdefined as funrep
print (__name__)
from contextlib import closing
import matplotlib.pyplot as plt
import py_general as pyg
import matplotlib as mpl
import numpy as np
# Turn interactive mode on/off
# plt.ioff()
# plt.ion()
sys.path.append('C:/Users/mihke/OneDrive - TalTech/Software_support/Python/plotting')
#import fun_replace_line_AND_filename as fun_rep
cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))

#If you want in mac to navigate to current directory when folder is open 
# os.chdir(os.path.dirname(__file__))
# cwd=os.getcwd()


#umat_name=glob.glob('*.for')
#fname=glob.glob('FD.rp*')
#ALternative 1
#------------------------------------------------------------------------
#% Setting properties to lines (several methods like in matlab) 
# Controlling line properties
# Lines have many attributes that you can set: linewidth, dash style, antialiased, etc; see matplotlib.lines.Line2D. There are several ways to set line properties
# plt.savefig(('image analysis-' + fname[:-4] + '.png'),dpi=500,transparent=False)

def formatting():
    # Figure grids, axes labels, tick marks etc
    ax.grid(b=True, which='major')
    plt.grid(b=True, which='minor', color='#CCCCCC', linestyle='-', alpha=0.2)  # alpha is for transparaency       
    plt.minorticks_on()            
    plt.xlabel('Displacement (mm)') 
    plt.ylabel('Force (kN)') 
    ax.set(xlim=[0,None],ylim=[-10e+6,None])
    ax.set(ylim=[0,700],xlim=[0, 200]); ax.set(ylim=[0,None],xlim=[0,None])
    #ticks -------------------------
# Calling this function with no arguments (e.g. xticks()) is the pyplot equivalent of calling get_xticks and get_xticklabels on the current axes. Calling this function with arguments is the pyplot equivalent of calling set_xticks and set_xticklabels on the current axes.
    plt.xticks([0.2, 0.4, 0.6, 0.8, 1.],
               ["Jan\n2009", "Feb\n2009", "Mar\n2009", "Apr\n2009", "May\n2009"])
    xlab=ax.get_xticks()        #gives numeric array
    xlab=ax.get_xticklabels()   #gives numeric array + text
    
    ax.set_xticks(x)
    ax.set_xticklabels(labels)

# def axes_limits():
    xmax=a.get_xlim(); ymax=a.get_ylim();
    a.set(xlim=[0, np.ceil(xmax[1])],ylim=[0, np.ceil(ymax[1])]) 
    
    
    #Figure output    
    fig.set_size_inches(cm2inch(8, 8))
    plt.tight_layout()  # This is run after to fit the all labels and titles to specified box
    plt.savefig('FD_matine_plots.pdf',transparent=True)
    plt.savefig('FD_matine_plots.png',dpi=500,transparent=False)    
    
       
    #rc params can be applied directly from script but remain on for the session
    plt.rcParams['ytick.minor.visible'] = True    
# Kui tahad default option tagasi siis tuleb kernel restart teha!
    plt.style.use('mihkel_small_font')    
    
    
    
def simple():
    fig, ax = plt.subplots() #Total load figure

def critical_indx(E,E1,N,skip,col,cotB,*args):
    # *arg[0]=axis
    if len(args)>0:
        ax1=args[0]
        ax1.plot(E.t[N:][skip:],x[skip:],'-',color=col)          #start plotting from second stage
        ax1.plot([0,1],[2,2],'--')
        ax1.plot([0,1],[0,0],'--')
        ax1.set(ylim=[-1,3])



# Use keyword args:
def PLOT1_SIMPLE():
    plt.plot(x, y, linewidth=2.0)
    # Copy to clipboard
    # Use the setter methods of a Line2D instance. plot returns a list of Line2D objects; e.g., line1, line2 = plot(x1, y1, x2, y2). In the code below we will suppose that we have only one line so that the list returned is of length 1. We use tuple unpacking with line, to get the first element of that list:

    line, = plt.plot(x, y, '-')
    line.set_antialiased(False) # turn off antialiasing
    # Copy to clipboard
    # Use the setp() command. The example below uses a MATLAB-style command to set multiple properties on a list of lines. setp works transparently with a list of objects or a single object. You can either use python keyword arguments or MATLAB-style string/value pairs:

    lines = plt.plot(x1, y1, x2, y2)
    # use keyword args
    plt.setp(lines, color='r', linewidth=2.0)
    # or MATLAB style string value pairs
    plt.setp(lines, 'color', 'r', 'linewidth', 2.0)


    # -----------------------------------------------------------------------------------
def PLOT2_LOOP():
#Pack mohr curves
    Fmax2=np.zeros([3,3])
    name_PM=[]
    FDcurves2=glob.glob('SP4-PM/**/FD.rp*')
    for i,item in enumerate(FDcurves2):
        F=np.loadtxt(item,skiprows=4,usecols = (1)); #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
        d=np.loadtxt(item,skiprows=4,usecols = (2));
        Ep=np.loadtxt(item,skiprows=4,usecols = (3));
        ax.plot(d,F,'--',label=splitpath(item)[-15:])
        ind=max(F.argsort()[-10:])   #From the 10 biggest values take the last 10   
        Fmax2[i,:]=[ind,F[ind],Ep[ind]/area]      #Put the index,corresponding F value and normalized Ep to 3 column array
        name_PM.append(item)  

def PLOT3_grids_axis_properties():



    # activate axis
    # Use plt.sca(ax) to set the current axes, where ax is the Axes object you'd like to become active.
    # Plot without box
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
# Cycle through some properties to assign to multple figures simulatneously
    [a.grid(b=True, which='major', color='#666666', linestyle=':') for a in ax1[:,1]]   

#cycles through all line objects
    li,=ax.plot(T[i,Alive[i,:]],ep[i,Alive[i,:]],'.',color=[0.7,0.7,0.7],markersize=3,MarkerEdgeColor='none',visible=True)
    li,=ax.plot(T[i,dead[i,:]],ep[i,dead[i,:]],'.',markersize=3,MarkerEdgeColor='none', markerfacecolor='#f03b20',markeredgewidth=0.5) #orange '#feb24c')
    [plt.setp(a,markersize=10) for a in ax.lines] 

#cycles through specific lines
    L1=[]
    L1.append(ax.plot(T[i,Alive[i,:]],ep[i,Alive[i,:]],'.',color=[0.7,0.7,0.7],MarkerEdgeColor='none',visible=True))
    L1.append(ax.plot(T[i,dead[i,:]],ep[i,dead[i,:]],'.',markersize=3,MarkerEdgeColor='none', markerfacecolor='#f03b20',markeredgewidth=0.5) )
    L1.append(ax.plot(T[i,Om_bent_dead[i,:]],ep[i,Om_bent_dead[i,:]],'.g',markersize=3,MarkerEdgeColor='none',markeredgewidth=0.5) )
    [plt.setp(a,markersize=5) for a in L1]
    
def markers():
    ls=['-','--','-.','-',':']; marker=['','','','o','']
    ax1.plot(d[::20],F[::20],ls[i],label=pyg.splitpath(FD)[-7:], color=colorblue,markeredgecolor='none')
    ax1.plot(d[::60],F[::60],marker[i], color=colorblue,markeredgecolor='none',markersize=2)
def plot_colors():
    ax[1].set_prop_cycle('color',plt.cm.Spectral(np.linspace(0,1,10))) # use before plot command
     
    # Create a colormap
    col=np.empty([np.size(Le1_top1.iloc[:,1:f_step],1),3]); clb=np.empty([np.size(Le1_top1.iloc[:,1:f_step],1),3]); 
    for i in range(np.size(Le1_top1.iloc[:,1:f_step],1)):
        # print(i)
        # col[i,:]=[i,i,i]        #Gray
        col[i,:]=[i,0,0]        #red
        clb[i,:]=[0,0,i]       #blue
    col=col/np.size(Le1_top1.iloc[:,1:f_step],1)
    clb=clb/np.size(Le1_top1.iloc[:,1:f_step],1)

    #First subplot
    ax[0].set_prop_cycle(color=col)

def PLOT3_legend():
    # Legend related commands 
    leg = ax.legend()
    ax.legend(loc='best')
    ax.legend(loc='upper left',bbox_to_anchor=(0,1),prop={'size': 5})
# puts the legend outside of the plot window
    fig3.tight_layout()
    ax3.legend(loc=(1.04,0),prop={'size': 5}) 
#inside plot window
    ax3.legend(loc=(0,0),prop={'size': 5}) 
    for line in leg.get_lines():
        line.set_linewidth(4.0)

    fig, ax = plt.subplots(2,2)
    ax[0,0].plot(TIM,KR[:,0],'-',color=[0.1,0.1,1],label='K11',linewidth=0.5)
    ax[0,1].plot(TIM,KR[:,1],'-',color=[0.1,0.1,1],label='K13',linewidth=0.5)
    ax[1,0].plot(TIM,KR[:,2],'-',color=[0.1,0.1,1],label='K15',linewidth=0.5)
    ax[1,1].plot(TIM,KR[:,3],'-',color=[0.1,0.1,1],label='K33',linewidth=0.5)

    [a[1].legend(loc='best') for a in ax] 
    [a[0].legend(loc='best') for a in ax] 

    
def custom_legend():
    # plot something - usually in the loop and lot of data, so that for each marker own legend does not make sense
    ax[0].plot(Le1_top1.iloc[:,0]*1000,Le1_top1.iloc[:,1:f_step],'-',markersize=1,linewidth=0.5)
    # custom legend entries with specific color and linewidth
    custom_lines = [plt.Line2D([0], [0], color=col[0], lw=0.5),
                    plt.Line2D([0], [0], color=col[int(len(col)/2)], lw=0.5),
                    plt.Line2D([0], [0], color=col[-1], lw=0.5)]
    # add the legend to ax[0], with lines specifed in the custom lines 
    leg = ax[0].legend(custom_lines,['Top layer: beginning', '0.5', 'end'],loc='lower left',prop={'size': 5}) 

   


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

def PLOT4_barchart():
    # Bar plot
    # This example from C:\Work\1_Research\Bending\Case1-MATINE
    labels = ['2FS', 'PM', 'PMo','CL','EPS']
    # Fmaxcomb[0][0,2] - in the next line i iterate over the 5 cases to produce one array for 15 mm mesh
    EP_15mm=[]; EP_15mm=np.append(EP_15mm,[Fmaxcomb[i][0,2]  for i in np.arange(np.size(Fmaxcomb))])
    EP_7mm=[];    EP_7mm=np.append(EP_7mm,[Fmaxcomb[i][1,2]  for i in np.arange(np.size(Fmaxcomb))])                                        
             

    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars


    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, EP_7mm, width, label='7mm',color='#99d8c9')
    rects2 = ax.bar(x + width/2, EP_15mm, width, label='15mm',color='#2ca25f')
    ax.set_xticks(x)
    ax.legend()
    ax.set_xticklabels(labels)

def PLOT5_Historgram():

    #% Information about the histogram 
    # Do this now for last state only
    Bin_width=0.05          #Bin width in terms of triaxiality 
    Bins=np.arange(-0.7,0.7,Bin_width)
    np.size(Bins)

    fig, ax = plt.subplots()
    i=inc-1
    i=10

    # n, binsout, patc= ax.hist(T[i,alive_eps[i,:]], 100,density=True,fc=(1, 0, 0, 0.5),
    #                           edgecolor='k',rwidth=1)
    #1. First divide the data to bins. Count gives the number of data points in each bin
    # and edges gives the bin edges (length(hist)+1). Since edges is 
    count,edges=np.histogram(T[i,alive_eps[i,:]],Bins)
    #Normalize each bin with respect to total count to get the %
    weights=count/sum(count)
    # Plot the histogram with initial Bin width and edges. From the edges, exclude
    #the first value as this corresponds to -0.7 which is highly unprobable anyway. 
    #Exclusion is necessary as the edges parameter is 1 element longer than bins. 
    n, binsout, patc= ax.hist(edges[1:], Bins,weights=weights,fc=(1, 1, 0, 0.5),
                              edgecolor='k',rwidth=1)
    # The outcome is that sum(n) is now exactly 1


def PLOT_logicals():
# C:\Work\1_Research\Bending\Case4-Tbeam\PM\PM-T-L20
    import sys
    sys.path.append('C:/Work/1_Research/Bending/python')
    import fracture_criteria_plots as fcr
    print (__name__)
    import matplotlib.pyplot as plt
    import numpy as np
    #import time
    #Load comparative data for analysis -------------------------------------------
    ep=np.load('SDV1.npy')
    T=np.load('SDV6.npy')
    SDV3=np.load('SDV3.npy')    #Death of alive indicator
    D_DSSE=np.load('SDV7.npy')
    D_HC=np.load('SDV8.npy')
    Om=np.load('SDV15.npy')      #Bending indicator


    #---------------------------------------------------------------------------
    # Plotting scenarios
    Om[np.isnan(Om)] = 0        #Replace NANs with 0
    #Processing parameters (elements and increments)
    inc=len(ep[0:,1]); el=len(ep[0,:])
    dead=SDV3[:,:]==0           #Element death
    Alive=~dead                 #Element alive
    D_DSSE_dead=D_DSSE[:,:]>1; D_DSSE_live=~D_DSSE_dead
    D_HC_dead=D_HC[:,:]>1; D_HC_live=~D_HC_dead
    Om_bent=Om>0.7
    Om_bent_dead=np.logical_and(Om_bent,D_DSSE_dead)

    Om_mean=np.mean(Om[1:],0)
    Om_av_bent=Om_mean>0.7
    Om_av_bent_dead=np.logical_and(Om_av_bent,dead[-1,:])


    eps01=ep>0.05
    alive_eps=np.logical_and(Alive,eps01)
    #---------------------------------------------------------------------------
    # Plotting the fracture criteria
    fig, ax = plt.subplots()
    fcr.plot_HC(1.3762,1.4113,0.0088,'HC matine cal') 
    fcr.plot_DSSE()
    ax=plt.gca(); fig=plt.gcf()

def PLOT_zoom_inset():
    Files=glob.glob('A*-eps3*.csv')
    fig, ax = plt.subplots(figsize=(12, 6))  
    axins = ax.inset_axes([0.05, 0.07, 0.5, 0.4])    #define the location of the new axis
    axins.tick_params(labelsize=5)
    for j,item in enumerate(Files):
        A1 = pd.read_csv(item,skiprows=(0),header=0,delim_whitespace=False,sep=';')
        ax.plot(A1.iloc[:,0],A1.iloc[:,1],'o-',markersize=1,Markeredgecolor='none',label=item)
        axins.plot(A1.iloc[:,0],A1.iloc[:,1],'o-',markersize=1,Markeredgecolor='none',label=item)
        
def naming():
    path_part,fname_part=pyg.splitpath(os.getcwd())
    leg = ax[0].legend(loc='upper left',prop={'size': 5}) 
    ax[1].set(ylabel='S11 stess along beam (-)',xlabel='Beam length (mm)')
    ax[0].set(title=b,ylabel='S11 stress along beam (-)',xlabel='Beam length (mm)')

    ax.legend(loc='best') 
    ax.set(title='t reduction %',ylabel='t reduction %',xlabel='Section length (-)')
    # ax[1].set(title='Parameter effect Step',ylabel='epsY (-)',xlabel='Section length (-)')
    ax.grid(b=True, which='major'); axins.grid(b=True, which='major')
    ax.set(xlim=[50, 65])
    fig.set_size_inches(fcr.cm2inch(14, 7))
    plt.tight_layout()
    plt.savefig('t reduction_' + folder + '.png',dpi=500,transparent=False)
    

def picking_from_figure():
    fig, ax = plt.subplots(figsize=(14, 3))
    ax.plot(df.x,df.y,'.k',picker=5)
    def onpick(event):
        thisline = event.artist
        xdata = thisline.get_xdata()
        ydata = thisline.get_ydata()
        ind = event.ind
        points = tuple(zip(xdata[ind], ydata[ind]))
        print('onpick points:', points)
    fig.canvas.mpl_connect('pick_event', onpick) 
    
    
def imov(original,modim,a):
    #This puts two images toegether whereas one overlays the other. The a is the 
    # transparaency how much the other figure shines through. Usually not needed,
    # except in case using ax[1].imshow(image,'gray')
    # plot 2 figures together, but only in cases where the 
    np.invert(im_bw) # this inverts white and black pixels
    bld = a*im_bw + (1-a)*modim
    return bld

def replot(*args):
    '''
    Unclear here what are the args?
    '''
    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(5, 5))
    else:
        ax=ax[0]
    return fig,ax 

def FLC_plot(*args):
    #*args is the desired figure number
    if len(args)>0:
        fig,ax=newfigdef(args[0])
    else: 
        fig,ax=newfigdef()

    ax.grid(which='major') 
    # ax.set(xlabel='Minor strain [-]',ylabel='Major strain [-]')
    ax.set(xlim=[-0.8, 0.8],ylim=[0,1.5])

    # Forming limit diagram
    ax.plot([0,-0.75],[0,1.5],':',color=[0.6,0.6,0.6]);
    ax.plot([0,-1],[0,1],':',color=[0.6,0.6,0.6]);
    ax.plot([0,1],[0,1],':',color=[0.6,0.6,0.6]);
    ax.spines['left'].set_position(('data', 0.0))
    ax.spines['bottom'].set_position(('data', 0.0))
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')   
    return fig,ax 

def multiple_figure_plotting():
    #For instance, this is done in Script1.py
    fig, ax = plt.subplots() #Total load figure
    fig2, axf = plt.subplots() #Frame load figure
    plt.figure(1)
    plt.grid(which='major') 
    plt.xlabel('Displacement (mm)') 
    plt.ylabel('Total load (kN)') 
    plt.figure(2)
    plt.grid(which='major') 
    plt.xlabel('Displacement (mm)') 
    plt.ylabel('Frame load (kN)')     
    
    # This part is done in Script2.py
    fig=plt.figure(1); 
    ax1=fig.axes[0] #or 
    ax2=fig.axes[1]     # this would be the second axis used in the plot fig. For instance, it could be the second y axis. 
    fig2=plt.figure(2); axf=fig2.axes[0]
    
# When not sure wheather you have already a figure open, then use this option
    # fig=plt.gcf(); ax=fig.axes          #this to plot on existing figure
    
    # #THis makes it even more simple. 
    # fig=plt.gcf(); ax=fig.gca()           #look for existing figure and axes
    # # fig.gca() - create axes actually
    # if ax==[]:                           #if axes is empty, create a new
        # plt.close(fig)                   #Since plt.gcf() creates a new figure, we need to close this first
        # fig, ax = plt.subplots(1,2,figsize=(12, 6))     #create a figure with default properties.
# Get current figure and axis handles if you have more than one axis (second y axis)
    
    fig=plt.gcf(); ax=fig.axes   #if you would use here ax=fig.gca(), this would create axis by itself, so later in conditional it will be always true
    if len(fig.axes) > 1: #eg, we have a y2 axis
        print("1st")
        ax=fig.axes[0]
        y2ax = fig.axes[1]
    if ax==[]:              #eg, we dont have any axis
        print("2nd")
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(12, 6)) 
        y2ax = ax.twinx() 
    




    fig=plt.gcf(); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(12, 6))
        print('create from scratch')
    else:
        ax=ax[0]
        print('plot on exisiting')   
# '''
# If you have two figures open and you want to plot them second time then use this.
# Here the trick is to define figure by its name, and not by gcf
# '''
    fig=plt.figure(1); ax=fig.axes
    if ax==[]:
        plt.close(fig)
        fig, ax = plt.subplots(figsize=(12, 6))
        print('create from scratch')
    else:
        ax=ax[0]
        print('plot on exisiting')   
    fig2=plt.figure(2); axf=fig2.axes
    if axf==[]:
        plt.close(fig2)
        fig2, axf = plt.subplots(figsize=(12, 6))
        print('create from scratch')
    else:
        axf=axf[0]
        print('plot on exisiting')  


    # fig=plt.figure(1); ax=fig.axes
    # if ax==[]:
    #     plt.close(fig)
    #     fig, ax = plt.subplots(figsize=pyp.cm2inch(8, 8))
    #     print('create from scratch')
    # else:
    #     ax=ax[0]
    #     print('plot on exisiting') 
        
    # mngr = plt.get_current_fig_manager()
    # mngr.window.setGeometry(3000,1000,700, 700)    
        
    # fig2=plt.figure(2); axf=fig2.axes
    # if axf==[]:
    #     plt.close(fig2)
    #     fig2, axf = plt.subplots(figsize=pyp.cm2inch(8, 8))
    #     print('create from scratch')
    # else:
    #     axf=axf[0]
    #     print('plot on exisiting')  


    # ax.grid(which='major') 
    # ax.set(xlabel='Triaxiality',ylabel='EPS')

    # axf.grid(which='major') 
    # axf.set(xlabel='Minor strain [-]',ylabel='Major strain [-]')





def annotate():  
    ax.plot(d[frac_step],F[frac_step],'or',markersize=3)
    ano=('{:3.0f}mm,{:3.1f}MN'.format(d[frac_step],F[frac_step]/1000))  #Annotate text for fracture point
    ax.annotate(ano,(d[frac_step],F[frac_step]),fontsize=4,             #Annotation
                xytext=(0, 3),textcoords="offset points")
    # Using latex on labels
    ax2.set(xlabel=r'$t\sqrt {\frac{{2g}}{B}} $',ylabel=r'$\frac{{{h_{33}}}}{{2\rho gD}}$')
# This is for load-time figure       

def subplots2():
    fig, (ax,ax2) = plt.subplots(1,2) #Total load figure, ax and ax2 are axis handles
    # fig, (ax,ax2) = plt.subplots(1,2,figsize=(8, 4))
    ax.legend(loc='best')
    ax.grid(which='major') 
    ax.set(title='Input damping matrices from HS',xlabel='Wave freq (rad/s)',ylabel='Amrt')


def fig_position(): # set the figure position
    mngr = plt.get_current_fig_manager()
    mngr.window.setGeometry(50,100,640, 545)

def newfigdef(*args):
    if len(args)==0:
        fignr=None
    else:
        fignr=args[0]
    def myfig(*fignr):
        if len(fignr)==1:
            fig=plt.figure(fignr[0]); ax=fig.axes
            
            # print("Create figure with nr ",fignr[0])
        else:
            fig=plt.figure(); ax=fig.axes
        if ax==[]:
            plt.close(fig)
            fig, ax = plt.subplots(1,1,figsize=cm2inch(12, 8),num=fignr[0]) 
            # print('create from scratch')
        else:
            ax=fig.axes[0]
        # print('plot on exisiting') 
        return fig,ax
    
    fig,ax=myfig(fignr)
    return fig,ax 

def opensaved_figure():
    import pickle
    # save the figure to pickle file
    pickle.dump(fig, open('Figure-energy-BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15.fig.pickle', 'wb')) 

    # open figure
    figx = pickle.load(open('FigureObject-energy.fig.pickle', 'rb'))
    #if you want to get the axes handle then 
    ax=plt.gca()
def args_kwargs():
    # maybe you have a situation where you want to use default value and optionally set the parameters to something else
    def plot_1_step_FLC(filepath,*fignr,**kwargs):
        # os.chdir(filepath)
        defaultKwargs = { 'plottype': 'flc'}  #This is the default value, option value is set 
        kwargs = { **defaultKwargs, **kwargs} # two starst means that dictionaries are merged with second dict overwriting the first
        if len(fignr)>0:
            if kwargs['plottype']=='normal':
                fig,ax=pyp.newfigdef(fignr[0])
            elif kwargs['plottype']=='flc':
                fig,ax=pyp.FLC_plot(fignr[0])
        else:
            fig,ax=pyp.FLC_plot()

def export_to_proper_size():
    # This is the most convenient way to save
# 1) Put the legend outside of the figure 
# 2) set the figure size so that you like it more less 
# 3) set tight layoug
# 4) export
    ax3.legend(loc=(1.1,0),prop={'size': 5})
    fig3.set_size_inches(pyp.cm2inch(12, 7))
    ax3.set(ylim=[0,1.6],xlim=[-1.2,1.2],xlabel='Minor strain (-)',ylabel='Major strain (-)')
    fig3.tight_layout()        
    # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
    ax3.set_xticks(np.arange(-1.2,1.2+0.4,0.4))
    # plt.savefig('FD_matine_plots.pdf',transparent=True)
    # plt.savefig('FD_matine_plots.png',dpi=500,transparent=False)  
if __name__ == "__main__":
    fig,ax=FLC_plot(1)
